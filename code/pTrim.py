#!/usr/bin/python2.7

import pandas as pd
import numpy as np
import unicodedata
from collections import defaultdict
import argparse
import textwrap
import sys
import os
import time as tm
from time import localtime, strftime
import logging
import subprocess as sp
import re
import gzip
import gc
from itertools import izip
import threading
import multiprocessing as mp
import glob

#input arguements
parser = argparse.ArgumentParser(description="This script is intended to trim primer sequences from FASTQ files, using a list of primers.\n Optionally, can also be used to trim sequences that occurs upstream to primers such as UMI & SSE.\n",
								 usage= 'use "python %s --help" for more information' % sys.argv[0],
								 formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("--version",action='version',version=textwrap.dedent('''\
	Program: %(prog)s
	Version: 2.6.1
	Author: shashank, BI TEAM, EDGC
	contact: s.katiyar@edgc.com
	Update Notes:
		V2.6.1:
			Fixing:
				1) Reads were being discared if 'N' was present in the read; Now will not be discared
				2) Fixed issue where shorter than 10 bp reads were being discared. Now in reality, we should discard such reads
				   but, this was unintended side-effect, hence fixed. I might apply a read length filter instead.
		V2.6:
			Functional:
				1) Now includes option to skip the primer trimmin and do rest of the jobs.
				This is helpful if the read length becomes very short after the primer trimming.
				Like for ED-M or Organ transplant panel.				
			Performance:
				1) Performance wise it will stay the same but for very large files, may take sligthly more time.
			Bug fixed:
				1) Now paired reads will also be trimmed if ROI file is provided.
			
		V2.5:
			Functional:
				1) Process the input file in blocks of 1 million line or 250000 reads
				2) Process multiple blocks together
				3) UMI now can be appended at the end of FastQ ID in @Seq_ID (first line of each read in fastq) line. It is going to be optional though.
			Performance:
				1) RAM usage fixed now. Will depend on the number of reads used per core. (Like for one Million reads per core, it should not exceed 600 to 700 MB.)
				2) Multithreading supported, hence execution  is significantly much faster.
				
		V2.4:
		Major:
			1) Trim all the primers or ROI based, both options are included in this script now. Now, Selective trimming is optional. 
			   If a ROI file is provided, 'Selective Trimming' will be performed, lese all the primers will be trimmed from GSP-conataining and UMI-containing reads both.
			2) Now SSE trimming is supported during primer trimming. SSE of both UMI containing and GSP containing reads can be trimmed now.
			3) UMIs will be saved during the SSE trimming now, in the fastq format.
			4) UMI extraction method is changed in this version: Now if a UMI is present in the GSP read, it will be used instead of the UMI read.
		Minor:
			1) For debugging, Implemented the option to save UMI-containing (R2 in qiagen) reads that does not have any primers. Such reads are unusal to have.
			2) Better memory management
		
		V2.3:
		1) Implementing the primer trimming of the paired reads; In the earlier versions, only the gene specific primer containing reads were being trimmed
		
		V2.2:
		1) Bug Fix: Previous versions were not trimming the reverse primers. Fixed now.
		
		V2.1:
		1) Allow one mismatch while searching for the primer sequence in the Read
		
		V2.0:
		1) Supports the 'Selective Primer Trimming (SPT)'. SPT is trimming of only those primers that occur in the ROI regions. In reality, only these primers are required to be trimmed.
		This will improve the alignment and avoid the edge effect, plus will not have short reads
		2) Removal of reads if after trimming the read length is less than 40
		3) Support for the .fasta file as an input
		4) Auto cut-off set mode; How many bases of primer sequence to use to make the index, that is searched in the read. We want to keep it as minimum as possible.
	'''))
parser.add_argument('--info', help=textwrap.dedent('''\
------------------------------------------------------------------------REQUIRED ARGUEMENTS------------------------------------------------------------------------
'''
))
parser.add_argument('-s',"--sequencing",type=str,choices=['paired','single'],help=textwrap.dedent('''\
Required: Please provide the type of the sequencing.
Choices: 
	paired : For paired-end sequencing
	single: For single-end sequencing
'''))
parser.add_argument('-if',"--input-fastq",type=str,help=textwrap.dedent('''\
Required: Please provide the input fastq.gz/fq.gz that starts with Gene Specific Primer.
	*_R1.fastq.gz : In Qiaseq Panel
	*_R2.fastq.gz : In Archer Panel
'''))

parser.add_argument('-if2',"--input-fastq2",type=str,help=textwrap.dedent('''\
Required in case of 'paired' end sequencing: Please provide the paired read fastq to be trimmed file.\n
'''))

parser.add_argument('-pl',"--primer-list",type=str,help=textwrap.dedent('''\
Required: Please provide the input list of primer sequences for fastq read trimming.
This list should contain only the sequences, one in each row. File extension does not matter.
NOTE: THIS LIST WILL BE USED TO MATCH PRIMER SEQUENCE WITH READS. HENCE FOLLOING CRITERIA MUST MATCH:
	1) PLUS STRAND PRIMER SEQUENCES: NO CHANGE.
	2) MINUS STRAND PRIMER SEQUENCES: REVERSE COMPLEMENTED (to match with reverse reads). 
'''))
parser.add_argument('-ssp',"--sse-startpos",type=int, help=textwrap.dedent('''\
Required: Please provide the position in read from where SSE is starting in the paired reads i.e. given using the option -if2
E.G.:
For qiaseq panel: 13 (12 Base UMI followed by SSE)
For archer panel: 9 (8 Base UMI followed by SSE)
For ED-panel panel: 11 (10 Base UMI followed by SSE)
'''))
parser.add_argument('-ss','--sse-sequence',type=str, help=textwrap.dedent('''\
Required: Please provide the Sequence of SSE in the paired reads i.e. given using the option -if2. 
E.G.:
For qiagen panel: ATTGGAGTCC
For archer panel: AACCGCCAGGAGT
For ED-panel: AACCGCCAGGAGT
For OrganTransplan: AAGCGCCAGGAG
'''))

parser.add_argument('-st','--sse-tail',type=int, help=textwrap.dedent('''\
Required: If there is/are base/bases just after SSE that are introduced artificially, like A-tail after SSE in qiagen. Please provide the number of bases.
0: No tailing base (in Archer panel)
1: 1 base tailing (in Qiagen panel)
User can give any base number from 0 to n.

------------------------------------------------------------------------OPTIONAL ARGUEMENTS: Core Arguments------------------------------------------------------------------------	
'''))

parser.add_argument('-pt','--primer-trimming',type=str,default='yes',choices=['yes','no'],help=textwrap.dedent('''\
Optionl but important: Trim primers or Not?
Choices: yes(default): Trim primers + all other functions
		 no : Do not trim primers but do all other functions
'''))
parser.add_argument('-mr','--minimum-readlength',type=int,default=15,help=textwrap.dedent('''\
Optional but important: Discard reads that are less than -mr parameter length, after all kind of trimming.
Default: 15
Note: This read lengtht is the processed read length means after removing 'UMI','SSE','Primer' sequences.
'''))
parser.add_argument('-roi',"--target-bed", type=str, help=textwrap.dedent('''\
Optional But Important: (reference genome and 'bedtools' are needed).
Please provide the location of the target ROI bed file.
This option enables the "Selective Primer Trimming". Only those primers will be trimmed that occur inside the ROI region.
NOTE: If no target roi file is provide using this option or '-rois' option, all the primers will be trimemd.
'''))
parser.add_argument('-rois',"--target-sequences", type=str, help=textwrap.dedent('''\
Optional: This option is alternative of -roi option, if user wants to directly provide the sequences of the target region. Helpful, if user does not has reference database or BEDtools.
Please provide the location of file that contains the sequences of target regions.
'''))
parser.add_argument('-uem','--umi-extractionmethod',default='write', type=str, choices=['write','append'],help=textwrap.dedent('''\
Optional but Important: UMI can be extracted by follwoing two methods:
	1) 'write' (default) : Write the UMI information in a separate file in .fastq format.
	2) 'append' : Append the UMI at the 11th column of @Seq_ID (first line of each read) in output R1 and R2 fastq file.
		e.g.: 
		Normal Read:
		@NB502108:23:HVHJKBGX5:4:13606:24655:7596 1:N:0:CAGAGAGG+TCCTGCTT
		GGGACTGACTTTCTGCTCTTGTCTTTCAGACTTCCTGAAAACAACGTTCTGGTAAGGA
		+
		AEEEEEA/E//A//<EEA/EEEEEEAEAEA<EAEA/AAEE/EE/EEAAA/E<AEA/EE
		
		'append' method UMI extraction, added its 10 base UMI + Quality (separated by #) at the last of fastqid:
		@NB502108:23:HVHJKBGX5:4:13606:24655:7596:CTTTGGAT#EEEEEEEE 1:N:0:CAGAGAGG+TCCTGCTT
		GGGACTGACTTTCTGCTCTTGTCTTTCAGACTTCCTGAAAACAACGTTCTGGTAAGGA
		+
		AEEEEEA/E//A//<EEA/EEEEEEAEAEA<EAEA/AAEE/EE/EEAAA/E<AEA/EE

'''))

parser.add_argument('-usp','--umi-startpos',default='1', type=int,help=textwrap.dedent('''\
Optional but Important: Please provide the UMI start position.
Default: 1 (UMI) is starting from the first base.
'''))

parser.add_argument('-uep','--umi-endpos',type=int,help=textwrap.dedent('''\
Optional but Important: Please provide the UMI end position.
Default: By default, the UMI end position is guessed based on the SSE start position.
		 Because for Qiagen and Archer, SSE starts just after UMI.
		 If this argument is provided, this will be used to extract UMIs.
'''))
"""
parser.add_argument('-dds','--drop-doublesse', default ='1', type=str,choices=['1','2'], help=textwrap.dedent('''\
Optional: Some of the UMI-containing reads might contain SSE twice. This is error and such reads can be dropped.
Choices:
	0: Do not find and drop such reads (Please use this option if unknown or new panel.)
	1 (Default): Find and drop such reads (Will work almost accurately for the Qiagen and Archer panel.)
'''))
"""
parser.add_argument('-uof','--umi-outfilename',type=str,help=textwrap.dedent('''\
Optional: Please provide the output filename of UMI fastqfile.
Default: '<output-filename-prefix>_umi.fastq'
'''))

parser.add_argument('-te',"--trim-extra",default='0',type=str,help=textwrap.dedent('''\
Optional: If fastq file contains some sequences/characters before the primer sequence, the length of such sequence must be defined using this option.
For e.g., if there are 10 UMI bases, 12 SSE sequence bases and 1 A-tagging base, the total of 23 should be provided.
Default: 0 (Use 0, if input fastq reads are already trimmed and start with primer sequences.)
WARNING: Providing wrong information will surly lead to wrong trimming of the bases that would affect all the future steps.

------------------------------------------------------------------------OPTIONAL ARGUEMENTS: Multithreading Arguments------------------------------------------------------------------------	
'''))

parser.add_argument('-np','--numberof-process',type=int,default=10, help=textwrap.dedent('''\
Optional: Please provide the number of clocks to process simultaneosly or simply put, how many cores to use for processing?
Default : 10 (maximum 10 processes at once)
NOTE: This is the maximum number of processes to use at once, and actual usage may be less (is most cases it will be quite less than it).
'''))

parser.add_argument('-tir','--totalinput-reads',type=int,help=textwrap.dedent('''\
Optional: Please provide the total number of reads in the input fastq files.
	Use this option to optimize the multiprocessing. By default, each block to be processed contains 5000000 lines.
	Hence, the number of blocks (and processing cycles) are selected automatically by above number (and are different from number of process). 
	However, if use provides the total number of lines in the input fastq file, the number of blocks can be made equals to number of process.
	And the whole file can be processed in one cycle.
Warning: DO NOT PROVIDE -TIR IF YOU BELEIVE THAT PER BLOCK READS' (totalInputReads/numberOfProcess) WILL CONSUME LOT'S OF MAMORY.
	For example, if numberOfProcess is 1 and user provide this option, all the reads will be processed at once and consume lots of mamory.
'''))

parser.add_argument('-pr','--percore-reads',type=int,default=100000,help=textwrap.dedent('''\
Optional: Use this option to override the default per core reads to be processed count.
Default: 100000 (One Hundred Thousand)
NOTE: If '-tir' option is used, this value is will be overridden.

------------------------------------------------------------------------OPTIONAL ARGUEMENTS: Locations------------------------------------------------------------------------	
'''))


parser.add_argument('-od',"--out-dir",type=str,default=os.getcwd(),help='Optional= Location of output result files. Default: %s' %os.getcwd())

parser.add_argument('-o', "--output-filename", type=str, default='0', help=textwrap.dedent('''\
Optional: Please provide the output file for the first read.
default: Input file name + '_primerTrimmed.fastq'.)
'''))

parser.add_argument('-o2', "--output2-filename", type=str, default='0', help=textwrap.dedent('''\
Optional: Please provide the output filename for the paired read.
default: Input file name + '_primerTrimmed.fastq'.)
'''))

parser.add_argument('-refgen', "--refgen-location", type=str, default='/Work/BIO/refs/hg19/ucsc.hg19.fasta', help=textwrap.dedent('''\
Optional: Please provide the location of human genome database. This will be used to convert -roi bed file to sequence.
default: /Work/BIO/refs/hg19/ucsc.hg19.fasta
'''))

parser.add_argument('-bt', "--bedtools-location", type=str, default='/usr/bin/bedtools', help=textwrap.dedent('''\
Optional: Please provide the location of BEDTools software. This will be used to convert -roi bed file to sequence.
default: /usr/bin/bedtools

------------------------------------------------------------------------OPTIONAL ARGUEMENTS: Developmental------------------------------------------------------------------------	
'''))

parser.add_argument('-sd', "--savedropped-reads", type=str, choices=['0','1'], default='0', help=textwrap.dedent('''\
Optional: Weather to save reads that have error in the primer region. By default, these reads are discarded.
If turned-on, A fastq file ending with '_dropped.fastq' will be available in the run directory.
default: 0 (Do Not Save)
'''))

parser.add_argument('-snp', "--savenoprimer-reads", type=str, choices=['0','1'], default='0', help=textwrap.dedent('''\
Optional: Save R2 reads that do not have atleast 10 base overlapping primers. This option is used for debugging and development purpose only.
default: 0 (Do Not Save)
'''))

parser.add_argument('-snpnp', "--savenoprimernopair-reads", type=str, choices=['0','1'], default='0', help=textwrap.dedent('''\
Optional: Save R2 reads that do not have complete primers + atleast 10 base overlapping Paired read. This option is used for debugging and development purpose only.
default: 0 (Do Not Save)
'''))

parser.add_argument('-dnpnp', "--dropnoprimernopair-reads", type=str, choices=['0','1'], default='0', help=textwrap.dedent('''\
Optional: Do not include R1 and R2 both if, R2 read does not have complete primers + atleast 10 base overlapping Paired read. This option is used for debugging and development purpose only.
default: 0 (Do Not Save)
'''))

parser.add_argument('-kt','--keep-tmpfolder',type=str,choices=['no','yes'],default='no', help=textwrap.dedent('''\
Optional: Keep the tem folder 'pTrimBlocks' after the processing is complete or not?
Choices:
	no (Default): Delete the 'pTrimBlocks' folder once the job is completed.
	yes: Keep the 'pTrimBlocks' folder once the job is completed.
'''))

parser.add_argument('-pb', "--progressbar", type=str,choices=['0','1'], default='1',help=textwrap.dedent('''\
Optional: Use progress bar to display progress of execution or not. Disable this option if 'tqdm' library is not installed.
Or you can install the 'tqdm' library as: 'sudo pip install tqdm' or 'pip install U tqdm'
default: Enabled.
Choices:
	0: Disabled
	1: Enabled (Default)
'''))

i_args = parser.parse_args()

parser.print_help()

print "\n\n===========================================================================================================================================================\n\
Starting the primer trimming using %s program\n\n================================================================>--Verifying the locations" % sys.argv[0]

###Functions
#Make a job handling functions
def makeProcess(command):
    process = sp.Popen(command, stderr=sp.PIPE,stdout=sp.PIPE, shell=True)
    output, error = process.communicate()
    returnCode = process.returncode
    return output, error, returnCode
	
####define a method to reverse complement the sequence; this will be use to check if a primer sequence is in ROI region or not. Otherwise , minus sequences will be left out.
def rc(inputSequence):
	'''
	input: Sequence
	output: reverse complemented sequence
	'''
	complementDict={'G':'C','A':'T','C':'G','T':'A','N':'N'}
	rcList = list(reversed([complementDict[x] for x in inputSequence]))
	return ''.join(rcList)

####Multithreading options
processNum=i_args.numberof_process
if processNum == 0:
	print "\n\nINPUT_ARGUMENT_TYPE_ERROR: The number of process can not be 0. Please choose 1 or more using option '-np'\n"
	sys.exit()

if i_args.totalinput_reads:
	#check if the input read number if multiple of 4:
	totalinput_reads=i_args.totalinput_reads
	if not ((i_args.totalinput_reads % 4) == 0):
		print "\n\nINPUT_ARGUMENT_TYPE_ERROR: The number of reads in input file are not multiple of 4. Please provide the input number of reads correctly using option '-tir'\n"
		sys.exit()
	linespercore=(i_args.totalinput_reads)/processNum
else:
	totalinput_reads=None
	linespercore=i_args.percore_reads
	
	
###output directory control
if i_args.out_dir:
	print "Will use following DIR for the results.\n%s" % i_args.out_dir
	try:
		os.chdir(i_args.out_dir)
		outputDir = i_args.out_dir.replace("\n",'')
		print "Working inside %s DIR" % outputDir
	except:
		print "Invalid output DIR location. Please verify the outdir location.\n Terminating.\n"
		raise
else:
	print "No outdir specified, working in current directory."
	outputDir = makeProcess('pwd')
	outputDir=outputDir[0].replace("\n",'')
	print "\tInside 'qualityCheck' DIR. Working at the following location.\n\t%s"  % outputDir[0]

#check input FASTQ file
if i_args.input_fastq:
	#let's check if the file exists
	iFASTQ = os.path.abspath(i_args.input_fastq)
	if os.path.isfile(iFASTQ):
		print "Input fastq file found. Moving on."
	else:
		print "\n\nError:Input fastq file could not be located.\nPlease provide the correct PATH and Name of the FASTQ file\n.Terminating.\n"
		sys.exit()
	#check if the input file is fastq file, by extension
	#get the filename
	try:
		fastqName_obj = re.search('[0-9a-zA-Z-_\.]*\.gz',iFASTQ)
		fastqName = fastqName_obj.group(0)
	except AttributeError:
		print "Could not extract the FASTQ filename. Please check the script.\n"
		raise
	
	if fastqName[-6:] == '.fq.gz':
		print "Input file verified as fastq file."
		fastq_extension = '.fq.gz'
		getLogFileName = fastqName[:-6]+'_Ptrim.log' #log file name
		outFQ_filename = fastqName[:-6]+'_primerTrimmed.fastq' #output filename
		dropoutFQ_filename=fastqName[:-6]+'_dropped.fastq' #output filename
		sampleName=fastqName[:-6]
	elif fastqName[-9:] == '.fastq.gz':
		print "Input file verified as fastq file."
		fastq_extension = '.fastq.gz'
		getLogFileName = fastqName[:-9]+'_Ptrim.log' #log file name
		outFQ_filename = fastqName[:-9]+'_primerTrimmed.fastq' #output filename
		dropoutFQ_filename=fastqName[:-9]+'_dropped.fastq' #output filename
		sampleName=fastqName[:-9]
	else:
		print "\n\nError:: Could not verify the input file as a FASTQ file. Please provide input file as fastq."
		sys.exit()
else:
	print "\n\nError:Please provide an input FASTQ file using -if option.\nType '%s -h' for help." % sys.argv[0]
	sys.exit()

#Logging start
if os.path.isfile(getLogFileName):
	os.system("rm %s" %getLogFileName)

logging.basicConfig(filename=getLogFileName, mode='w',level=logging.DEBUG,format='%(asctime)s %(message)s',datefmt='%d/%m/%Y %I:%M:%S %p-->')	
	
####output filename
if i_args.output_filename == '0':
	print "ATTENTION: User did not specify any output filename. Using the 'input filename'+'_filtered.txt' as output filename."
	#get the current directory
	outFQ_filename = outFQ_filename
	print "Name of the output file: %s"%outFQ_filename
else:
	outFQ_filename = i_args.output_filename
	print "Using user provided output filename.\nName of the output file: %s"%outFQ_filename
	
#check the input fastq file
if i_args.primer_list:
	primerList_file = os.path.abspath(i_args.primer_list)
	if os.path.isfile(primerList_file):
		print "Primer sequence file has been located."
	else:
		print "\n\nError:The PATH to the primer list file is invalid. Please provide a correct path to the primer list file.\n"
		sys.exit()
else:
	print "\n\nError:Please provide an input Primer Sequence file using -pl option.\nType '%s -h' for help." % sys.argv[0]
	sys.exit()
	
#check the input fastq file
isROI = False
isROIs = False
#check the roi bed file
if i_args.target_bed:
	isROI = True
	iBED = os.path.abspath(i_args.target_bed)
	if os.path.isfile(iBED):
		print "BED file located."
	else:
		print "\n\nError:Could not locate the BED file at user provided location.\nPlease provide the correct location of ROI bed file using option '-roi'.\nUse '%s -h' for more help." % sys.argv[0]
		sys.exit()
else:
	print "User did not provide any ROI file, Hence all the primers will be trimemd."
	print i_args.target_sequences
	
#check the roi bed file
if i_args.target_sequences:
	isROIs = True
	iBEDseq = os.path.abspath(i_args.target_sequences)
	if os.path.isfile(iBEDseq):
		print "Sequence file of the target region located.\n"
	else:
		print "\n\nError:Could not locate the Sequence file of the target region, at user provided location.\nPlease provide the correct location of ROI bed file using option '-rois'.\nUse '%s -h' for more help." % sys.argv[0]
		sys.exit()

####trim extra
if i_args.trim_extra:
	try:
		trimExtra = int(i_args.trim_extra)
	except ValueError:
		print "ERROR: Provide length is not an integer type. Please provide only numbers.\nPlease provide the number of bases occurring before the primer,using '-te' option.\n use '0' if fastq reads start with primer sequences.\nType '%s -h' for more detail.\n" % sys.argv[0]
		raise
	print "User reported that there are %d bases before the primer starts." %trimExtra

####SSE treatment
if i_args.sse_startpos:
	sseStartPos_index=i_args.sse_startpos-1
else:
	print "\n\nMissingArgument: Please provide the SSE start position in the read, using the argument '-ssp'."
	sys.exit()

if i_args.sse_sequence:
	sseSeq = i_args.sse_sequence
	reverseSSE_real=rc(sseSeq)
	sseFirstLetter=sseSeq[:1]
	lengthSSE=len(sseSeq)
	sseEndPos_index=sseStartPos_index+lengthSSE
else:
	print "\n\nMissingArgument: Please provide the SSE sequence in the reads, using the argument '-ss'."
	sys.exit()	
	
try:
	sseTail_count = int(str(i_args.sse_tail))
except (TypeError,ValueError):
	print "\n\nMissingArgument: Please provide the length of bases, tailing the SSE, using the argument '-st'."
	sys.exit()

''' #let's always discard double sse reads

if i_args.drop_doublesse == '1':
	dromDoubleSSE = True
elif i_args.drop_doublesse == '0':
	dromDoubleSSE = False
'''

#UMI treatment
if i_args.umi_extractionmethod == 'write':
	isAppendUMI=False
elif i_args.umi_extractionmethod == 'append':
	isAppendUMI=True
else:
	print "\n\nWrongInputArgument: There can only be two UMI extraction methods. Please choose one using option '-uem'.\n"
	sys.exit()	
	
umiStartPos_index = i_args.umi_startpos - 1
if i_args.umi_endpos: #if user has defined the end position of UMI
	umiEndPos_index=i_args.umi_endpos

else: #else, figure out using the SSE data
	umiEndPos_index=sseStartPos_index
lengthUMI=umiEndPos_index-umiStartPos_index

####Trim primers or Not
if i_args.primer_trimming == 'yes' or i_args.primer_trimming == 'Yes' or i_args.primer_trimming == 'YES' or i_args.primer_trimming == 'y' or i_args.primer_trimming == 'Y':
	pTrim=True
	logging.debug("-pt: Yes | Primers will be trimmed.\n")
elif i_args.primer_trimming == 'no' or i_args.primer_trimming == 'No' or i_args.primer_trimming == 'NO' or i_args.primer_trimming == 'n' or i_args.primer_trimming == 'N':
	pTrim=False
	logging.debug("-pt: no | Primers will not be trimmed.\n")

#minimum-readlength filter
minReadLen=i_args.minimum_readlength

#progressbar = '1'
#if i_args.progressbar == '1':
if i_args.progressbar == '1':
	pb = True
	try:
		from tqdm import tqdm
	except ImportError:
		print "Error: Could not import tqdm. Trying to install it for you."
		os.system("pip install tqdm")
		try:
			from tqdm import tqdm
		except ImportError:
			print "\n\nERROR: Installation failed, could not install 'tqdm' . Please install the module using 'pip install tqdm' command, if you want to use the progressbar."
			pb = False
else:
	pb = False		
		
if i_args.savenoprimer_reads == '1':
	saveNoPrimer_R2=True
	saveNoPrimer_R2_list=[]
else:
	saveNoPrimer_R2=False
	

if i_args.savenoprimernopair_reads == '1':
	saveNoPrimerNopair_R2=True
	saveNoPrimerNopair_R2_list=[]
else:
	saveNoPrimerNopair_R2=False
	
if i_args.dropnoprimernopair_reads == '1':
	dropNoPrimerNopair_R2=True
else:
	dropNoPrimerNopair_R2=False
	

if isROI == False and isROIs == False:
	print "WARNING: User did not provide any ROI bed file or sequence file for the target region. All, the primers will be trimmed.\n"
	logging.debug("WARNING: User did not provide any ROI bed file or sequence file for the target region. All, the primers will be trimmed.\n")
	roiSeq_list=[]
#let's first treat the ROI file; if given, get its sequences
if isROI and (not isROIs):
	try:
		getBedName_obj = re.search('[0-9a-zA-Z-_\.]*.bed',iBED)
		getBedName = getBedName_obj.group(0)
	except AttributeError:
		print "Could not extract the real name of input bed file. Using the complete name"
		logging.debug("Could not extract the real name of input bed file. Using the complete name")
		getBedName = iBED
	bedSequence_filename = getBedName+'.sequence'
	#we will need reference genome to get the sequence, hence, let's verify its location
	if os.path.isfile(i_args.refgen_location):
		print "Reference Genome located at: %s" % i_args.refgen_location
		logging.debug("Reference Genome located at: %s" % i_args.refgen_location)
	else:
		print "\n\nError:Could not locate the reference genome. In order to use the roi, user must provide reference genome location. \n Optionally, use can directly give the targets region's sequences using '-rois' option. \nPlease use '-reference' option to provide the genome or  '%s -h' for more help" % sys.argv[0]
		logging.debug("\n\nError:Could not locate the reference genome. In order to use the roi, user must provide reference genome location. \n Optionally, use can directly give the targets region's sequences using '-rois' option. \nPlease use '-reference' option to provide the genome or  '%s -h' for more help" % sys.argv[0])
		sys.exit()
		
	#check if the bedtools is installed on the system
	isBedtools = makeProcess('%s' % i_args.bedtools_location)
	#isBedtools = makeProcess('bed')
	#print "Output:%s\nError:%s\nReturncode:%s\n" % (isBedtools[0],isBedtools[1],str(isBedtools[2]))
	
	if not str(isBedtools[2]) == '0':
		print "\n\nError:Could not locate the bedtools on this system. Please provide the path of bedtools using option '-bt'."
		print "ERROR:\n%s" % isBedtools[1]
		print "Return Code was:\n%s" % isBedtools[2]
		logging.debug("Could not locate the bedtools on this system. Please provide the path of bedtools using option '-bt'.")
		logging.debug("ERROR:\n%s" % isBedtools[1])
		logging.debug("Return Code was:\n%s" % isBedtools[2])
		sys.exit()
	else:
		print "bedtools located on this system."
		logging.debug("bedtools located on this system.")
	
	#let's generate the sequence using the bedtools
	getBedSequence_command = '%s getfasta -fi %s -bed %s -fo %s -tab' % (i_args.bedtools_location, i_args.refgen_location, iBED, bedSequence_filename)
	print "\nRetrieving the sequences of the target regions using the following command:\n%s" % getBedSequence_command
	logging.debug("\Retrieving the sequences of the target regions using the following command:\n%s" % getBedSequence_command)
	getBedSequence = makeProcess(getBedSequence_command)
	if not getBedSequence[2] == 0:
		print "\n\nError:Returncode:%s\nError occurred with bedtools:\n%s" % (getBedSequence[2],getBedSequence[1])
		logging.debug("\n\nError:Returncode:%s\nError occurred with bedtools:\n%s" % (getBedSequence[2],getBedSequence[1]))
		sys.exit()
	else:
		print "Sequence retrieval successful.\n%sWARNING:%s\n" % (getBedSequence[0],getBedSequence[1])
		logging.debug("Sequence retrieval successful.\n%sWARNING:%s\n" % (getBedSequence[0],getBedSequence[1]))
		
	#need to retrieve the sequences from the converted file
	roiSeq_list = []
	with open(bedSequence_filename) as bedFileseq:
		for eachLine in bedFileseq:
			eachLine = eachLine.rstrip().replace("\n",'')
			if len(eachLine) > 0:
				lineData = eachLine.split("\t")
				roiSeq_list.append(lineData[-1])
				#print "Seq is:%s" % lineData[-1]
elif isROIs:
	roiSeq_list = []
	with open(iBEDseq) as bedFileseq:
		for eachLine in bedFileseq:
			eachLine = eachLine.rstrip().replace("\n",'')
			if len(eachLine) > 0:
				roiSeq_list.append(eachLine.uppder())


#
##
###
####
#####primer dictionary making
####
###
##
#
####mismatch generator
def generateMismatch(inputSeq):
	'''
	Input: A sequence
	Output: List of all the possible sequences with one mismatch at every position
	'''
	misMatch_list = []
	misMatch_list.append(inputSeq)
	inputSeq_list = [x for x in inputSeq]
	replacementDict = {'G':['A','C','T'],'A':['G','C','T'],'C':['A','G','T'],'T':['A','C','G'],}
	for index,base in enumerate(inputSeq_list):
		replacemnetList = replacementDict[base]
		for eachbase in replacemnetList:
			replacedList=[] #make a new list
			replacedList.extend(inputSeq_list) #fill it with the original list
			replacedList[index]=eachbase #mutate with the possible base name
			misMatch_list.append(''.join(replacedList)) #make it a sequence again and append it to mismatch list
	return misMatch_list

#orig = 'GACTATACTACTGTCAGGCTAGCTACAGTCGATCGTAGCTACGTACGATCGATCGATG'
#print orig
#print generateMismatch(orig)

#Note: Discard the mismatches but keep those primer data that is not in ROIdiscar
print "Reading the primer list and making the index.\n"
logging.debug("Reading the primer list and making the index.\n")


primerNum = 0
usedPrimers = 0
excludedPrimers_list = []
primDict = {} #dictionary to store the trimmed primer sequences and the length
'''
primDict structure:
key:
value: list
list_index0:primer length
list_index1:primer sequence
list_index2:reverse complementary of the sequence
'''
primDict_oneLess = {} #this dictionary will contain the same key, but one character less from the left	
excludedPrimers_dict = {} #keep record of those who were not in the ROI, these reads will be kept
excludedPrimers_dict_oneLess = {}


#first we need to find the smallest primer length; in advance
miniPrimerLength = 1000
with open (primerList_file) as pf:
	for pline in pf:
		pline = pline.rstrip().replace("\n",'').replace(" ",'').replace("\s",'').replace("\t",'').replace("\r",'')
		primerLen = len(pline)
		if  primerLen < miniPrimerLength:
			miniPrimerLength = primerLen

cutOffLen = miniPrimerLength
saveWarnings=[] #this list will save warnings for all the primers are seems to be duplicated at smallest primer size index level
warningFound = False
with open (primerList_file) as pf:		
	if pb:
		tq_primerList=tqdm(total=os.path.getsize(primerList_file),unit='B',unit_scale=True)
	else:
		pass
	for pline in pf:
		if pb:
			tq_primerList.update(len(pline))
		else:
			pass
		pline = pline.rstrip().replace("\n",'').replace(" ",'').replace("\s",'').replace("\t",'').replace("\r",'').upper()
		rc_pline = rc(pline)
				
		primerLen = len(pline)
		if primerLen > 0:
			primerNum = primerNum + 1
			####if user has given target region file, then use only those primers that are in ROI regions and do not trim those who are outside of ROI
			isInROI =False
			if isROI == False and isROIs == False: #we are not doing ROI based trimming and all the primers must be considered.
				isInROI =True
				usedPrimers = usedPrimers + 1
				#print "No ROI based trimming identified, all primers will be considered."
			else:
				for eachTargetSeq in roiSeq_list:
					
					if (pline in eachTargetSeq) : #For plus strand primers
						isInROI =True
						usedPrimers = usedPrimers + 1
						#print "%s primer was found to be inside %s sequence." % (pline, eachTargetSeq)
						break
					elif (rc_pline in eachTargetSeq): #for minus strand primers
						isInROI =True
						usedPrimers = usedPrimers + 1
						#print "%s primer was found to be inside %s sequence." % (pline, eachTargetSeq)
						break					
			
			trimmedPrimer = pline[:cutOffLen] #save the starting 'cutoff' characters
			
			#let's get all the possible mismatches of the trimmed primer sequence
			trimmedPrimer_onemismatch_list = generateMismatch(trimmedPrimer)
			
			for eachMismatched in trimmedPrimer_onemismatch_list:
				if isInROI: #only add the primer into dictionary, if it exist inside the ROI region
					#let's add the data to dictionary
					if not len(eachMismatched) == cutOffLen:
						print "ERROR: The length of index %s is not equal to the cutoff length.\n" % eachMismatched
						os.kill(os.getpid(), 9)
					if eachMismatched in primDict:	
						#primDict.clear() #dictionary to store the trimmed primer sequences and the length
						warningFound = True
						warning=textwrap.dedent('''					
"Search primer is:"
	%s						
"Duplicated Matched primer is:"
	%s
						''') % (pline,primDict[eachMismatched])
						saveWarnings.append(warning)
					else:		
						primerData = []		
						primerData.append(primerLen) #first index
						primerData.append(pline) #second index
						primerData.append(rc_pline) #third index
						primDict[eachMismatched] = primerData
						primDict_oneLess[eachMismatched[:-1]] = primerData
				else:#update the expluded primer's mismatches
					primerData = []		
					primerData.append(primerLen)		
					primerData.append(pline)		
					primerData.append(rc_pline)		
					excludedPrimers_dict[eachMismatched] = primerData
					excludedPrimers_dict_oneLess[eachMismatched[:-1]] = primerData					
			
			if isInROI == False: #update the excluded primer list
				#print "%s was not in the target region, and will not be used." % pline
				#print "Excluding %s" % pline
				excludedPrimers_list.append(pline)
	if pb:
		tq_primerList.close()
	else:
		pass

if warningFound:
	print "WARNING: Duplicated primer sequence found for few primers. Please check the log file for details:\n"
	logging.debug("WARNING: Duplicated primer sequence found for the following primers:\n%s" % "\n".join(saveWarnings))
				
print "Out of total %d primer, only %d will be used for trimming.\n" % (primerNum,usedPrimers)
print "Smallest primer length:%d" %miniPrimerLength
print "Entries in the primer match dictionary, while allowing one mismatch: %d" %len(primDict) 

logging.debug("\n\nOut of total %d primer, only %d will be used for trimming.\n" % (primerNum,usedPrimers))
logging.debug("Smallest primer length:%d" %miniPrimerLength)
logging.debug("Entries in the primer match dictionary, while allowing one mismatch: %d" %len(primDict))
#let's generate a file that will keep record of the primers that are excluded
excludedPrimers_list_FH=open('excludedPrimers_list.txt','w+')
excludedPrimers_list_FH.write("\n".join(excludedPrimers_list))
excludedPrimers_list_FH.write("\n")
logging.debug("Please look for 'excludedPrimers_list.txt' to know about excluded primer sequences.")

#
##
###
####
#####Core pTrim Worker
####
###
##
#

try:
	try:
		os.system('rm -r pTrimBlocks')
	except OSError:
		pass
	os.system('mkdir pTrimBlocks')
except OSError:
	pass	

def pTrim_Core(inputGSP,inputUMI,primDict,primDict_oneLess,excludedPrimers_dict,excludedPrimers_dict_oneLess,outputName,cutOffLen,roiSeq_list,pTrim,minReadLen):
	'''
	Description: This is a core pTrim module. Any change in he functionality should be addressed in this part.
	
	Input Arguments:
	inputGSP : List of Reads from GSP starting reads
	inputUMI : List of Reads from UMI starting reads
	primDict: Dictionary of the primer combinations
	primDict_oneLess: Dictionary of the primer combinations
	excludedPrimers_dict:
	excludedPrimers_dict_oneLess:
	outputName : Will be used to determine the output file names
	'''
	startTime = tm.time()
	
	#part's logging
	logging.debug("Being processed: %s\nReads in GSP reads:%d\nReas in UMI reads:%d" % (outputName,len(inputGSP),len(inputUMI)))
	
	#print roiSeq_list

	###################### Primer Trimming Algo: Working in Version 1.0
	#1) set the smallest search sequence length from the list --> cutOffLen
	#2) Make a dictionary using the 'cutOffLen' : primDict
		#key: trimmed primer sequences
		#value: Length of the actual primer
	#2) for each fastQ sequence line, grab 'cutOffLen' characters from the start --> primerInFastq
	#3) Find these characters of 'primerInFastq' in key of 'primDict'
	#4) Get the length of actual primer in from the 'value' of dictionary --> primLen
	#5) Trim the 'primLen' number of characters from the start of fastq line
	#6) Trim the same 'primLen' characters from the quality line too

	###################### Primer Trimming Algo: Working in Version 1.1: Update
	#>>>updated
	#1) set the smallest search sequence length from the list --> cutOffLen : Not Doing it now
		#2) Make a dictionary using the complete sequence of the primer : primDict
			#key: trimmed primer sequences
			#value: Length of the actual primer
		#2.1) While making the dictionary, make all possible combinations with one mismatch at every location
	#2) for each fastQ sequence line, grab 'cutOffLen' characters from the start --> primerInFastq
	#3) Find these characters of 'primerInFastq' in key of 'primDict'
	#4) Get the length of actual primer in from the 'value' of dictionary --> primLen
	#5) Trim the 'primLen' number of characters from the start of fastq line
	#6) Trim the same 'primLen' characters from the quality line too



	#sys.exit()	
	#In a block in fastq
	#line1: Index+TAG+INFO
	#line2: Sequence
	#line3: Strand
	#line4: Quality	
	lineNumber = 0 #there are 4 lines in a block in fastq file

	####error rate
	noPrimer = 0
	errorRead = 0
	readCount = 0
	trimmedReads = 0

	###let's write the output file
	#outFQ_FH = gzip.open(outFQ_filename,'w+')
	outFQ_filename_part=outputName+'_primerTrimmed.fastq'
	outFQ_FH_part = open(outFQ_filename_part,'w+')

	#define a list that will hold the trimmed reads
	writeChunk = []
	droppedChunk = []
	droppedChunk_paired = []
	doubleSSE_reads=0
	trimmedSSE_umiContainingReads=0
	trimmedSSE_gspContainingReads=0
	totalPairedReads=0
	#maxDoubleSSE_pos=0
	cycles=0
	oneInsertUMI=0
	oneDelUMI=0
	fullUMI=0
	####For saving the UMI fastq
	umiFastq=[]
	umiSame_inR1R2=0
	umi_inGSP=0	
	shortReads_discared=0

	if i_args.sequencing == 'single':
		for line in inputGSP:				
				
			lineNumber = lineNumber + 1
			#trim any possible extra characters
			line = line.replace("\r",'')
			
			if line[-1:] == "\n" or line[-1:] == " " or line[-1:] == "\t" or line[-1:] == "\s":
				line = line[:-1]

			if line[:1] == "\n" or line[:1] == " " or line[:1] == "\t" or line[:1] == "\s":
				line = line[1:]
				
			if lineNumber == 1:
				Firstline = line
			elif lineNumber == 2:
				fullRead = line
				seqLength=len(fullRead)
				readCount = readCount + 1
				primerInFastq = line[trimExtra:cutOffLen] #selected the characters only to the length of smallest primer
				#get the length of the primer
				try:
					primLen = primDict[primerInFastq][0]
					#trim the fastq sequence according to the primer length
					trimLength = trimExtra+primLen
					trimmed_read = line[trimLength:]
					trimQual = True #by default, trim the quality line too
					#print primerInFastq
					isReadShort= False
					trimmedReads = trimmedReads + 1
				except KeyError:
					#for some sequences, the 'primerInFastq' contains one extra character on the right side that leads to the mismatch, let's try to match after that
					try:
						primerInFastq_oneless = primerInFastq[1:]
						primLen = primDict_oneLess[primerInFastq_oneless][0]
						#trim the fastq sequence according to the primer length	
						trimLength = trimExtra+primLen	
						trimmed_read = line[(trimLength-1):]	#trim one less base as it was already short of one base
						trimQual = True #by default, trim the quality line too	
						#print "Found in the second try"
						isReadShort= True
						trimmedReads = trimmedReads + 1
					except KeyError:
						#now, a 'primerInFastq' could not be found in the index due to following two reasons:
						#1) It was not in ROI
						#2) There was an error in the 'primerInFastq' sequence
						#We want to keep 1) and Discard 2)
						#We already have a dictionary of those primers who were not in ROI, we will use that dictionary

						if (primerInFastq in excludedPrimers_dict) or (primerInFastq_oneless in excludedPrimers_dict_oneLess):
							read_isInROI = True
							noPrimer = noPrimer + 1						
						else:
							read_isInROI = False
							droppedRead = line
							errorRead = errorRead + 1
						trimQual = False #because there will not be any read trimming for this block, let's not trim the quality line too
						
			elif lineNumber == 3:
				Thirdline = line
			elif (lineNumber == 4) and (trimQual == True) and (minReadLen < seqLength): #this is where we will decide, whether or not to write the block, depending on the error in primer#write only those who matched
				if isReadShort:
					trimmer_qual = line[trimLength-1:]
				else:
					trimmer_qual = line[trimLength:]
				#let's write all the data from this block
				if pTrim:
					writeChunk.append(Firstline)
					writeChunk.append(trimmed_read)
					writeChunk.append(Thirdline)
					writeChunk.append(trimmer_qual)
				else:
					writeChunk.append(Firstline)
					writeChunk.append(fullRead)
					writeChunk.append(Thirdline)
					writeChunk.append(line)					
				
				lineNumber = 0 #reset lineNumber to o after fourth line, so that we always have sense of line inside the block.
			elif lineNumber == 4 and trimQual == False and (minReadLen < seqLength):
				if read_isInROI: #Do not drop the reads whose primers were not in ROI
					#let's write all the data from this block, but without any trimming
					
					writeChunk.append(Firstline)
					writeChunk.append(fullRead)
					writeChunk.append(Thirdline)
					writeChunk.append(line)
				else:
					if i_args.savedropped_reads == '1':
						droppedChunk.append(Firstline)
						droppedChunk.append(droppedRead)
						droppedChunk.append(Thirdline)
						droppedChunk.append(line)
			else: #just to count short length dropped reads
				if (minReadLen < seqLength):
					shortReads_discared+=1
				lineNumber = 0 #reset lineNumber to o after fourth line, so that we always have sense of line inside the block.
		primerErrorRate = float(errorRead) / float(readCount)*100	
		outFQ_FH_part.write('\n'.join(writeChunk))
		outFQ_FH_part.write('\n')

		endTime = tm.time()
		totalTime = endTime - startTime	
		stats=textwrap.dedent('''\
		
		*******************************************STATS***********************************
		Sample Part: %s
		SequencingMode='Single'
		Total Reads: %d
		Trimmed Reads: %d
		Trimmed percentage: %f
		Reads not trimmed: %d
		Reads dropped due to Error: %d
		Dropped reads percentage: %f
		Reads dropped due to short read length: %d 
		Reads dropped due to short percentage: %f%%
		Execution Time: %f Seconds
		Final total reads in trimmed fastq: %d		
		''')%(outputName,
		readCount,
		trimmedReads,
		((float(trimmedReads)/float(readCount))*100),
		noPrimer,
		errorRead,
		primerErrorRate,
		shortReads_discared,
		((float(shortReads_discared)/float(readCount))*100),		
		totalTime,
		(len(writeChunk)/4)
		)		
		#close the output fastq file
		outFQ_FH_part.close()

		#let's write the dropped reads if user has asked for it
		if i_args.savedropped_reads == '1':
			dropoutFQ_filename=outputName+'_dropped.fastq'
			dropoutFQ_FH = open(dropoutFQ_filename,'w+')
			dropoutFQ_FH.write("\n".join(droppedChunk))
			dropoutFQ_FH.write("\n")
			dropoutFQ_FH.close()
		
		#write the stats for this block
		
		writeBlockStats=open("%s.stats" %outputName,'w+')
		writeBlockStats.write(stats)
		writeBlockStats.close()
		
		#logging.debug(stats)
		#WriteThe job completion file
		writeBlockCompletion=open("%s.completed" %outputName,'w+')
		writeBlockCompletion.write('completed at %s' % tm.strftime("%b %d %Y %H:%M:%S", tm.localtime()))
		writeBlockCompletion.close()
		#print ">>>>part %s written." %outputName
		#logging.debug(">>>>part %s written." %outputName)
	else: #this is 'paired' end sequencing, processing
		#let's first read the paired fastq file and make a dictionary
		
		'''
		dictionary structure:
		key: fastq id
		value: list
		list_index_0:  line1 (fastqID)
		list_index_0:  line2 (read)
		list_index_0:  line3 (+)
		list_index_0:  line4 (qualityLine)
		'''
				
		pairedFastq_dict = {} #this dictionary conatians the fasqdata of each paired read in the following manner:
		#key: fastqid
		#value=[] ; 6 element list
		#index 0:first line of read in fastq (fastqread header)
		#index 1:second line of read in fastq (sequence)
		#index 2:third line of read in fastq (+)
		#index 3:fourth line of read in fastq (quality line)
		#index 4:UMI of the read
		#index 5:SSE of the read
		
		readData = []
		pairedReadNum = 0
		lineNumbers=0 #this will be used for tqdm progress bar while trimming the primers as the number of line in paired reads are same as in GSP containing read file; so that we have accurate tracking.
		lineCounter=0
		###also, we want to make sure that UMI of this read is not of very poor quality
		dropReads_poorQualUMI=0
		poorQualityBases={'!':0,'"':1,'#':2,'$':3,'%':4,'&':5,"'":6,'(':7,')':8,'*':9,'+':10}
		
		for eachLine in inputUMI:
			lineNumbers+=1
			lineCounter += 1
			eachLine = eachLine.rstrip().replace("\n",'')
			#if lineCounter == 1:
			if eachLine.startswith('@'):
				readData = [] #empty the list for the next block
				lineCounter = 1				
				'''
				if not eachLine[:1] == '@':
					print "\n\nERROR: Inconsistency of data in paired read file. Paired reads are not in multiple of 4, hence not every fourth line is starting with @."
					logging.debug("\n\nERROR: Inconsistency of data in paired read file. Paired reads are not in multiple of 4, hence not every fourth line is starting with @.")
					os.kill(os.getpid(), 9)
				'''
				readData.append(eachLine)
				#readID = eachLine.split("\t")[0].split("\s")[0]
				readID = eachLine.split(" ")[0]
			elif lineCounter == 2:
				totalPairedReads+=1
				'''
				####need to do following here
				####Find double sse reads and drop them
				####Find UMI and save them
				####Find UMI+SSE+SSEtail and trim the read
				####Save the identified respective SSE sequence and save it
									
				#let's do the UMI and SSE treatment first for the paired read
				print "Paired read is: %s" % eachLine
				print "identified UMI: %s" % eachLine[umiStartPos_index:umiEndPos_index]
				print "identified SSE: %s" % eachLine[sseStartPos_index:sseEndPos_index]
				print "identified tail: %s" % eachLine[sseEndPos_index:sseEndPos_index+sseTail_count]
				sys.exit()
				'''
				####TO do >>>> Find the double index reads
				#Algo:
				#1) Keep the first letetr of SSE sequence constant; sseFirstLetter
				#2)make pattern string: 
					#<AnyCharacter*UMI_length><sseFirstLetter><anycharacter*anyNumberOfTime><SSE>
				#3) If above is found in any read
				#4) Find the index of match, and see if it exceeds the 50% of read length, if yes; keep this read (its reference sequence), if no, discard it
				#5) count the discard count for stats by with and without length filter, if without length filter, not many reads are dropped, use that method.
				
				'''
				#for stats purpose
				#ATTGGAGTCC;quaseq
				#AACCGCCAGGAGT: archer
				try:#find full length UMI
					
					re.search('.........A.C..C.......',eachLine).start()
					fullUMI+=1
				except AttributeError:
					try:
						
						re.search('........A.C..C.......',eachLine).start()
						oneDelUMI+=1
					except AttributeError:
						try:
							
							re.search('..........A.C..C.......',eachLine).start()
							oneInsertUMI+=1
						except AttributeError:
							pass
				'''
				
				UMI=eachLine[umiStartPos_index:umiEndPos_index]
				SSE=eachLine[sseStartPos_index:sseEndPos_index]
				trimLength_endIndex=sseEndPos_index+sseTail_count
				trimmedRead=eachLine[trimLength_endIndex:]			
				#let's check if this read contains the double SSE
				#we always assume second SSE occurance does not contain any error				
				try:
					secondSSE_index=re.search(sseSeq,trimmedRead).start()
					doubleSSE_reads+=1
					isSecondSSE=True
					#we can find out such read that contains second sse sequence but are actually reference sequence, by finding the position of second sse in the read; ususally if the sse is found at last of position (past 70%), its reference sequence
					#but because the number of such is insignificantly low, its better to skip this filter
					#and filter out any read for wich there are double sse sequences
					''' #Future possibility: not doing it right now
					trimmedLen=len(trimmedRead)
					print "Trimmed read length: %d" %trimmedLen
					
					if secondSSE_index <= int(trimmedLen/1.6):
						isSecondSSE=True
					else:
						isSecondSSE=False
						if secondSSE_index > maxDoubleSSE_pos:
							maxDoubleSSE_pos = secondSSE_index
							if maxDoubleSSE_pos > 114:
								sys.exit()						
					print "is Second SSE:%r" %isSecondSSE
					print "Maximum second SSE position: %d" % maxDoubleSSE_pos
					'''
				except AttributeError:
					#no double sse found
					pass
					isSecondSSE=False
				
				#save the trimmed read instead
				readData.append(trimmedRead)
				
			elif lineCounter == 3:
				readData.append(eachLine)
			elif lineCounter == 4:
				if not isSecondSSE: #save paired read only if there is no double SSE; if we drop paired-read here, the respective R1/R2 will automatically be dropped in later step because we are droping those reads that are not properly paired.			
					#store all the data from this block
					#need to trim the quality line too
					readData.append(eachLine[trimLength_endIndex:])
					#add UMI,SSE data as 5th and 6th element
					readData.append(UMI) #add UMI
					readData.append(SSE) #add SSE
					readData.append(eachLine[umiStartPos_index:umiEndPos_index]) #add UMI quality
					pairedFastq_dict[readID] = readData
					pairedReadNum += 1
					trimmedSSE_umiContainingReads+=1	
		processedPairReads = [] #this will hold the paired-trimmed reads data, using list so that order is maintained
		#when a read is trimmed from FASTQ, respective read will also be trimmed and extended (not appended) in the 'processedPairReads'; later whole list will be written to the output
		
		#let's start the priner trimming
		#start processing the reads that start with GSP
		unpairedReads = 0 #this is the counter for the reads that do not have respective pairs
		tmpCount = 0
		PairedReadTrim_count = 0
		noPairedReadTrim_count_total = 0
		noPairedReadTrim_count_noPrimer = 0
		noPairedReadTrim_count_noPrimer_noPair=0
		noPairedReadTrim_count_noROI = 0
		sseInGSPread=0
		lineNumber=0
		
		#print "\nReading GSP start read fastq and processing the primer trimming."
		for line in inputGSP:
			lineNumber = lineNumber + 1
			#trim any possible extra characters
			line = line.replace("\r",'')
			
			if line[-1:] == "\n" or line[-1:] == " " or line[-1:] == "\t" or line[-1:] == "\s":
				line = line[:-1]

			if line[:1] == "\n" or line[:1] == " " or line[:1] == "\t" or line[:1] == "\s":
				line = line[1:]
				
			if line.startswith('@'):
				lineNumber=1
				Firstline = line
			elif lineNumber == 2:
				fullRead = line
				completeLine = line
				readCount = readCount + 1
				primerInFastq = line[trimExtra:cutOffLen] #selected the characters only to the length of smallest primer
				screenedOneLess=False
				#get the length of the primer
				try:
					primLen = primDict[primerInFastq][0]
					#trim the fastq sequence according to the primer length
					trimLength = trimExtra+primLen
					trimmed_read = line[trimLength:]
					trimQual = True #by default, trim the quality line too
					#print primerInFastq
					isReadShort= False
					trimmedReads = trimmedReads + 1
				except KeyError:
					#for some sequences, the 'primerInFastq' contains one extra character on the right side that leads to the mismatch, let's try to match after that
					try:
						screenedOneLess = True
						primerInFastq_oneless = primerInFastq[1:]
						primLen = primDict_oneLess[primerInFastq_oneless][0]
						#trim the fastq sequence according to the primer length	
						trimLength = trimExtra+primLen	
						trimmed_read = line[(trimLength-1):]	#trim one less base as it was already short of one base
						trimQual = True #by default, trim the quality line too	
						#print "Found in the second try"
						isReadShort= True
						trimmedReads = trimmedReads + 1
					except KeyError:
						#now, a 'primerInFastq' could not be found in the index due to following two reasons:
						#1) It was not in ROI
						#2) There was an error in the 'primerInFastq' sequence
						#We want to keep 1) and Discard 2)
						#We already have a dictionary of those primers who were not in ROI, we will use that dictionary

						if (primerInFastq in excludedPrimers_dict) or (primerInFastq_oneless in excludedPrimers_dict_oneLess):
							read_isInROI = True
							#fullRead = line
							noPrimer = noPrimer + 1						
						else:
							read_isInROI = False
							droppedRead = line
							errorRead = errorRead + 1
						trimQual = False #because there will not be any read trimming for this block, let's not trim the quality line too
				
			elif lineNumber == 3:
				Thirdline = line
				tmpCount=1
			elif (lineNumber == 4) and (trimQual == True): #this is where we will decide, whether or not to write the block, depending on the error in primer#write only those who matched				
				dropNoPrimerNopair_R2_decision=False
				if isReadShort:
					trimmer_qual = line[trimLength-1:]
				else:
					trimmer_qual = line[trimLength:]
							
				#time to process the paired read;
				#we need to trim this read's respective paired read also
				
				#first get the fastqid of this block's read
				#block_readID = Firstline.split(" ")[0] #doing direct retrieval
				#now, let's get the data of paired read
				try:
					#let's trim GSP from the paired reads; note that at this point paired read is already trimmed for UMI+SSE
					paired_line1,paired_line2,paired_line3,paired_line4,UMI,SSE,UMI_qual= pairedFastq_dict.pop(Firstline.split(" ")[0])
					'''
					try:
						paired_line1,paired_line2,paired_line3,paired_line4,UMI,SSE,UMI_qual= pairedFastq_dict.pop(Firstline.split(" ")[0])
					except ValueError:
						#print "\n\n"
						print pairedFastq_dict.pop(Firstline.split(" ")[0])
						print "Hi"
						#print "\n\n"
						raise
					'''
					#first thing; trim SSE and UMI from GSP reads
					#for this, find the location of SSE in GSP read, and trim everything after that
					#if no SSE is found, means there is no SSE in that GSP read
					
					#method to find SSE in GSP-containing read;
						#1) Find the end position of reverse complement  of starting 10 bases of UMI-contatining read
						#2) If found, trim everything after that, that's it
						#3) If not found, try to find the start of reverseComplement of real SSE (we do not expect error in SSE of GSP-conataining read)
						#4) If found, discard everything after that
						#) if not found, don't trim, most probably there is no SSE in the read
					try:			
						pairedReadOverlap_pos = re.search(rc(paired_line2[:10]),trimmed_read).end() #####This Line is triggering the KeyError; if not handled will falsly report as unpaired read
						#print "\nSSE contatining read: %s" %trimmed_read
						#print "SSE contatining read quality: %s" %trimmer_qual							
						#before trimming the read, check if it contains the UMI
						umiFrom_gspRead = rc(trimmed_read[(pairedReadOverlap_pos+sseTail_count+lengthSSE):(pairedReadOverlap_pos+sseTail_count+lengthSSE+lengthUMI)])
						umiFrom_gspRead_qual=trimmer_qual[(pairedReadOverlap_pos+sseTail_count+lengthSSE):(pairedReadOverlap_pos+sseTail_count+lengthSSE+lengthUMI)]
						#print umiFrom_gspRead
						if not len(umiFrom_gspRead) == lengthUMI: #UMI not found in GSP read or its partial
							selectedUMI=UMI #UMI from the UMI-containing read is selected to be saved
							selectedUMI_qual=UMI_qual
						else: #UMI found in GSP read, let's select it as UMI
							selectedUMI=umiFrom_gspRead #UMI from the GSP read is selected
							selectedUMI_qual=umiFrom_gspRead_qual
							umi_inGSP+=1
					
						#just for stats
						if UMI==umiFrom_gspRead:
							umiSame_inR1R2+=1
						else:
							pass
							#print "\nR2 UMI is: %s" % UMI
							#print "R1 UMI is: %s" % umiFrom_gspRead
							#logging.debug("\nR2 UMI is: %s" % UMI)
							#logging.debug("R1 UMI is: %s" % umiFrom_gspRead)
						
						
						trimmed_read=trimmed_read[:pairedReadOverlap_pos]
						trimmer_qual=trimmer_qual[:pairedReadOverlap_pos]
						trimmedSSE_gspContainingReads+=1
						if (minReadLen > len(trimmed_read)): #this trimmed read is shorted than the cutoff; do not write it
							shortReads_discared+=1
							continue
					except (AttributeError,KeyError) as e:
						#read overlap based SSE could not be detected, let's use the reverse comp of origional SSE
						try:
							SSE_pos = trimmed_read.index(reverseSSE_real,0)
							#SSE_pos=SSE_pos-sseTail_count #account for SSE tail if any

							#before trimming the read, check if it contains the UMI
							umiFrom_gspRead = rc(trimmed_read[(SSE_pos+lengthSSE):(SSE_pos+lengthSSE+lengthUMI)])
							umiFrom_gspRead_qual=trimmer_qual[(SSE_pos+lengthSSE):(SSE_pos+lengthSSE+lengthUMI)]
							if not len(umiFrom_gspRead) == lengthUMI: #UMI not found in GSP read or its partial
								selectedUMI=UMI #UMI from the UMI-containing read is selected to be saved
								selectedUMI_qual=UMI_qual
							else: #UMI found in GSP read, let's select it as UMI
								selectedUMI=umiFrom_gspRead #UMI from the GSP read is selected
								selectedUMI_qual=umiFrom_gspRead_qual
								umi_inGSP+=1

							#just for stats
							if UMI==umiFrom_gspRead:
								umiSame_inR1R2+=1							
							
							trimmed_read=trimmed_read[:SSE_pos]
							if (minReadLen > len(trimmed_read)): #this trimmed read is shorted than the cutoff; do not write it
								shortReads_discared+=1
								continue
							trimmer_qual=trimmer_qual[:SSE_pos]
							trimmedSSE_gspContainingReads+=1
						except ValueError:
							#Do not trim the read
							selectedUMI=UMI
							selectedUMI_qual=UMI_qual
					
					'''
					#for debugging
					try:
						reverseSSE=rc(SSE)
						reverseSSE_gspRead=reverseSSE
						sseInGSPread_index=re.search(reverseSSE,trimmed_read).start()
						sseInGSPread_index=sseInGSPread_index- sseTail_count#consider if any SSE tail is present
						r1UMI=rc(trimmed_read[(sseInGSPread_index+11):(sseInGSPread_index+23)])
						trimmed_read=trimmed_read[:sseInGSPread_index]
						r1UMI_ql=trimmer_qual[(sseInGSPread_index+11):(sseInGSPread_index+23)] #use non SSE+UMI trimmed quality read for this purpose
						reverseSSE_real_ql=trimmer_qual[(sseInGSPread_index+1):(sseInGSPread_index+11)]#use non SSE+UMI trimmed quality read for this purpose
						trimmer_qual=trimmer_qual[:sseInGSPread_index]
						
						sseInGSPread+=1
						sseInR1=True
					except AttributeError:
						try:
							reverseSSE_gspRead=reverseSSE_real
							sseInGSPread_index=re.search(reverseSSE_real,trimmed_read).start()
							sseInGSPread_index=sseInGSPread_index- sseTail_count#consider if any SSE tail is present
							r1UMI=rc(trimmed_read[(sseInGSPread_index+11):(sseInGSPread_index+23)])						
							r1UMI_ql=trimmer_qual[(sseInGSPread_index+11):(sseInGSPread_index+23)]
							reverseSSE_real_ql=trimmer_qual[(sseInGSPread_index+1):(sseInGSPread_index+11)]
						except AttributeError:
							reverseSSE_gspRead='-'
							r1UMI='-'
							r1UMI_ql='-'
							reverseSSE_real_ql='-'
						sseInR1=False
							
					print "\nFirst complete read: %s" % completeLine
					print "First trimmed read: %s" % trimmed_read
					print "First trimmed quality: %s" % trimmer_qual
					print "Its paired read: %s" % paired_line2
					print "Its paired quality: %s" % paired_line4
					print "\tSSE in UMI-read: %s" %reverseSSE
					print "\tSSE in GSP-read: %s" %reverseSSE_gspRead
					print "\tQUL in GSP-read: %s" %reverseSSE_real_ql
				
					print sseInR1
					print "R2 QUL: %s" %paired_line4[:len(UMI)]
					print "R2 UMI: %s" %UMI
					print "R1 UMI: %s" %r1UMI
					print "R1 new UMI: %s" %umiFrom_gspRead					
					print "R1 QUL: %s" %r1UMI_ql
					if cycles==25:
						cycles+=25
						sys.exit()
					'''
					###let's check if seelcted UMI is not of very poot quality
					if '#' in selectedUMI_qual:
						dropReads_poorQualUMI+=1	
						#lineNumber = 0					
						continue #if so, just skip this read to write
					#get the last 10 characters of GSP
					#get ist reversecpmplementry, as its the paired read; try to find in the paired read
					#or get the reverse complement of primer and use its' starting 10 bases to find in paired read
					#use the dictionary to save the processing power

					if screenedOneLess:
						try:
							reverseC_primer=primDict_oneLess[primerInFastq_oneless][2]
							primerStartIndex=paired_line2.find(reverseC_primer[:10])
						except KeyError:
							primerStartIndex=-1
						'''
						last10_primerStartIndex = paired_line2.find(reverseC_primer[-10:]) #@@@@
						
						#to get the actual index
						# last10indexIn_pairedRead) - (primer length - 10)
						primerStartIndex = last10_primerStartIndex - (primDict_oneLess[primerInFastq_oneless][0] - 10)
						'''
						#primerStartIndex=paired_line2.find(reverseC_primer[:10])
					else:
						try:
							reverseC_primer=primDict[primerInFastq][2]
							primerStartIndex=paired_line2.find(reverseC_primer[:10])
						except KeyError:
							primerStartIndex=-1
						'''
						last10_primerStartIndex = paired_line2.find(reverseC_primer[-10:]) #@@@@
						primerStartIndex = last10_primerStartIndex - (primDict[primerInFastq][0] - 10)
						'''				
					#if reverse complementary primer is absent in paired read. primerStartIndex == -1
					#now, there can be scenario, when less then complete primer is present in paired read; because the total read length has to be around 150, in such case, reverse complementary of the R1_trimmed read must 
					#be present in the paired read. in such case, we need to do following to find out, from where primer is starting
						#1) find the index of reverse complementary of 'R1_trimmed read' : lets s x
						#2) add the length of 'R1_trimmed read' : x+len_r1 = newIdx
						#3) trimmed pair read = pairedRead[:newIdx]
					if primerStartIndex < 0:
						reverseC_trimmed_read = rc(trimmed_read)
						try:
							firstReadIndex_inPaired = paired_line2.find(reverseC_trimmed_read[:10])
							#if, the trimmed read in not present in the paired read, that means there is GSP site in the paired read, hence does not require any trimming
						except KeyError:
							firstReadIndex_inPaired=-1

						if saveNoPrimer_R2: #we can save such R2 reads that has no 10 base overlapping GSP primer 
						#paired_line1,paired_line2,paired_line3,paired_line4
							saveNoPrimer_R2_list.append(paired_line1)
							saveNoPrimer_R2_list.append(paired_line2)
							saveNoPrimer_R2_list.append(paired_line3)
							saveNoPrimer_R2_list.append(paired_line4)
						
						if firstReadIndex_inPaired == -1:
							paired_line2_trimmed = paired_line2 #no trimming
							
							if saveNoPrimerNopair_R2: #we can save such R2 reads that has no 10 base overlapping GSP primer 
								saveNoPrimerNopair_R2_list.append(paired_line1)
								saveNoPrimerNopair_R2_list.append(paired_line2)
								saveNoPrimerNopair_R2_list.append(paired_line3)
								saveNoPrimerNopair_R2_list.append(paired_line4)
							
							if dropNoPrimerNopair_R2:
								dropNoPrimerNopair_R2_decision=True

							if (minReadLen > len(paired_line2_trimmed)): #this trimmed read is shorted than the cutoff; do not write it
								shortReads_discared+=1
								continue
								
							#print "\n\n\n\nNo paired read trimming."
							noPairedReadTrim_count_total += 1
							noPairedReadTrim_count_noPrimer_noPair+=1
							noPairedReadTrim_count_noPrimer += 1								
							
							#we need need not to trim the quality line here
							trimmed_PairedQualityLine=paired_line4
							
						else: #trim based on the trimmedFirst read
							trimmedPairReadLen = firstReadIndex_inPaired + len(reverseC_trimmed_read)
							paired_line2_trimmed = paired_line2[:trimmedPairReadLen]
							if (minReadLen > len(paired_line2_trimmed)): #this trimmed read is shorted than the cutoff; do not write it
								shortReads_discared+=1								
								continue
							PairedReadTrim_count += 1
							noPairedReadTrim_count_noPrimer += 1
							
							#now we need to trim the quality line too							
							trimmed_PairedQualityLine=paired_line4[:len(paired_line2_trimmed)]
					else: #trim based on the reverse complementary GSP site
						#print "\n\n\n\nRC GSP based trimming."
						paired_line2_trimmed = paired_line2[:primerStartIndex]
						if (minReadLen > len(paired_line2_trimmed)): #this trimmed read is shorted than the cutoff; do not write it
							shortReads_discared+=1							
							continue
						PairedReadTrim_count += 1
						
						#now we need to trim the quality line too
						trimmed_PairedQualityLine=paired_line4[:len(paired_line2_trimmed)]										
	
					'''
					#for debugging
					print "index is: %d" % primerStartIndex
					print "\nOne less dictionary: %s" % str(screenedOneLess)
					
					if screenedOneLess:
						print primDict_oneLess[primerInFastq_oneless]
					else:
						print primDict[primerInFastq]
					
					#print primDict_oneLess[primerInFastq_oneless]
					
					
					print "\nR1 complete read: %s" %completeLine
					print "\nR1 trimmed read: %s" %trimmed_read
					print "R2 read: %s" % paired_line2
					print "Trimmed paried read: %s" % paired_line2_trimmed
					
					tmpCount += 1
					if tmpCount == 50:
						sys.exit()
					'''

					if not dropNoPrimerNopair_R2_decision:
						if isAppendUMI:
							#to update the fastq ID with UMI
							toReplaceSpace=":%s#%s " %(selectedUMI,selectedUMI_qual)						
							
							if pTrim:
								#let's write all the first read (GSP starting read) data from this block
								writeChunk.append(Firstline.replace(" ",toReplaceSpace))
								writeChunk.append(trimmed_read)
								writeChunk.append(Thirdline)
								writeChunk.append(trimmer_qual)
								
								#let's store the trimmed paired read data
								processedPairReads.append(paired_line1.replace(" ",toReplaceSpace))
								processedPairReads.append(paired_line2_trimmed)
								processedPairReads.append(paired_line3)
								processedPairReads.append(trimmed_PairedQualityLine)
							else:
								#let's write all the first read (GSP starting read) data from this block
								writeChunk.append(Firstline.replace(" ",toReplaceSpace))
								writeChunk.append(fullRead)
								writeChunk.append(Thirdline)
								writeChunk.append(line)
								
								#let's store the nontrimmed paired read data
								processedPairReads.append(paired_line1.replace(" ",toReplaceSpace))
								processedPairReads.append(paired_line2)
								processedPairReads.append(paired_line3)
								processedPairReads.append(paired_line4)
						else:
							#let's write all the first read (GSP starting read) data from this block
							if pTrim:
								writeChunk.append(Firstline)
								writeChunk.append(trimmed_read)
								writeChunk.append(Thirdline)
								writeChunk.append(trimmer_qual)
								
								#let's store the trimmed paired read data
								processedPairReads.append(paired_line1)
								processedPairReads.append(paired_line2_trimmed)
								processedPairReads.append(paired_line3)
								processedPairReads.append(trimmed_PairedQualityLine)
							else:
								writeChunk.append(Firstline)
								writeChunk.append(fullRead)
								writeChunk.append(Thirdline)
								writeChunk.append(line)
								
								#let's store the trimmed paired read data
								processedPairReads.append(paired_line1)
								processedPairReads.append(paired_line2)
								processedPairReads.append(paired_line3)
								processedPairReads.append(paired_line4)							
							
							#now that we have got the UMI sequence, let's save the UMI data
							umiFastq.append(Firstline)
							umiFastq.append(selectedUMI)
							umiFastq.append(Thirdline)
							umiFastq.append(selectedUMI_qual)
				except KeyError:
					#because there is no respective paired read found for this read, let's just drop both the reads
					#print "Unpaired Read drop."
					unpairedReads += 1
					continue
								
				#lineNumber = 0 #reset lineNumber to o after fourth line, so that we always have sense of line inside the block.
				
			elif (lineNumber == 4) and (trimQual == False): #non trimmed reads
				if read_isInROI: #Do not drop the reads whose primers were not in ROI
					###let's trim the SSE; excatly same concept as above, so removing all the comments from here
					trimmer_qual = line
					trimmed_read = fullRead
					if (minReadLen > len(trimmed_read)): #this trimmed read is shorted than the cutoff; do not write it
						shortReads_discared+=1						
						continue					
					#now, let's get the data of paired read
					try:
						paired_line1,paired_line2,paired_line3,paired_line4,UMI,SSE,UMI_qual= pairedFastq_dict.pop(Firstline.split(" ")[0])

						try:
							pairedReadOverlap_pos = re.search(rc(paired_line2[:10]),trimmed_read).end()
							
							#before trimming the read, check if it contains the UMI
							umiFrom_gspRead = rc(trimmed_read[(pairedReadOverlap_pos+sseTail_count+lengthSSE):(pairedReadOverlap_pos+sseTail_count+lengthSSE+lengthUMI)])
							umiFrom_gspRead_qual=trimmer_qual[(pairedReadOverlap_pos+sseTail_count+lengthSSE):(pairedReadOverlap_pos+sseTail_count+lengthSSE+lengthUMI)]
							if not len(umiFrom_gspRead) == lengthUMI: #UMI not found in GSP read or its partial
								selectedUMI=UMI #UMI from the UMI-containing read is selected to be saved
							else: #UMI found in GSP read, let's select it as UMI
								selectedUMI=umiFrom_gspRead #UMI from the GSP read is selected
								selectedUMI_qual=umiFrom_gspRead_qual							
								
							trimmed_read=trimmed_read[:(pairedReadOverlap_pos)]
							if (minReadLen > len(trimmed_read)): #this trimmed read is shorted than the cutoff; do not write it
								shortReads_discared+=1								
								continue
							trimmer_qual=trimmer_qual[:(pairedReadOverlap_pos)]
							trimmedSSE_gspContainingReads+=1
						except (KeyError,ttributeError) as e:
						#read overlap based SSE could not be detected, let's use the reverse comp of origional SSE
							try:
								SSE_pos = trimmed_read.index(reverseSSE_real,0)
								#SSE_pos=SSE_pos-sseTail_count #account for SSE tail if any
								
								#before trimming the read, check if it contains the UMI
								umiFrom_gspRead = rc(trimmed_read[(SSE_pos+lengthSSE):(SSE_pos+lengthSSE+lengthUMI)])
								umiFrom_gspRead_qual=trimmer_qual[(SSE_pos+lengthSSE):(SSE_pos+lengthSSE+lengthUMI)]
								#print umiFrom_gspRead
								if not len(umiFrom_gspRead) == lengthUMI: #UMI not found in GSP read or its partial
									selectedUMI=UMI #UMI from the UMI-containing read is selected to be saved
									selectedUMI_qual=UMI_qual
								else: #UMI found in GSP read, let's select it as UMI
									selectedUMI=umiFrom_gspRead #UMI from the GSP read is selected
									selectedUMI_qual=umiFrom_gspRead_qual
								
								trimmed_read=trimmed_read[:SSE_pos]
								if (minReadLen > len(trimmed_read)): #this trimmed read is shorted than the cutoff; do not write it
									shortReads_discared+=1									
									continue
								trimmer_qual=trimmer_qual[:SSE_pos]
								trimmedSSE_gspContainingReads+=1
							except ValueError:
								#Do not trim the read
								selectedUMI=UMI
								selectedUMI_qual=UMI_qual
						
						###let's check if seelcted UMI is not of very poot quality
						if '#' in selectedUMI_qual or '$' in  selectedUMI_qual:
							dropReads_poorQualUMI+=1
							#lineNumber = 0							
							continue #if so, just skip this read to write

							
						#time to process the paired read;
						#we need to keep this read's respective paired read also
						#now, let's get the data of paired read
						
						#get the last 10 characters of GSP
						#get ist reversecpmplementry, as its the paired read; try to find in the paired read
						#or get the reverse complement of primer and use its' starting 10 bases to find in paired read
						#use the dictionary to save the processing power
						if screenedOneLess:
							try:
								reverseC_primer=primDict_oneLess[primerInFastq_oneless][2]
								primerStartIndex=paired_line2.find(reverseC_primer[:10])
							except KeyError:
								primerStartIndex=-1
							'''
							last10_primerStartIndex = paired_line2.find(reverseC_primer[-10:]) #@@@@
							
							#to get the actual index
							# last10indexIn_pairedRead) - (primer length - 10)
							primerStartIndex = last10_primerStartIndex - (primDict_oneLess[primerInFastq_oneless][0] - 10)
							'''
							#primerStartIndex=paired_line2.find(reverseC_primer[:10])
						else:
							try:
								reverseC_primer=primDict[primerInFastq][2]
								primerStartIndex=paired_line2.find(reverseC_primer[:10])
							except KeyError:
								primerStartIndex=-1

							'''
							last10_primerStartIndex = paired_line2.find(reverseC_primer[-10:]) #@@@@
							primerStartIndex = last10_primerStartIndex - (primDict[primerInFastq][0] - 10)
							'''
						primerStartIndex=paired_line2.find(reverseC_primer[:10])
						#if reverse complementary primer is absent in paired read. primerStartIndex == -1
						#now, there can be scenario, when less then complete primer is present in paired read; because the total read length has to be around 150, in such case, reverse complementary of the R1_trimmed read must 
						#be present in the paired read. in such case, we need to do following to find out, from where primer is starting
							#1) find the index of reverse complementary of 'R1_trimmed read' : lets s x
							#2) add the length of 'R1_trimmed read' : x+len_r1 = newIdx
							#3) trimmed pair read = pairedRead[:newIdx]
						
						if primerStartIndex < 0:
							reverseC_trimmed_read = rc(trimmed_read)
							firstReadIndex_inPaired = paired_line2.find(reverseC_trimmed_read[:10])
							#if, the trimmed read in not present in the paired read, that means there is GSP site in the paired read, hence does not require any trimming
							
							if saveNoPrimer_R2: #we can save such R2 reads that has no 10 base overlapping GSP primer 
							#paired_line1,paired_line2,paired_line3,paired_line4
								saveNoPrimer_R2_list.append(paired_line1)
								saveNoPrimer_R2_list.append(paired_line2)
								saveNoPrimer_R2_list.append(paired_line3)
								saveNoPrimer_R2_list.append(paired_line4)
							
							if firstReadIndex_inPaired == -1:
								paired_line2_trimmed = paired_line2 #no trimming
								
								if saveNoPrimerNopair_R2: #we can save such R2 reads that has no 10 base overlapping GSP primer 
									saveNoPrimerNopair_R2_list.append(paired_line1)
									saveNoPrimerNopair_R2_list.append(paired_line2)
									saveNoPrimerNopair_R2_list.append(paired_line3)
									saveNoPrimerNopair_R2_list.append(paired_line4)
								
								if dropNoPrimerNopair_R2:
									dropNoPrimerNopair_R2_decision=True
									
								if (minReadLen > len(paired_line2_trimmed)): #this trimmed read is shorted than the cutoff; do not write it
									shortReads_discared+=1								
									continue
								#print "\n\n\n\nNo paired read trimming."
								noPairedReadTrim_count_total += 1
								noPairedReadTrim_count_noPrimer_noPair+=1
								noPairedReadTrim_count_noPrimer += 1

								#we need need not to trim the quality line here
								trimmed_PairedQualityLine=paired_line4
								
							else: #trim based on the trimmedFirst read
								trimmedPairReadLen = firstReadIndex_inPaired + len(reverseC_trimmed_read)
								paired_line2_trimmed = paired_line2[:trimmedPairReadLen]
								if (minReadLen > len(paired_line2_trimmed)): #this trimmed read is shorted than the cutoff; do not write it
									shortReads_discared+=1
									continue
								PairedReadTrim_count += 1
								noPairedReadTrim_count_noPrimer += 1
								
								#now we need to trim the quality line too							
								trimmed_PairedQualityLine=paired_line4[:len(paired_line2_trimmed)]
						else: #trim based on the reverse complementary GSP site
							#print "\n\n\n\nRC GSP based trimming."
							paired_line2_trimmed = paired_line2[:primerStartIndex]
							if (minReadLen > len(paired_line2_trimmed)): #this trimmed read is shorted than the cutoff; do not write it
								shortReads_discared+=1
								continue
							PairedReadTrim_count += 1
							
							#now we need to trim the quality line too
							trimmed_PairedQualityLine=paired_line4[:len(paired_line2_trimmed)]						
							
						if isAppendUMI:						
							#UMI is not to be written, but appended at the first line of read in UMi containing read
							#let's update the fastq read ID with the UMI data
							toReplaceSpace=":%s#%s " %(selectedUMI,selectedUMI_qual)

							if pTrim:
								#let's write all the first read (GSP starting read) data from this block
								writeChunk.append(Firstline.replace(" ",toReplaceSpace))
								writeChunk.append(trimmed_read)
								writeChunk.append(Thirdline)
								writeChunk.append(trimmer_qual)
								#let's store the trimmed paired read data
								processedPairReads.append(paired_line1.replace(" ",toReplaceSpace))
								processedPairReads.append(paired_line2_trimmed)
								processedPairReads.append(paired_line3)
								processedPairReads.append(trimmed_PairedQualityLine)
								noPairedReadTrim_count_total += 1
								noPairedReadTrim_count_noROI += 1								
							else:
								writeChunk.append(Firstline.replace(" ",toReplaceSpace))
								writeChunk.append(fullRead)
								writeChunk.append(Thirdline)
								writeChunk.append(line)		
								#let's store the trimmed paired read data
								processedPairReads.append(paired_line1.replace(" ",toReplaceSpace))
								processedPairReads.append(paired_line2)
								processedPairReads.append(paired_line3)
								processedPairReads.append(paired_line4)

						else:												
							#let's write all the first read (GSP starting read) data from this block
							if pTrim:
								writeChunk.append(Firstline)
								writeChunk.append(trimmed_read)
								writeChunk.append(Thirdline)
								writeChunk.append(trimmer_qual)
								
								#no trimming is required here; so just write the data
								#let's store the trimmed paired read data that is already trimmed
								processedPairReads.extend(paired_line1)
								processedPairReads.extend(paired_line2)
								processedPairReads.extend(paired_line3)
								processedPairReads.extend(paired_line4)
								noPairedReadTrim_count_total += 1
								noPairedReadTrim_count_noROI += 1								
							else:
								writeChunk.append(Firstline)
								writeChunk.append(fullRead)
								writeChunk.append(Thirdline)
								writeChunk.append(line)		
								processedPairReads.extend(paired_line1)
								processedPairReads.extend(paired_line2)
								processedPairReads.extend(paired_line3)
								processedPairReads.extend(paired_line4)								
							
							#now that we have got the UMI sequence, let's save the UMI data
							umiFastq.append(Firstline)
							umiFastq.append(selectedUMI)
							umiFastq.append(Thirdline)
							umiFastq.append(selectedUMI_qual)						
								
					except KeyError:
						#because there is no respective paired read found for this read, let's just both the reads
						unpairedReads += 1								
					
				else: #no need to do any trimming for this, save time; these reads are to be dropped
					if i_args.savedropped_reads == '1':
						'''
						if isAppendUMI:
							toReplaceSpace=":%s#%s " %(selectedUMI,selectedUMI_qual)
							writeChunk.append(Firstline.replace(" ",toReplaceSpace))
							droppedChunk.append(droppedRead)
							droppedChunk.append(Thirdline)
							droppedChunk.append(line)
							#paired_line1,paired_line2,paired_line3,paired_line4
							processedPairReads.append(paired_line1.replace(" ",toReplaceSpace))
							droppedChunk_paired.extend(paired_line2)
							droppedChunk_paired.extend(paired_line3)
							droppedChunk_paired.extend(paired_line4)						
						else:
						'''
						try:
							paired_line1,paired_line2,paired_line3,paired_line4,UMI,SSE,UMI_qual= pairedFastq_dict.pop(Firstline.split(" ")[0])
						except KeyError:
							continue
							#paired_line1='-'
							#paired_line2='-'
							#paired_line3='-'
							#paired_line4='-'
						#Let's not worry about Append UMI method for dropped reads
						droppedChunk.append(Firstline)
						droppedChunk.append(droppedRead)
						droppedChunk.append(Thirdline)
						droppedChunk.append(line)
						#paired_line1,paired_line2,paired_line3,paired_line4
						droppedChunk_paired.append(paired_line1)
						droppedChunk_paired.append(paired_line2)
						droppedChunk_paired.append(paired_line3)
						droppedChunk_paired.append(paired_line4)
				
				#lineNumber = 0 #reset lineNumber to o after fourth line, so that we always have sense of line inside the block.
		#if pTrim==true, reset the counters as no reads were trimmed
		trimmedReads=0
		PairedReadTrim_count=0
		
		endTime = tm.time()
		totalTime = endTime - startTime
		#STATS
		primerErrorRate = float(errorRead) / float(readCount)*100

		stats=textwrap.dedent('''\
			
		*******************************************STATS***********************************
		Sample Part: %s
SequencingMode='Paired'
>>>>GSP READ STATISTICS:
	*Total GSP Reads: %d
	*Trimmed Stats:
		Trimmed GSP Reads: %d
		Trimmed GSP reads percentage: %f
		Not trimmed GSP reads: %d
	*Dropped Read Stats:
		GSP Reads dropped due to Error: %d
		GSP reads dropped due to Error percentage: %f
		Number of SSE trimmed in GSP-containing reads: %d
		%% of SSE trimmed in GSP-containing reads: %f
		
>>>>PAIRED UMI READ STATISTICS:
	*Total paired UMI reads: %s
	*Total paired reads (without double SSE): %d	
	*Primer Trimming Stats:
		Primer Trimmed UMI Reads: %d
		Primer Trimmed UMI reads percentage: %f
		Total Paired reads not primer trimmed: %d
		Paired reads' primer not trimmed due to lack of 10 base overlapping primers: %d
		Paired reads' primer not trimmed due to lack of 10 base of its pair overlapping GSP-containing read (this guarantee the no-primer in UMI-containing read count): %d
		Paired reads' primer not trimmed due to outside ROI region: %d
	*SSE Trimming stats:
		SSE trimmed in UMI-containing reads: %d
		%% SSE trimmed in UMI-containing reads: %f
	*Dropped Read Stats:
		Reads with double SSE: %d
		%% of reads with double SSE: %f
		Paired reads dropped due to Error (same as first read): %d
		Dropped reads due to lack of pair (including dropped due to double SSE + If any duplicated read is present in same fastq file): %d
		Dropped reads due to ultra poor quality base: %d
		Reads dropped due to short read length: %d
		Reads dropped due to short percentage: %f
		Total Dropped Read count: %d
		Total Dropped Read %%: %f
	*UMI stats:
		UMI found in number GSP reads: %d
		Same in both R1 and R2: %d
	Execution Time: %f seconds
	Final total reads in trimmed fastq: %d
		''')%(outputName,
		readCount,
		trimmedReads,
		((float(trimmedReads)/float(readCount))*100),
		noPrimer,
		errorRead,
		primerErrorRate,
		trimmedSSE_gspContainingReads,
		(100*float(trimmedSSE_gspContainingReads)/(readCount)),
		
		totalPairedReads,
		pairedReadNum,		
		PairedReadTrim_count,
		((float(PairedReadTrim_count)/float(totalPairedReads))*100),
		noPairedReadTrim_count_total,
		noPairedReadTrim_count_noPrimer,
		noPairedReadTrim_count_noPrimer_noPair,
		noPairedReadTrim_count_noROI,
				
		trimmedSSE_umiContainingReads,
		(100*float(trimmedSSE_umiContainingReads)/(pairedReadNum)),		
				
		doubleSSE_reads,
		(100*float(doubleSSE_reads)/float(totalPairedReads)),
		errorRead,
		unpairedReads,
		dropReads_poorQualUMI,
		shortReads_discared,
		(float(shortReads_discared)*100.0)/float(totalPairedReads),
		(errorRead+unpairedReads+dropReads_poorQualUMI+shortReads_discared),
		(float(errorRead+unpairedReads+dropReads_poorQualUMI+shortReads_discared)*100.0)/float(totalPairedReads),
		
		umi_inGSP,
		umiSame_inR1R2,
			
		totalTime,
		(len(writeChunk)/4)
		)		
		
		#write the UMI trimmed reads
		#output file handle for paired reads
		#get the name for Non GSP read, it basically the other read type in paried, i.e. if GSP is R1 then UMI read would be R2 and vice versa
		outputName_list=outputName.replace('//','/').split("/")
		if len(outputName_list) > 0: #its a full path, only replace the R1/R2 from actual filename, not the path
			if '_R1_' in outputName_list[-1]: #means UMI read would be R2
				#only modify the last filename, not the full path
				modifiedName=outputName_list[-1].replace('_R1_','_R2_')
				del outputName_list[-1]
				outputName_list.append(modifiedName)
				pairedOutputName="/".join(outputName_list)
				outFQ2_filename_part=pairedOutputName+'_primerTrimmed.fastq'
				umiOut_filename="%s_umi.fasta" % (pairedOutputName)
			elif '_R2_' in outputName_list[-1]:#means UMI read would be R1
				modifiedName=outputName_list[-1].replace('_R2_','_R1_')
				del outputName_list[-1]
				outputName_list.append(modifiedName)
				pairedOutputName="/".join(outputName_list)
				outFQ2_filename_part=pairedOutputName+'_primerTrimmed.fastq'
				umiOut_filename="%s_umi.fasta" % (pairedOutputName)		
			else:
				print "\n\nPairedReadName-ERROR: There in no _R1_ or _R2_ in the input file name. Please check the names of input paired reads."
				logging.debug("\n\nPairedReadName-ERROR: There in no _R1_ or _R2_ in the input file name. Please check the names of input paired reads.")
				sys.exit()			
		else: #not a complete path, just the final name, replace from complete filename
			if '_R1_' in outputName: #means UMI read would be R2
				pairedOutputName=outputName.replace('_R1_','_R2_')
				outFQ2_filename_part=pairedOutputName+'_primerTrimmed.fastq'
				umiOut_filename="%s_umi.fasta" % (outputName.replace('_R1_','_R2_'))
			elif '_R2_' in outputName:#means UMI read would be R1
				pairedOutputName=outputName.replace('_R2_','_R1_')
				outFQ2_filename_part=pairedOutputName+'_primerTrimmed.fastq'
				umiOut_filename="%s_umi.fasta" % (outputName.replace('_R2_','_R1_'))
			else:
				print "\n\nPairedReadName-ERROR: There in no _R1_ or _R2_ in the input file name. Please check the names of input paired reads."
				logging.debug("\n\nPairedReadName-ERROR: There in no _R1_ or _R2_ in the input file name. Please check the names of input paired reads.")
				sys.exit()
		#let's write the dropped reads if user has asked for it
		if i_args.savedropped_reads == '1':
			dropoutFQ_filename="%s_dropped.fastq" % (outputName)
			dropoutFQ_FH = open(dropoutFQ_filename,'w+')
			dropoutFQ_FH.write("\n".join(droppedChunk))
			dropoutFQ_FH.write("\n")
			
			dropoutFQ2_filename_part="%s_dropped.fastq" % (pairedOutputName)
			dropoutFQ2_FH = open(dropoutFQ2_filename_part,'w+')
			dropoutFQ2_FH.write("\n".join(droppedChunk_paired))
			dropoutFQ2_FH.write("\n")
			
			dropoutFQ_FH.close()
			dropoutFQ2_FH.close()
			
		if saveNoPrimer_R2:
			noPrimeR2_filename="%s_noPrimer_R2reads.fastq" % (pairedOutputName)
			saveNoPrimer_R2_FH=open(noPrimeR2_filename,'w+')
			saveNoPrimer_R2_FH.write("\n".join(saveNoPrimer_R2_list))
			saveNoPrimer_R2_FH.write("\n")
			saveNoPrimer_R2_FH.close()
			
		if saveNoPrimerNopair_R2:
			noPrimernoPair_R2_filename="%s_noPrimernoPair_R2reads.fastq" % (pairedOutputName)
			saveNoPrimerNopair_R2_FH=open(noPrimernoPair_R2_filename,'w+')
			saveNoPrimerNopair_R2_FH.write("\n".join(saveNoPrimerNopair_R2_list))
			saveNoPrimerNopair_R2_FH.write("\n")
			saveNoPrimerNopair_R2_FH.close()
		
		#Write the GSP trimmed file
		outFQ_FH_part.write('\n'.join(writeChunk))
		outFQ_FH_part.write('\n')
		outFQ_FH_part.close()		
		
		
		outFQ2_FH = open(outFQ2_filename_part,'w+')	
		outFQ2_FH.write('\n'.join(processedPairReads))
		outFQ2_FH.write("\n")
		outFQ2_FH.close		
		
		#write the UMI reads
		if not isAppendUMI:
			writeUMIfastq=open(umiOut_filename,'w+')
			writeUMIfastq.write('\n'.join(umiFastq))
			writeUMIfastq.write("\n")
			writeUMIfastq.close()

	
		#write the stats for this block
		writeBlockStats=open("%s.stats" %outputName,'w+')
		writeBlockStats.write(stats)
		writeBlockStats.close()
		
		#logging.debug(stats)
		#WriteThe job completion file
		writeBlockCompletion=open("%s.completed" %outputName,'w+')
		writeBlockCompletion.write('completed at %s' % tm.strftime("%b %d %Y %H:%M:%S", tm.localtime()))
		writeBlockCompletion.close()

		#print ">>>>part %s written." %outputName
		#logging.debug(">>>>part %s written." %outputName)
	#####Let's free up the memory
	try:
		del inputGSP[:]
		del inputUMI[:]
	except TypeError:
		pass
	

#
##
###
####
#####pTrim Manager
####
###
##
#
if i_args.sequencing == 'paired':
	if i_args.input_fastq2:
		iFASTQ2=os.path.abspath(i_args.input_fastq2)
		if os.path.isfile(iFASTQ2):
			print "Input fastq file of paired read found. Moving on."
		else:
			print "\n\nError:Input fastq file of paired read could not be located.\nPlease provide the correct PATH and Name of the second paired read FASTQ file\n.Terminating.\n"
			sys.exit()
		
		#get the filename
		try:
			fastqName_obj = re.search('[0-9a-zA-Z-_\.]*\.gz',iFASTQ2)
			fastqName2 = fastqName_obj.group(0)
		except AttributeError:
			print "Could not extract the FASTQ filename from the paired reads. Please check the script.\n"
			raise
		
		if fastqName2[-6:] == '.fq.gz':
			print "Input file verified as fastq file."
			fastq_extension = '.fq.gz'
			outFQ2_filename = fastqName2[:-6]+'_primerTrimmed.fastq' #output filename
			dropoutFQ2_filename=fastqName2[:-6]+'_dropped.fastq' #output filename
		elif fastqName2[-9:] == '.fastq.gz':
			print "Input file verified as fastq file."
			fastq_extension = '.fastq.gz'
			outFQ2_filename = fastqName2[:-9]+'_primerTrimmed.fastq' #output filename
			dropoutFQ2_filename=fastqName2[:-9]+'_dropped.fastq' #output filename
		else:
			print "\n\nError:: Could not verify the input paired reads' file as a FASTQ file. Please provide input paired file as fastq."
			sys.exit()		
	else:
		parser.print_help()
		print "\n\nERROR: Because the sequencing type is 'paired-end', user need to provide paired fastQ read too.\nPlease provide the paired fastq reads.\n"
		sys.exit()
	#for stats purpose
	loggingSoFar = "\n\n================================================================>--Input arguments' summary:\
	\nWorking DIR:%s\n\
	trim primers: %s\n\
	Sequencning type: paired\n\
	Input FASTQ File PATH:%s\n\
	Input FASTQ Filename:%s\n\
	Input paired FASTQ File PATH:%s\n\
	Input paired FASTQ Filename:%s\n\
	Input primer sequence file PATH:%s\n\
	SSE Sequence: %s\n\
	SSE Start Position: %d\n\
	SSE End Position: %d\n\
	SSE length: %d\n\
	SSE tail length: %d\n\
	Drop reads with Double SSE: Yes\n\
	UMI Start Position: %d\n\
	UMI End Position: %d\n\
	UMI length: %d\n\
	User defined total number of reads in input fastq: %s\n\
	Number of maximum cores to use: %d\n\
	Number of maximum reads per block: %d\n\
	output trimmed GSPreads-filename: %s\n\
	output trimmed UMIreads-filename: %s\n\
	is UMI appended to fastqID: %r\n\
	Minimum read length: %d\n\
	\n\n================================================================>--Processing Now\n" % (outputDir,
	pTrim,
	iFASTQ,
	fastqName,
	iFASTQ2,
	fastqName2,
	primerList_file,
	sseSeq,
	(sseStartPos_index+1),
	sseEndPos_index,
	lengthSSE,
	sseTail_count,
	umiStartPos_index,
	umiEndPos_index,
	lengthUMI,
	str(totalinput_reads),
	processNum,
	linespercore,
	outFQ_filename,
	outFQ2_filename,
	isAppendUMI,
	minReadLen)
		
elif i_args.sequencing == 'single' :
	#for stats purpose
	loggingSoFar = "\n\n================================================================>--Input arguments' summary:\
	\nWorking DIR:%s\n\
	Sequencning type: single\n\
	Input FASTQ File PATH:%s\n\
	Input FASTQ Filename:%s\n\
	Input primer sequence file PATH:%s\n\
	User defined total number of reads in input fastq: %s\n\
	Number of maximum cores to use: %d\n\
	Number of maximum reads per block: %d\n\
	Minimum read length: %d\n\
	\n\n================================================================--Processing Now\n" % (outputDir,
	iFASTQ,
	fastqName,
	primerList_file,
	str(totalinput_reads),
	processNum,
	linespercore,
	minReadLen)	
else:
	print "\n\nMissingArgumentERROR: Please provide the mode of primer trimming, using the option '-s'. Choose between 'single' or 'paired'.\n\n"
	sys.exit()

####TO DO:
#File anme management
#input management: For multiprocessng, the input argument vaule's can not be shared, hence need to make a list of all the arguements and provide it to the pTrimCore
#stats management , return will not work for the multiprocessing, hence I will need to make a table for each part, and finally merge all the tables
###write  statistics
logging.debug("\n================================================================>\nStarting the primer trimming using %s - Version 2.6 program\n" % sys.argv[0])
#logging.debug(signature)
logging.debug(loggingSoFar)
print loggingSoFar
startMainTime = tm.time()

blockCount=0		
submitted_jobs_list=[]

#Function to ckeck submitted job's status
def checkJobStatus(submitted_jobs_list,outputName):
	'''
	checks for the '*.completed' file in 'pTrimBlocks' folder and
	returns list of unfinished jobs
	'''
	nonCompletedJobs=[]
	for eachJob in submitted_jobs_list:
		if not os.path.isfile(eachJob): #if for any of the job is not completed, jobFinished will become false
			#print "%s Job not Completed" % eachJob
			nonCompletedJobs.append(eachJob)
		else:
			print "%s Job Completed" % eachJob
			
	return nonCompletedJobs
	
if i_args.sequencing == 'paired':
	#iFASTQ
	#iFASTQ2
	####output paired filename	
	
	if i_args.output2_filename == '0':
		print "ATTENTION: User did not specify any output filename. Using the 'input filename'+'_primerTrimmed.fastq' as output filename.\n"
		#get the current directory
		outFQ2_filename = outFQ2_filename
		print "Name of the output the paired-reads file: %s\n"%outFQ2_filename
	else:
		outFQ2_filename = i_args.output2_filename
		print "Using user provided output filename for the paired-reads file.\nName of the output file: %s\n"%outFQ2_filename	
			
	block_GSPreads_list=[]
	block_UMIreads_list=[]
	
	#umif=gzip.open(iFASTQ2,'r+') #open the UMI file for reading
	gspLineNum=0
	with gzip.open(iFASTQ,'r') as gspf, gzip.open(iFASTQ2,'r') as umif:
		for eachGSPLine,eachUMILine in izip(gspf,umif):
			if gspLineNum == linespercore: #read only one million lines from GSP reads and process				
				blockCount+=1
				print "\n Processing Block-%d, reads in this blocks: %d,%d" %(blockCount,len(block_GSPreads_list),len(block_UMIreads_list))
				#
				##
				###Launch the job
				##
				#
				
				outputName='%s/pTrimBlocks/part_%d_%s' % (outputDir,blockCount,sampleName)
				outputName=outputName.replace("//","/")
				#thread = threading.Thread(target=pTrim_Core, args=(block_GSPreads_list,block_UMIreads_list,primDict,primDict_oneLess,excludedPrimers_dict,excludedPrimers_dict_oneLess,outputName,cutOffLen,roiSeq_list))
				thread = mp.Process(target=pTrim_Core, args=(block_GSPreads_list,block_UMIreads_list,primDict,primDict_oneLess,excludedPrimers_dict,excludedPrimers_dict_oneLess,outputName,cutOffLen,roiSeq_list,pTrim,minReadLen))
				thread.daemon = True                       
				thread.start()                             
				
				#Check if the submitted jobs does not exceed the -np count
				submitted_jobs_list.append("%s.completed"%(outputName))
				print "Submitted job number so far: %d" %len(submitted_jobs_list)
				submitted_jobs_list=checkJobStatus(submitted_jobs_list,outputName)#update the submitted list with non finished jobs
				print "At present Running job's number: %d" %len(submitted_jobs_list)
				while len(submitted_jobs_list) == processNum:
					print "\n\n$$$$Maximum job per cylce limit reached. Let's wait for jobs to finish.$$$$"
					tm.sleep(2)#check again in 2 second
					submitted_jobs_list=checkJobStatus(submitted_jobs_list,outputName)#update the submitted list with non finished jobs
					print "At present Running job's number: %d" %len(submitted_jobs_list)
				
				#reset the counter and the list
				umiLineNum=0
				block_UMIreads_list=[]					
				gspLineNum=0
				block_GSPreads_list=[]#empty the GSP list and read one million lines from the UMI read

				#add the current reads to the next blocks
				block_GSPreads_list.append(eachGSPLine[:-1])
				block_UMIreads_list.append(eachUMILine[:-1])
			else:
				block_GSPreads_list.append(eachGSPLine[:-1])
				block_UMIreads_list.append(eachUMILine[:-1])
			
			gspLineNum +=1#increase the read count
	#process the last blocks
	blockCount+=1
	outputName='%s/pTrimBlocks/part_%d_%s' % (outputDir,blockCount,sampleName)
	print "\n Processing Block-%d and the last block, reads in this blocks: %d" %(blockCount,len(block_UMIreads_list))
	pTrim_Core(block_GSPreads_list,block_UMIreads_list,primDict,primDict_oneLess,excludedPrimers_dict,excludedPrimers_dict_oneLess,outputName,cutOffLen,roiSeq_list,pTrim,minReadLen)	
else:
	block_GSPreads_list=[]
	gspLineNum=0
	totalLines=0
	with gzip.open(iFASTQ,'r') as gspf:
		for eachGSPLine in gspf:
			totalLines+=1
			if gspLineNum == linespercore: #read only one million lines from GSP reads and process				
				blockCount+=1
				print "\nProcessing Block-%d, lines in this blocks: %d" %(blockCount,len(block_GSPreads_list))
				outputName='%s/pTrimBlocks/part_%d_%s' % (outputDir,blockCount,sampleName)		
				#
				##
				###Launch the job
				##
				#
				#pTrim_Core(inputGSP,inputUMI,primDict,primDict_oneLess,excludedPrimers_dict,excludedPrimers_dict_oneLess,outputName,cutOffLen,roiSeq_list)
				inputUMI=None
				#thread = threading.Thread(target=pTrim_Core, args=(block_GSPreads_list,inputUMI,primDict,primDict_oneLess,excludedPrimers_dict,excludedPrimers_dict_oneLess,outputName,cutOffLen,roiSeq_list))
				thread = mp.Process(target=pTrim_Core, args=(block_GSPreads_list,inputUMI,primDict,primDict_oneLess,excludedPrimers_dict,excludedPrimers_dict_oneLess,outputName,cutOffLen,roiSeq_list,pTrim,minReadLen))
				thread.daemon = True
				thread.start()   							
				
				#Check if the submitted jobs does not exceed the -np count
				submitted_jobs_list.append("%s/%s.completed"%(os.getcwd(),outputName))
				print "Submitted job number so far: %d" %len(submitted_jobs_list)
				submitted_jobs_list=checkJobStatus(submitted_jobs_list,outputName)#update the submitted list with non finished jobs
				print "At present Running job's number: %d" %len(submitted_jobs_list)
				while len(submitted_jobs_list) == processNum:
					print "\n\n$$$$Maximum job per cylce limit reached. Let's wait for jobs to finish.$$$$"
					tm.sleep(2)#check again in 2 second
					submitted_jobs_list=checkJobStatus(submitted_jobs_list,outputName)#update the submitted list with non finished jobs
					print "At present Running job's number: %d" %len(submitted_jobs_list)
				
				#reset the counter and the list				
				gspLineNum=0
				block_GSPreads_list=[]#empty the GSP list and read one million lines from the UMI read

				#add the current reads to the next blocks
				block_GSPreads_list.append(eachGSPLine[:-1])
			else:
				block_GSPreads_list.append(eachGSPLine[:-1])
			
			gspLineNum +=1#increase the read count	
	#process the last blocks
	blockCount+=1	
	inputUMI=None
	outputName='%s/pTrimBlocks/part_%d_%s' % (outputDir,blockCount,sampleName)
	print "\n Processing Block-%d and the last block, reads in this blocks: %d" %(blockCount,len(block_GSPreads_list))
	pTrim_Core(block_GSPreads_list,inputUMI,primDict,primDict_oneLess,excludedPrimers_dict,excludedPrimers_dict_oneLess,outputName,cutOffLen,roiSeq_list,pTrim,minReadLen)

#before Moveing on the last block, let's wait for all the jobs to be finished
while len(submitted_jobs_list) > 0:
	print "\n\n$$$$Few pending jobs running in background. Let's wait for jobs to finish.$$$$"
	tm.sleep(2)#check again in 2 second
	submitted_jobs_list=checkJobStatus(submitted_jobs_list,outputName)#update the submitted list with non finished jobs
	print "At present Running job's number: %d" %len(submitted_jobs_list)	
print "Total blocks: %d" % blockCount


#combine the parts
if i_args.sequencing == 'paired':
	if '_R1' in sampleName:
		os.system('cat %s/pTrimBlocks/part*_R1*primerTrimmed.fastq > %s' % (outputDir,outFQ_filename))
		os.system('cat %s/pTrimBlocks/part*_R2*primerTrimmed.fastq > %s' % (outputDir,outFQ2_filename))
		if not isAppendUMI:
			if i_args.umi_outfilename:
				os.system('cat %s/pTrimBlocks/part*_R2*umi.fasta > %s' % (outputDir,i_args.umi_outfilename))
			else:
				os.system('cat %s/pTrimBlocks/part*_R2*umi.fasta > %s_UMI.fastq' % (outputDir,sampleName.replace('_R1','_R2')))
		else:
			logging.debug('User decided to append the UMI in fastq files, hence no UMI file will be written.')	
	elif '_R2' in sampleName:
		os.system('cat %s/pTrimBlocks/part*_R2*primerTrimmed.fastq > %s' % (outputDir,outFQ_filename))
		os.system('cat %s/pTrimBlocks/part*_R1*primerTrimmed.fastq > %s' % (outputDir,outFQ2_filename))
		if not isAppendUMI:
			if i_args.umi_outfilename:
				os.system('cat %s/pTrimBlocks/part*_R1*umi.fasta > %s' % (outputDir,i_args.umi_outfilename))
			else:
				os.system('cat %s/pTrimBlocks/part*_R1*umi.fasta > %s_UMI.fastq' % (outputDir,sampleName.replace('_R2','_R1')))
		else:
			logging.debug('User decided to append the UMI in fastq files, hence no UMI file will be written.')		

else:
	if '_R1' in sampleName:
		os.system('cat %s/pTrimBlocks/part*_R1*primerTrimmed.fastq > %s' % (outputDir,outFQ_filename))
	elif '_R2' in sampleName:
		os.system('cat %s/pTrimBlocks/part*_R2*primerTrimmed.fastq > %s' % (outputDir,outFQ_filename))

#
##
###
####
#####Final Stats Compilation
####
###
##
#
getStatFile_list=glob.glob('%s/pTrimBlocks/*.stats' %outputDir)

readCount = 0
trimmedReads=0

noPrimer=0
errorRead=0
trimmedSSE_gspContainingReads=0

totalPairedReads=0
pairedReadNum=0
PairedReadTrim_count=0

noPairedReadTrim_count_total=0
noPairedReadTrim_count_noPrimer=0
noPairedReadTrim_count_noPrimer_noPair=0
noPairedReadTrim_count_noROI=0
		
trimmedSSE_umiContainingReads=0
		
doubleSSE_reads=0
unpairedReads=0
dropReads_poorQualUMI=0
totalDroppedReads=0

umi_inGSP=0
umiSame_inR1R2=0

finalTotalTrimmedReads=0

droppedReads_shortReads=0

isPaired=False
for eachStatFile in getStatFile_list:
	with open(eachStatFile,'r') as sf:
		for eachLine in sf:
			if 'SequencingMode' in eachLine:
				if 'Paired' in eachLine:
					isPaired=True
					continue				
			
			if isPaired:
				if '*Total GSP Reads: ' in eachLine:
					readCount+=int(eachLine.replace('*Total GSP Reads: ','').replace('\n',''))
				elif 'Trimmed GSP Reads:' in eachLine:
					trimmedReads+=int(eachLine.replace('Trimmed GSP Reads: ','').replace('\n',''))
				elif 'Not trimmed GSP reads:' in eachLine:
					noPrimer+=int(eachLine.replace('Not trimmed GSP reads: ','').replace('\n',''))
				elif 'GSP Reads dropped due to Error:' in eachLine:
					errorRead+=int(eachLine.replace('GSP Reads dropped due to Error: ','').replace('\n',''))
				elif 'Number of SSE trimmed in GSP-containing reads' in eachLine:
					trimmedSSE_gspContainingReads+=int(eachLine.replace('Number of SSE trimmed in GSP-containing reads: ','').replace('\n',''))
				elif '*Total paired UMI reads' in eachLine:
					totalPairedReads+=int(eachLine.replace('*Total paired UMI reads: ','').replace('\n',''))					
				elif '*Total paired reads (without' in eachLine:
					pairedReadNum+=int(eachLine.replace('*Total paired reads (without double SSE): ','').replace('\n',''))
				elif 'Primer Trimmed UMI Reads' in eachLine:
					PairedReadTrim_count+=int(eachLine.replace('Primer Trimmed UMI Reads: ','').replace('\n',''))
				elif 'Total Paired reads not primer trimmed' in eachLine:
					noPairedReadTrim_count_total+=int(eachLine.replace('Total Paired reads not primer trimmed: ','').replace('\n',''))					
				elif "Paired reads' primer not trimmed due to lack of 10 base overlapping primers" in eachLine:
					noPairedReadTrim_count_noPrimer+=int(eachLine.replace("Paired reads' primer not trimmed due to lack of 10 base overlapping primers: ",'').replace('\n',''))										
				elif "Paired reads' primer not trimmed due to lack of 10 base of its pair overlapping GSP-containing read" in eachLine:
					noPairedReadTrim_count_noPrimer_noPair+=int(eachLine.replace("Paired reads' primer not trimmed due to lack of 10 base of its pair overlapping GSP-containing read (this guarantee the no-primer in UMI-containing read count): ",'').replace('\n',''))									
				elif "Paired reads' primer not trimmed due to outside ROI region" in eachLine:
					noPairedReadTrim_count_noROI+=int(eachLine.replace("Paired reads' primer not trimmed due to outside ROI region: ",'').replace('\n',''))								
				elif ("SSE trimmed in UMI-containing reads" in eachLine) and (not '%' in eachLine):
					trimmedSSE_umiContainingReads+=int(eachLine.replace("SSE trimmed in UMI-containing reads: ",'').replace('\n',''))
				elif "Reads with double SSE" in eachLine:
					doubleSSE_reads+=int(eachLine.replace("Reads with double SSE: ",'').replace('\n',''))
				elif "Dropped reads due to lack of pair (including dropped due" in eachLine:
					unpairedReads+=int(eachLine.replace("Dropped reads due to lack of pair (including dropped due to double SSE + If any duplicated read is present in same fastq file): ",'').replace('\n',''))								
				elif "Dropped reads due to ultra poor quality base" in eachLine:
					dropReads_poorQualUMI+=int(eachLine.replace("Dropped reads due to ultra poor quality base: ",'').replace('\n',''))
				elif "Reads dropped due to short read length:" in eachLine:
					droppedReads_shortReads=droppedReads_shortReads+int(eachLine.replace("Reads dropped due to short read length: ",'').replace('\n',''))
				elif "Total Dropped Read count" in eachLine:
					totalDroppedReads+=int(eachLine.replace("Total Dropped Read count: ",'').replace('\n',''))
				elif "UMI found in number GSP reads" in eachLine:
					umi_inGSP+=int(eachLine.replace("UMI found in number GSP reads: ",'').replace('\n',''))
				elif "Same in both R1 and R2" in eachLine:
					umiSame_inR1R2+=int(eachLine.replace("Same in both R1 and R2: ",'').replace('\n',''))
				elif "Final total reads in trimmed fastq" in eachLine:
					finalTotalTrimmedReads+=int(eachLine.replace("Final total reads in trimmed fastq: ",'').replace('\n',''))
			else:
				if 'Total Reads: ' in eachLine:
					readCount+=int(eachLine.replace('Total Reads: ','').replace('\n',''))	
				elif 'Trimmed Reads:' in eachLine:
					trimmedReads+=int(eachLine.replace('Trimmed Reads: ','').replace('\n',''))	
				elif 'Reads not trimmed:' in eachLine:
					noPrimer+=int(eachLine.replace('Reads not trimmed: ','').replace('\n',''))
				elif 'Reads dropped due to Error:' in eachLine:
					errorRead+=int(eachLine.replace('Reads dropped due to Error: ','').replace('\n',''))		
				elif "Final total reads in trimmed fastq" in eachLine:
					finalTotalTrimmedReads+=int(eachLine.replace("Final total reads in trimmed fastq: ",'').replace('\n',''))					
				
#let's gather the overall stats
"""
#Shell involving method; works perfectly fine
totalDropped_command=textwrap.dedent('''cd pTrimBlocks;c=0;for i in *.stats; do c=$(echo "$(($c+$(cat $i|grep 'Total Dropped Read count:'| cut -d' ' -f 5)))");done;echo $c;cd ..;''')
totalDropped=makeProcess(totalDropped_command)
totalDropped=int(totalDropped[0])
"""
#		(errorRead+unpairedReads+dropReads_poorQualUMI+droppedReads_shortReads),
#		(float(errorRead+unpairedReads+dropReads_poorQualUMI+droppedReads_shortReads)*100.0)/float(totalPairedReads),	
if isPaired:
	stats=textwrap.dedent('''\
		
		*******************************************STATS***********************************
SequencingMode='Paired'
>>>>GSP READ STATISTICS:
	*Total GSP Reads: %d
	*Trimmed Stats:
		Trimmed GSP Reads: %d
		Trimmed GSP reads percentage: %f
		Not trimmed GSP reads: %d
	*Dropped Read Stats:
		GSP Reads dropped due to Error: %d
		GSP reads dropped due to Error percentage: %f
		Number of SSE trimmed in GSP-containing reads: %d
		%% of SSE trimmed in GSP-containing reads: %f
		
>>>>PAIRED UMI READ STATISTICS:
	*Total paired UMI reads: %s
	*Total paired reads (without double SSE): %d	
	*Primer Trimming Stats:
		Primer Trimmed UMI Reads: %d
		Primer Trimmed UMI reads percentage: %f
		Total Paired reads not primer trimmed: %d
		Paired reads' primer not trimmed due to lack of 10 base overlapping primers: %d
		Paired reads' primer not trimmed due to lack of 10 base of its pair overlapping GSP-containing read (this guarantee the no-primer in UMI-containing read count): %d
		Paired reads' primer not trimmed due to outside ROI region: %d
	*SSE Trimming stats:
		SSE trimmed in UMI-containing reads: %d
		%% SSE trimmed in UMI-containing reads: %f
	*Dropped Read Stats:
		Reads with double SSE: %d
		%% of reads with double SSE: %f
		Paired reads dropped due to Error (same as first read): %d
		Dropped reads due to lack of pair (including dropped due to double SSE + If any duplicated read is present in same fastq file): %d
		Dropped reads due to ultra poor quality base: %d
		Reads dropped due to short read length: %d
		%% of Reads dropped due to short read length: %f%%
		Total Dropped Read count: %d
		Total Dropped Read %%: %f
	*UMI stats:
		UMI found in number GSP reads: %d
		Same in both R1 and R2: %d
	*Total reads in trimmed output fastq: %d
		''')%(readCount,
		trimmedReads,
		((float(trimmedReads)/float(readCount))*100),
		noPrimer,
		errorRead,
		(float(errorRead) / float(readCount))*100.0,
		trimmedSSE_gspContainingReads,
		(100*float(trimmedSSE_gspContainingReads)/(readCount)),
		
		totalPairedReads,
		pairedReadNum,		
		PairedReadTrim_count,
		((float(PairedReadTrim_count)/float(totalPairedReads))*100),
		noPairedReadTrim_count_total,
		noPairedReadTrim_count_noPrimer,
		noPairedReadTrim_count_noPrimer_noPair,
		noPairedReadTrim_count_noROI,
				
		trimmedSSE_umiContainingReads,
		(100*float(trimmedSSE_umiContainingReads)/(pairedReadNum)),		
				
		doubleSSE_reads,
		(100*float(doubleSSE_reads)/float(totalPairedReads)),
		errorRead,
		unpairedReads,
		dropReads_poorQualUMI,
		droppedReads_shortReads,
		(float(droppedReads_shortReads)*100.0)/float(totalPairedReads),
		(errorRead+unpairedReads+dropReads_poorQualUMI+droppedReads_shortReads),
		(float(errorRead+unpairedReads+dropReads_poorQualUMI+droppedReads_shortReads)*100.0)/float(totalPairedReads),
		
		umi_inGSP,
		umiSame_inR1R2,
		
		finalTotalTrimmedReads
		)
else:
	stats=textwrap.dedent('''\
		
*******************************************STATS***********************************
SequencingMode='Single'
	Total Reads: %d
	Trimmed Reads: %d
	Trimmed percentage: %f
	Reads not trimmed: %d
	Reads dropped due to Error: %d
	Dropped reads percentage: %f
	Total reads in trimmed output fastq: %d
		''')%(readCount,
		trimmedReads,
		((float(trimmedReads)/float(readCount))*100),
		noPrimer,
		errorRead,
		(float(errorRead) / float(readCount))*100.0,
		finalTotalTrimmedReads
		)	
			
logging.debug(stats)
print stats	
#
##
###
####
#####All done, let's clean up
####
###
##
#

#delete tmp folder
if i_args.keep_tmpfolder == 'no':
	os.system('rm -r "%s/pTrimBlocks"' % outputDir)
elif i_args.keep_tmpfolder == 'yes':
	print "Temp folder '%s/pTrimBlocks' not deleted as used decided to keep it." % outputDir
	logging.debug("Temp folder '%s/pTrimBlocks' not deleted as used decided to keep it." % outputDir)

ttoe=tm.time()-startMainTime
if ttoe >= 3600:
	logging.debug("\nTotal Time Of execution: %f Hours" % (float(ttoe)/(60.0*60.0)))
elif ttoe >= 60 and (ttoe < 3600):
	logging.debug("\nTotal Time Of execution: %f Minutes" % (float(ttoe)/60.0))
else:
	logging.debug("\nTotal Time Of execution: %f Seconds" % (ttoe))
	
try:
	tmp = int('ThisIsA_Hack_to_exit')
except:
	print "\n\n****pTrim %s Execution completed.****" % sys.argv[0]
	parser.parse_args(['--version'])