import os
import sys
import argparse
import pandas as pd
############################################################# arguments #############################################################
M_parser = argparse.ArgumentParser(description='Variants annotator only for OverlapCaller.')
M_parser.add_argument('-itxt','--input-txt',type=str,required=True,help='* Required: Please provide the PATH of input txt file only OverlapCaller output.')
#M_parser.add_argument('-ivcf','--input-vcf',type=str,required=True,help='* Required: Please provide the PATH of input vcf file only Cosmic.')
M_args=M_parser.parse_args()

S_intxt=M_args.input_txt
#S_vcf=M_args.input_vcf

S_outName = S_intxt.split('/')[-1].split('.')[0]
df_inFile = pd.read_csv(S_intxt, sep='\t')
S_inPath = '/'.join(S_intxt.split('/')[:-1])
S_outPath = S_inPath+'/Annotate'
os.system( 'mkdir '+S_outPath )
#S_vcf = '/Work/BIO/bin/annovar/humandb_extract/vcf/ROI_dSA09725v1-0_bi_target_filtered_CosMutsV84.vcf'
S_vcf = '/Work/BIO/bin/annovar/humandb_extract/vcf/CosmicMuts_hg19_v84_sorted_CNTextracted.vcf'

df_Cosmic = pd.read_csv(S_vcf, sep='\t', comment='#', header=None,
						names=['chr','pos','id','ref','alt','qual','filter','info','format','Ex']
						)
#####################################################################################################################################

A_CDS = df_Cosmic['info'].str.split('CDS=').str[-1].str.split(';').str[0]
A_AA = df_Cosmic['info'].str.split('AA=').str[-1].str.split(';').str[0]
A_CNT = df_Cosmic['info'].str.split('CNT=').str[-1].str.split(';').str[0]

df_Cosmic_trim = df_Cosmic[['chr','pos','ref','alt']].drop_duplicates()
df_Cosmic_trim.insert(loc=df_Cosmic_trim.shape[1],column='CDS',value=A_CDS)
df_Cosmic_trim.insert(loc=df_Cosmic_trim.shape[1],column='AA',value=A_AA)
df_Cosmic_trim.insert(loc=df_Cosmic_trim.shape[1],column='CNT',value=A_CNT)

df_merge = pd.merge(df_inFile, df_Cosmic_trim, how='left', on=['chr','pos','ref','alt'] ).fillna('.')
df_merge['CNT'] = df_merge['CNT'].replace('.',0)

df_merge[['depth','AF','AO','ErrorFree','CNT']] = df_merge[['depth','AF','AO','ErrorFree','CNT']].astype(str)

A_info = df_merge['BaseCounts']+'|'+df_merge['depth']+'|'+df_merge['AF']+'|'+df_merge['AO']+'|'+df_merge['ErrorFree']+'|'+df_merge['CDS']+'|'+df_merge['AA']+'|'+df_merge['CNT']

df_merge_form = df_merge[['chr','pos','ref','alt']]
df_merge_form.insert( loc=2, column='if',value=['.']*df_merge_form.shape[0] )
df_merge_form.insert( loc=df_merge_form.shape[1], column='info1',value=A_info )
df_merge_form.insert( loc=df_merge_form.shape[1], column='info2',value=A_info )
df_merge_form.insert( loc=df_merge_form.shape[1], column='info3',value=A_info )
df_merge_form.to_csv(S_outPath+'/'+S_outName+'_annotated.vcf', sep='\t',index=None, header=None)

############################################################ Annovar ############################################################

os.system( '/Work/BIO/bin/annovar/convert2annovar.pl -format vcf4 '+S_outPath+'/'+S_outName+'_annotated.vcf'
		+' > '+S_outPath+'/'+S_outName+'_Anno.avinput'
		)
os.system( 'cut -f 1,2,3,4,5,7 '+S_outPath+'/'+S_outName+'_Anno.avinput'+' > '
			+S_outPath+'/'+S_outName+'_Anno.2avinput' )
os.system( '/Work/BIO/bin/annovar/table_annovar.pl '
			+S_outPath+'/'+S_outName+'_Anno.2avinput'+' /Work/BIO/bin/annovar/humandb_extract -buildver hg19 -out '+S_outPath+'/'+S_outName+
			' -remove -protocol refGene,avsnp150,cosmic83,gnomad_exome,clinvar_20180225 -operation g,f,f,f,f -nastring . -otherinfo' )
os.system( 'rm '+S_outPath+'/'+S_outName+'_Anno.avinput '+S_outPath+'/'+S_outName+'_Anno.2avinput' )

#################################################################################################################################

df_Annotated = pd.read_csv(S_outPath+'/'+S_outName+'.hg19_multianno.txt',sep='\t')
df_Annotated = df_Annotated[(df_Annotated['Ref']!='0')&(df_Annotated['Ref']!=0)]

A_BaseCounts = df_Annotated['Otherinfo'].str.split('|').str[0]
A_depth = df_Annotated['Otherinfo'].str.split('|').str[1]
A_AF = df_Annotated['Otherinfo'].str.split('|').str[2]
A_AO = df_Annotated['Otherinfo'].str.split('|').str[3]
A_EF = df_Annotated['Otherinfo'].str.split('|').str[4]
A_CDS = df_Annotated['Otherinfo'].str.split('|').str[5]
A_AA = df_Annotated['Otherinfo'].str.split('|').str[6]
A_CNT = df_Annotated['Otherinfo'].str.split('|').str[7]

df_Annotated_info = df_Annotated.drop(['GeneDetail.refGene','CLNDBN','CLNACC','CLNDISDB','Otherinfo'],axis=1)
df_Annotated_info.insert(loc=df_Annotated_info.shape[1],column='CDS', value=A_CDS)
df_Annotated_info.insert(loc=df_Annotated_info.shape[1],column='AA', value=A_AA)
df_Annotated_info.insert(loc=df_Annotated_info.shape[1],column='BaseCounts', value=A_BaseCounts)
df_Annotated_info.insert(loc=df_Annotated_info.shape[1],column='depth', value=A_depth)
df_Annotated_info.insert(loc=df_Annotated_info.shape[1],column='AF', value=A_AF)
df_Annotated_info.insert(loc=df_Annotated_info.shape[1],column='AO', value=A_AO)
df_Annotated_info.insert(loc=df_Annotated_info.shape[1],column='EF', value=A_EF)
df_Annotated_info.insert(loc=df_Annotated_info.shape[1],column='CNT', value=A_CNT)
df_Annotated_info.to_csv( S_outPath+'/'+S_outName+'_annotated.csv',sep='\t', index=None )
df_Annotated_info.to_csv( S_outPath+'/'+S_outName+'_annotated.txt',sep='\t', index=None )