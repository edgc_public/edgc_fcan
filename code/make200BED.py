#!/usr/bin/env python2.7

#Authot: Shashank, BI@EDGC

import pandas as pd
import numpy as nm
import sys
import os
import argparse
import textwrap
import subprocess as sp
import re

parser=argparse.ArgumentParser(description="This script is intended to extend the region of a targets in a bed file, in both direction i.e. upsteam and downstream.",usage='use "python %s --help" for more information' % sys.argv[0],
formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-ib','--input-bed',type=str,help='Please provide the input BED file as the first arguement.')
parser.add_argument('-eru','--extraregion-upstream',default=200,type=int,help=textwrap.dedent('''\
Optional: Please provide the number of extra bases towards upstream.
Default: +200
'''))
parser.add_argument('-erd','--extraregion-downstream',type=int,default=200,help=textwrap.dedent('''\
Optional: Please provide the number of extra bases towards downstream.
Default: -200
'''))
parser.add_argument('-od','--output-directory',type=str,help=textwrap.dedent('''\
Optional: Please provide the output directory.
Default: Current Directory, from where the script is launched.
'''))

i_args=parser.parse_args()

#Make a job hadeling functions
def makeProcess(command):
	process = sp.Popen(command, stderr=sp.PIPE,stdout=sp.PIPE, shell=True)
	output, error = process.communicate()
	returnCode = process.returncode
	return output, error, returnCode

if i_args.input_bed:
	inputBED=i_args.input_bed
	if os.path.isfile(inputBED):
		print "The location of the BED file is verified. Will be using the following BED file:\n%s" % inputBED
	else:
		print "\n\nERROR: Could not locate the input bed file. Please check the location of input bed file.Terminating."
		parser.print_help()
		sys.exit()
		
	#Write the bedfile
	bed_filename_obj=re.search("[0-9a-zA-Z-_\.]*\.bed",inputBED)
	bed_filename=bed_filename_obj.group(0)
	if not bed_filename[-4:] == '.bed':
		print "ERROR: The input file does not appears to be a .bed file. Terminating. Please provide a .bed file."
		sys.exit()
else:
	print "\n\nERROR: Please provide the input BED file using option -ib. Terminating."
	parser.print_help()
	sys.exit()


extraUp = i_args.extraregion_upstream
extraDown = i_args.extraregion_downstream
print "The extra region Upstream is: %s" % extraUp
print "The extra region Downstream is: %s" % extraDown

if i_args.output_directory:
	outDIR = (i_args.output_directory).rstrip().replace("\n",'')
	if os.path.isdir(outDIR):
		print "The output directory location verified. The results will be produced in the following DIR.\n%s" % outDIR
	else:
		print "\n\nERROR: User provided output directory path does not exists. Please check the path."
		sys.exit()
else:
	outDIR_obj = makeProcess('pwd')
	outDIR = outDIR_obj[0].rstrip().replace("\n",'')


#read BED
readBED=pd.read_csv(inputBED, sep='\s+', dtype='str',names=['CHR','START','END', 'INFO1','INFO2'],comment='#')

bedName = "%s/%s_ExtraRegion.bed" % (outDIR,bed_filename)
bedFile_write = open(bedName, 'w+')

for index, eachRow in readBED.iterrows():
#	if (len(eachRow) < 2) and (eachRow[:1]=='#'):
#		continue
	try:
		modifiedBED_line = (str(eachRow['CHR'])+"\t"+ str(int(eachRow['START'])-extraUp)+"\t"+ str(int(eachRow['END'])+extraDown)+"\t"+str(eachRow['INFO1'])+str(eachRow['INFO2'])+"\n").replace("	nan",'').replace("nan",'')
		bedFile_write.write(modifiedBED_line)
	except ValueError:
		print "\n\nSOMETHING WRONG WITH INPUT BED FILE FORMAT: Please check if there is anything that is not a ROI region and is still not commented by # in bed file.\nPlease comment anything that is not #CHR START END."
		sys.exit()