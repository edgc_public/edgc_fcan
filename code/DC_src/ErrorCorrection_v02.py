################################
import sys,glob,os,re
import argparse
import textwrap
import time
import pysam as ps
import pandas as pd
import numpy as np
from collections import Counter
################################
######################################################################################################################################### Script's Functions
def ClusterErrorCorrection(I_ClusterOrder,df,S_ClusterCheck) :
	#print L_Names[I_ClusterOrder]
	#============================================================================================= basic df info
	L_Index = df.index
	I_ClusterCount = df.shape[0]
	#============================================================================================= Current MD, CIGAR analysis
	A_EditMD = df['MD'].apply( lambda x : 
								[ '{}{}'.format(d,D.strip()) for d,D in zip( re.findall('\d+',x), re.findall('\D+',x)+[' '] ) ] )
	#---------------------------------------------------------------------------------------------
	A_EditCIGAR = df['CIGAR'].apply( lambda x : 
								[ '{}{}'.format(d,D.strip()) for d,D in zip( re.findall('\d+',x), re.findall('\D+',x)+[' '] ) ] )
	#============================================================================================= edit MD position
	A_EditPos = []
	for I_index in L_Index :
		L_MD = A_EditMD[I_index]
		I_StartPos = df['StartPos'][I_index]
		#-----------------------------------------------------------------------------------------
		I_EditPos = 0
		L_EditPos = []
		for S_MD in L_MD :
			I_del_check = len(re.findall('\W+',S_MD))
			I_end_check = len(re.findall('\D+',S_MD))
			#-------------------------------------------------------------------------------------
			if I_del_check != 0 and I_end_check != 0 :
				I_del_pos = int(re.findall('\d+',S_MD)[0])
				I_EditPos += I_del_pos
			#-------------------------------------------------------------------------------------
			elif I_del_check == 0 and I_end_check == 0 :
				I_end_pos = int(S_MD)
				I_EditPos += I_end_pos
			#-------------------------------------------------------------------------------------
			else :
				I_snv_pos = int(re.findall('\d+',S_MD)[0])+1
				I_EditPos += I_snv_pos
			#-------------------------------------------------------------------------------------
			L_EditPos.append(I_EditPos)
		#-----------------------------------------------------------------------------------------
		L_EditPos = [ I_pos+I_StartPos for I_pos in L_EditPos ]
		A_EditPos.append(L_EditPos)
	#============================================================================================= create custom MD
	L_CustomMD = []
	A_CustomMD = []
	A_EndPos = []
	for I_index in L_Index :
		L_MD = A_EditMD[I_index]
		L_Pos =  A_EditPos[I_index]
		S_tmp = ''.join(L_MD)
		#-----------------------------------------------------------------------------------------
		A_CustomMD.append([])
		I_WholeDel_check = len(re.findall('\W+',''.join(L_MD)))
		I_AfterDelPos = 0
		for S_MD, I_pos in zip(L_MD,L_Pos) :
			I_del_check = len(re.findall('\W+',S_MD))
			I_end_check = len(re.findall('\D+',S_MD))
			L_editableMD = re.findall('\D+',S_MD)
			#-------------------------------------------------------------------------------------
			if I_del_check == 0 and I_end_check != 0 :
				S_NewMD = '{}{}'.format( I_pos-1+I_AfterDelPos, L_editableMD[0] )
				#---------------------------------------------------------------------------------
				L_CustomMD.append(S_NewMD)
				A_CustomMD[-1].append(S_NewMD)
			#-------------------------------------------------------------------------------------
			elif I_del_check == 0 and I_end_check == 0 :
				if I_WholeDel_check == 0 :
					A_EndPos.append(I_pos-1)
				else :
					L_Dels = [ len(re.findall('[A-Z]',S)) for S in L_MD if len(S.split('^')) != 1 ]
					#I_DelNum = len(L_Dels) 													  # Del number just minus 1
					I_DelNum = 1
					I_DelCount = sum(L_Dels)-I_DelNum
					#-----------------------------------------------------------------------------
					A_EndPos.append(I_pos+I_DelCount)
			#-------------------------------------------------------------------------------------
			else :
				if len(L_editableMD) != 0 :
					S_NewMD = '{}{}'.format( I_pos+I_AfterDelPos, L_editableMD[0] )
					I_DelCount = len(S_MD.split('^')[-1])
					I_AfterDelPos += I_DelCount
					L_CustomMD.append(S_NewMD)
					A_CustomMD[-1].append(S_NewMD)
			#-------------------------------------------------------------------------------------
	D_CustomMD = Counter(L_CustomMD)
	#============================================================================================= extract start end position
	S_chr = L_Chr[I_ClusterOrder]
	I_MinStartPos = df['StartPos'].min()
	I_MaxEndPos = max(A_EndPos)
	#============================================================================================= Depth
	L_Depth = []
	A_Range = []
	for I_start, I_end in zip(df['StartPos'],A_EndPos) :
		L_Depth += range(I_start,I_end+1)
		A_Range.append(range(I_start,I_end+1))
	D_Depth = Counter(L_Depth)
	#============================================================================================= create custom CIGAR
	A_InsertionPos = []
	L_InsertionPos = []
	L_InsertionQual = []
	for I_index in L_Index :
		L_Cigar = A_EditCIGAR[I_index]
		I_StartPos = df['StartPos'][I_index]
		S_seq = df['seq'][I_index]
		S_Qual = df['qual'][I_index]
		#-----------------------------------------------------------------------------------------
		I_EditPos = 0
		I_DelSeqCheck = 0
		A_InsertionPos.append([])
		for S_Cigar in L_Cigar :
			S_CigarText = re.findall('\D+',S_Cigar)[0]
			#-------------------------------------------------------------------------------------
			if S_CigarText == 'M' :
				I_MatchLen = int(re.findall('\d+',S_Cigar)[0])
				I_EditPos += I_MatchLen
			elif S_CigarText == 'D' :
				I_DelLen = int(re.findall('\d+',S_Cigar)[0])
				I_EditPos += I_DelLen
				I_DelSeqCheck += I_DelLen
			elif S_CigarText == 'I' :
				I_InsertionPos = I_EditPos+I_StartPos
				I_InsertionLen = int(re.findall('\d+',S_Cigar)[0])
				S_InsertionSeq = S_seq[(I_EditPos-I_DelSeqCheck):(I_EditPos-I_DelSeqCheck)+I_InsertionLen]
				S_InsertionQual = S_Qual[I_EditPos:I_EditPos+I_InsertionLen]
				#---------------------------------------------------------------------------------
				A_InsertionPos[-1].append('{}-{}'.format(I_InsertionPos,S_InsertionSeq))
				L_InsertionPos.append('{}-{}'.format(I_InsertionPos,S_InsertionSeq))
				L_InsertionQual.append(['{}-{}'.format(I_InsertionPos,S_InsertionSeq),S_InsertionQual])
	#---------------------------------------------------------------------------------------------
	L_InsPosSeq = set( [ L_InsQual[0] for L_InsQual in L_InsertionQual ] )
	D_InsQual = {}
	for S_InsPosSeq in L_InsPosSeq :
		D_InsQual[S_InsPosSeq] = []
		for S_InsQual, S_Qual in L_InsertionQual :
			if S_InsPosSeq == S_InsQual :
				D_InsQual[S_InsPosSeq].append(S_Qual)
	#---------------------------------------------------------------------------------------------
	D_InsQualAvg = {}
	for S_InsPosSeq,L_InsQual in D_InsQual.iteritems() :
		D_InsQualAvg[S_InsPosSeq] = ''
		L_InsQualSplited = [ list(S_InsQual) for S_InsQual in L_InsQual ]
		#-----------------------------------------------------------------------------------------
		L_MergedSplitedQual = [ [] for L in L_InsQualSplited[0] ]
		for L_SplitedQual in L_InsQualSplited :
			L_SpliedRange = range(len(L_SplitedQual))
			#-------------------------------------------------------------------------------------
			for I_RangePos in L_SpliedRange :
				try :
					L_MergedSplitedQual[I_RangePos].append( D_Symbol2Qscore[L_SplitedQual[I_RangePos]] )
				except :
					L_MergedSplitedQual[I_RangePos].append( D_Symbol2Qscore['I'] )
		#-----------------------------------------------------------------------------------------
		S_InsAvgQual = ''.join([ D_Qscore2Symbol[int(np.mean(L_Qscore))] for L_Qscore in L_MergedSplitedQual ])
		D_InsQualAvg[S_InsPosSeq] += S_InsAvgQual
	#---------------------------------------------------------------------------------------------
	#============================================================================================= create Mismatch Deletion DataFrame
	L_MisDel_ErrCor = []
	for S_CustomMD, I_count in D_CustomMD.iteritems() :
		I_pos = int(re.findall('\d+',S_CustomMD)[0])
		F_depth = float(D_Depth[I_pos])
		try :
			F_ratio = I_count/F_depth
		except :
			F_ratio = 0
		L_MisDel_ErrCor.append([S_CustomMD,I_pos,I_count,F_depth,F_ratio])
	#---------------------------------------------------------------------------------------------
	df_MisDel_ErrCor = pd.DataFrame(L_MisDel_ErrCor,columns=['CustomMD','pos','count','depth','ratio'])
	df_MisDel_ErrCor = df_MisDel_ErrCor[df_MisDel_ErrCor['depth']!=1].reset_index(drop=True) # SingleTon filter
	#============================================================================================= create Insertion DataFrame
	D_InsertionInfo = Counter(L_InsertionPos)
	A_InsertionInfo = []
	for S_PosIns, I_count in D_InsertionInfo.iteritems() :
		I_InsPos = int(S_PosIns.split('-')[0])
		F_depth = float(D_Depth[I_InsPos])
		S_InsertionSeq = S_PosIns.split('-')[-1]
		F_ratio = I_count/F_depth
		S_InsAvgQual = D_InsQualAvg[S_PosIns]
		A_InsertionInfo.append([S_PosIns,I_InsPos,I_count,F_depth,F_ratio,S_InsertionSeq,S_InsAvgQual])
	#---------------------------------------------------------------------------------------------
	df_Inser_ErrCor = pd.DataFrame(A_InsertionInfo,columns=['CustomCIGAR','pos','count','depth','ratio','seq','qual'])
	df_Inser_ErrCor = df_Inser_ErrCor[df_Inser_ErrCor['depth']!=1].reset_index(drop=True) # SingleTon filter
	#============================================================================================= Before EC NM counter
	I_B4SnpCount = 0
	I_B4DelCount = 0
	I_B4InsCount = 0
	for I_index in df_MisDel_ErrCor.index :
		S_CustomMD = df_MisDel_ErrCor['CustomMD'][I_index]
		I_count = df_MisDel_ErrCor['count'][I_index]
		#-----------------------------------------------------------------------------------------
		I_del_check = len(S_CustomMD.split('^'))
		#-----------------------------------------------------------------------------------------
		if I_del_check == 1 :
			I_B4SnpCount += I_count
		else :
			I_B4DelCount += I_count
	#---------------------------------------------------------------------------------------------
	I_B4InsCount = df_Inser_ErrCor['count'].sum()
	#============================================================================================= Custom DataFrame
	df_Customs = pd.DataFrame({'CustomMD':A_CustomMD,'CustomCIGAR':A_InsertionPos,'EndPos':A_EndPos,'Range':A_Range})
	df = pd.concat([df,df_Customs], axis=1)
	#============================================================================================= Return ClusterEC values
	L_ClusterEC = [df,I_ClusterOrder,S_ClusterCheck,
					S_chr,I_MinStartPos,I_MaxEndPos,
					D_Depth,
					I_B4SnpCount,I_B4DelCount,I_B4InsCount,
					df_MisDel_ErrCor,df_Inser_ErrCor
					]
	#=============================================================================================
	return L_ClusterEC
##################################################################################################
##################################################################################################
def OverlapErrorCorrection(df,I_ClusterOrder,S_ClusterCheck,
							S_chr,I_MinStartPos,I_MaxEndPos,
							D_Depth,
							I_B4SnpCount,I_B4DelCount,I_B4InsCount,
							df_MisDel_ErrCor,df_Inser_ErrCor,
							L_MisDel_SelectedPos,L_Inser_SelectedPos
							) :
	#============================================================================================= Error Correction cutoff-value setting
	L_MisDel_Selected_CustomMD = []
	for I_MisDel_SelectedPos in L_MisDel_SelectedPos :
		F_MaxRatio = df_MisDel_ErrCor[df_MisDel_ErrCor['pos']==I_MisDel_SelectedPos]['ratio'].max()
		S_Selected_CustomMD = df_MisDel_ErrCor[(df_MisDel_ErrCor['pos']==I_MisDel_SelectedPos)&
											(df_MisDel_ErrCor['ratio']==F_MaxRatio)]['CustomMD'].reset_index(drop=True)[0]
		L_MisDel_Selected_CustomMD.append(S_Selected_CustomMD)
	#---------------------------------------------------------------------------------------------
	L_Inser_Selected_CustomCIGAR = []
	for I_Inser_SelectedPos in L_Inser_SelectedPos :
		F_MaxRatio = df_Inser_ErrCor[df_Inser_ErrCor['pos']==I_Inser_SelectedPos]['ratio'].max()
		S_Selected_CustomCIGAR = df_Inser_ErrCor[(df_Inser_ErrCor['pos']==I_Inser_SelectedPos)&
												(df_Inser_ErrCor['ratio']==F_MaxRatio)]['CustomCIGAR'].reset_index(drop=True)[0]
		L_Inser_Selected_CustomCIGAR.append(S_Selected_CustomCIGAR)
	#============================================================================================= Target analyzer
	D_TargetMisDelBases = {}
	D_TargetMisDelQual = {}
	for S_Selected_CustomMD in L_MisDel_Selected_CustomMD :
		I_del_check = len(re.findall('\W+',S_Selected_CustomMD))
		#-----------------------------------------------------------------------------------------
		D_TargetMisDelBases[S_Selected_CustomMD] = []
		D_TargetMisDelQual[S_Selected_CustomMD] = []
		#-----------------------------------------------------------------------------------------
		A_TargetCheck = df['CustomMD'].apply( lambda x: len([S_MD for S_MD in x if S_MD==S_Selected_CustomMD]) )
		L_TargetIndex = A_TargetCheck[A_TargetCheck==1].index
		#-----------------------------------------------------------------------------------------
		if I_del_check == 0 :
			for I_index in L_TargetIndex :
				L_CustomMD = df['CustomMD'][I_index]
				L_CustomCIGAR = df['CustomCIGAR'][I_index]
				#I_StartPos = df['StartPos'][I_index]
				#I_EndPos = df['EndPos'][I_index]
				L_Range = df['Range'][I_index]
				S_Seq = df['seq'][I_index]
				S_Qual = df['qual'][I_index]
				#---------------------------------------------------------------------------------
				L_MdCigarSorted = sorted(L_CustomMD+L_CustomCIGAR)
				I_SortedIndex = L_MdCigarSorted.index(S_Selected_CustomMD)
				L_InDelList = L_MdCigarSorted[:I_SortedIndex]
				I_TargetPos = int(re.findall('\d+',S_Selected_CustomMD)[0])
				I_InserAdd = 0
				#---------------------------------------------------------------------------------
				for S_InDelInfo in L_InDelList :
					I_del_check = len(S_InDelInfo.split('^'))
					I_Ins_check = len(S_InDelInfo.split('-'))
					#-----------------------------------------------------------------------------
					if I_del_check == 2 :
						I_DelCount = len(S_InDelInfo.split('^')[-1])
						I_TargetPos -= I_DelCount
					elif I_Ins_check == 2 :
						I_InsCount = len(S_InDelInfo.split('-')[-1])
						I_InserAdd += I_InsCount
				#---------------------------------------------------------------------------------
				I_SeqPos = L_Range.index(I_TargetPos)+I_InserAdd
				S_TargetBase = S_Seq[I_SeqPos]
				S_TargetQual = S_Qual[I_SeqPos]
				try :
					I_TargetQual = D_Symbol2Qscore[S_TargetQual]
				except :
					I_TargetQual = 40
				#---------------------------------------------------------------------------------
				D_TargetMisDelBases[S_Selected_CustomMD].append(S_TargetBase)
				D_TargetMisDelQual[S_Selected_CustomMD].append(I_TargetQual)
		#-----------------------------------------------------------------------------------------
		else :
			S_DelInfo = re.findall('\D+',S_Selected_CustomMD)[0]
			D_TargetMisDelBases[S_Selected_CustomMD].append(S_DelInfo)
			D_TargetMisDelQual[S_Selected_CustomMD].append(36)
	#---------------------------------------------------------------------------------------------
	D_TargetMisDelCounts = { S_CustomMD:Counter(L_Bases) for S_CustomMD, L_Bases in D_TargetMisDelBases.iteritems() }
	D_TargetMisDelQual = { S_CustomMD:np.mean(L_Qauls) for S_CustomMD, L_Qauls in D_TargetMisDelQual.iteritems() }
	#============================================================================================= Merge CustomMD & CIGAR
	L_Selected_ErrCorrList = []
	for S_CustomMD,D_MisDelCounts in D_TargetMisDelCounts.iteritems() :
		I_del_check = len(re.findall('\W+',S_CustomMD))
		I_Pos = int(re.findall('\d+',S_CustomMD)[0])
		#-----------------------------------------------------------------------------------------
		D_MaxCaller = { I_count:S_Value for I_count, S_Value in zip(D_MisDelCounts.values(),D_MisDelCounts.keys()) }
		I_MaxCount = max(D_MaxCaller.keys())
		S_CalledValue = re.findall('\w+',D_MaxCaller[I_MaxCount])[0]
		I_Qaul = int(D_TargetMisDelQual[S_CustomMD])
		S_Qual = D_Qscore2Symbol[I_Qaul]
		#-----------------------------------------------------------------------------------------
		if I_del_check == 0 :
			L_Selected_ErrCorrList.append([S_CustomMD,'SNP',I_Pos,S_CalledValue,S_Qual])
		else :
			L_Selected_ErrCorrList.append([S_CustomMD,'D',I_Pos,S_CalledValue,S_Qual])
	#---------------------------------------------------------------------------------------------
	for S_Selected_CustomCIGAR in L_Inser_Selected_CustomCIGAR :
		#-----------------------------------------------------------------------------------------
		I_TargetIndex = df_Inser_ErrCor[df_Inser_ErrCor['CustomCIGAR']==S_Selected_CustomCIGAR].index
		#-----------------------------------------------------------------------------------------
		I_Pos = int(df_Inser_ErrCor['pos'][I_TargetIndex])
		S_Seq = list(df_Inser_ErrCor['seq'][I_TargetIndex])[0]
		S_Qual = list(df_Inser_ErrCor['qual'][I_TargetIndex])[0]
		#-----------------------------------------------------------------------------------------
		L_Selected_ErrCorrList.append([S_Selected_CustomCIGAR,'I',I_Pos,S_Seq,S_Qual])
	#---------------------------------------------------------------------------------------------
	df_MisDelInser_ErrCorr = pd.DataFrame(L_Selected_ErrCorrList,columns=['Custom','type','pos','seq','qual'])
	df_MisDelInser_ErrCorr_sorted = df_MisDelInser_ErrCorr.sort_values(by=['Custom']).reset_index(drop=True)
	#============================================================================================= Overlap case check
	L_MisDelInser_Indexes = df_MisDelInser_ErrCorr_sorted.index
	L_RmIndex = []
	for I_index in L_MisDelInser_Indexes :
		S_type = df_MisDelInser_ErrCorr_sorted['type'][I_index]
		I_pos = df_MisDelInser_ErrCorr_sorted['pos'][I_index]
		S_seq = df_MisDelInser_ErrCorr_sorted['seq'][I_index]
		#-----------------------------------------------------------------------------------------
		if S_type == 'D' :
			I_DelEndPos = I_pos+len(S_seq)
			L_Errors = list(df_MisDelInser_ErrCorr_sorted['pos'][L_MisDelInser_Indexes])
			I_OverlapCheck = len([ pos for pos in L_Errors if I_pos<=pos<=I_DelEndPos ])
			#-------------------------------------------------------------------------------------
			if 1 < I_OverlapCheck :
				L_RmIndex.append(I_index)
		elif S_type == 'I' :
			I_InsEndPos = I_pos+len(S_seq)
			L_Errors = list(df_MisDelInser_ErrCorr_sorted['pos'][L_MisDelInser_Indexes])
			I_OverlapCheck = len([ pos for pos in L_Errors if I_pos<=pos<=I_InsEndPos ])
			#-------------------------------------------------------------------------------------
			if 1 < I_OverlapCheck :
				L_RmIndex.append(I_index)
	L_SelectedIndex = [ pos for pos in L_MisDelInser_Indexes if not pos in L_RmIndex ]
	#---------------------------------------------------------------------------------------------
	df_MisDelInser_ErrCorr_sorted = df_MisDelInser_ErrCorr_sorted.loc[L_SelectedIndex]
	#============================================================================================= After NM calculator
	I_AfSnpCount = df_MisDelInser_ErrCorr_sorted[df_MisDelInser_ErrCorr_sorted['type']=='SNP'].shape[0]
	I_AfDelCount = df_MisDelInser_ErrCorr_sorted[df_MisDelInser_ErrCorr_sorted['type']=='D'].shape[0]
	I_AfInsCount = df_MisDelInser_ErrCorr_sorted[df_MisDelInser_ErrCorr_sorted['type']=='I'].shape[0]
	#---------------------------------------------------------------------------------------------
	I_NM = 0
	for I_index in df_MisDelInser_ErrCorr_sorted.index :
		S_Type = df_MisDelInser_ErrCorr_sorted['type'][I_index]
		S_Seq = df_MisDelInser_ErrCorr_sorted['seq'][I_index]
		I_SeqCount = len(S_Seq)
		if S_Type == 'SNP' :
			I_NM += I_SeqCount
		elif S_Type == 'D' :
			I_NM += I_SeqCount
		else :
			I_NM += I_SeqCount
	#============================================================================================= ErrorCorrection Output
	S_TargetLocus = '{}:{}-{}'.format(S_chr,I_MinStartPos,I_MaxEndPos)
	S_FastaRefSeq = os.popen('samtools faidx {} {}'.format(S_RefFasta,S_TargetLocus)).read()
	L_ErrCorrPos = range(I_MinStartPos,I_MaxEndPos+1)
	S_ErrCorrSeq = ''.join(S_FastaRefSeq.strip().split('\n')[1:]).upper()
	S_ErrCorrQual = ''.join(['E']*len(S_ErrCorrSeq))
	#---------------------------------------------------------------------------------------------
	#print S_ErrCorrSeq
	#print S_ErrCorrQual
	#---------------------------------------------------------------------------------------------
	I_InDelCount = 0
	for I_index in df_MisDelInser_ErrCorr_sorted.index :
		S_Custom = df_MisDelInser_ErrCorr_sorted['Custom'][I_index]
		S_Type = df_MisDelInser_ErrCorr_sorted['type'][I_index]
		I_Pos = df_MisDelInser_ErrCorr_sorted['pos'][I_index]
		S_Seq = df_MisDelInser_ErrCorr_sorted['seq'][I_index]
		S_Qual = df_MisDelInser_ErrCorr_sorted['qual'][I_index]
		#-----------------------------------------------------------------------------------------
		L_ErrCorrPos = range(I_MinStartPos,I_MaxEndPos+1+I_InDelCount)
		#-----------------------------------------------------------------------------------------
		if S_Type == 'SNP' :
			I_ErrCorrEditPos = L_ErrCorrPos.index(I_Pos+I_InDelCount)
			#-------------------------------------------------------------------------------------
			S_BackSeq = S_ErrCorrSeq[:I_ErrCorrEditPos]
			S_FrontSeq = S_ErrCorrSeq[I_ErrCorrEditPos+1:]
			S_BackQual = S_ErrCorrQual[:I_ErrCorrEditPos]
			S_FrontQual = S_ErrCorrQual[I_ErrCorrEditPos+1:]
			#-------------------------------------------------------------------------------------
			S_ErrCorrSeq = '{}{}{}'.format(S_BackSeq,S_Seq,S_FrontSeq)
			S_ErrCorrQual = '{}{}{}'.format(S_BackQual,S_Qual,S_FrontQual)
		#-----------------------------------------------------------------------------------------
		elif S_Type == 'D' :
			I_ErrCorrEditPos = L_ErrCorrPos.index(I_Pos+I_InDelCount)
			I_DeletedCount = len(S_Seq)
			#-------------------------------------------------------------------------------------
			S_BackSeq = S_ErrCorrSeq[:I_ErrCorrEditPos]
			S_FrontSeq = S_ErrCorrSeq[I_ErrCorrEditPos+I_DeletedCount:]
			S_BackQual = S_ErrCorrQual[:I_ErrCorrEditPos]
			S_FrontQual = S_ErrCorrQual[I_ErrCorrEditPos+I_DeletedCount:]
			#-------------------------------------------------------------------------------------
			S_ErrCorrSeq = '{}{}'.format(S_BackSeq,S_FrontSeq)
			S_ErrCorrQual = '{}{}'.format(S_BackQual,S_FrontQual)
			#-------------------------------------------------------------------------------------
			I_InDelCount -= I_DeletedCount
		#-----------------------------------------------------------------------------------------
		elif S_Type == 'I' :
			I_ErrCorrEditPos = L_ErrCorrPos.index(I_Pos+I_InDelCount)
			I_InsertedCount = len(S_Seq)
			#-------------------------------------------------------------------------------------
			S_BackSeq = S_ErrCorrSeq[:I_ErrCorrEditPos]
			S_FrontSeq = S_ErrCorrSeq[I_ErrCorrEditPos:]
			S_BackQual = S_ErrCorrQual[:I_ErrCorrEditPos]
			S_FrontQual = S_ErrCorrQual[I_ErrCorrEditPos:]
			#-------------------------------------------------------------------------------------
			S_ErrCorrSeq = '{}{}{}'.format(S_BackSeq,S_Seq,S_FrontSeq)
			S_ErrCorrQual = '{}{}{}'.format(S_BackQual,S_Qual,S_FrontQual)
			#-------------------------------------------------------------------------------------
			I_InDelCount += I_InsertedCount
	#---------------------------------------------------------------------------------------------
	#print S_ErrCorrSeq
	#print S_ErrCorrQual
	#============================================================================================= CIGAR maker
	#---------------------------------------------------------------------------------------------
	L_ErrCorrPos = range(I_MinStartPos,I_MaxEndPos+1)
	S_OutPutCIGAR = ''
	I_ErrCorrSeqLen = len(L_ErrCorrPos)
	I_ErrCorrSeqStart = 0
	#---------------------------------------------------------------------------------------------
	for I_index in df_MisDelInser_ErrCorr_sorted.index :
		S_Type = df_MisDelInser_ErrCorr_sorted['type'][I_index]
		I_Pos = df_MisDelInser_ErrCorr_sorted['pos'][I_index]
		S_Seq = df_MisDelInser_ErrCorr_sorted['seq'][I_index]
		#-----------------------------------------------------------------------------------------
		if S_Type == 'D' :
			I_MatchingCount = L_ErrCorrPos.index(I_Pos)-I_ErrCorrSeqStart
			I_DelCount = len(S_Seq)
			S_OutPutCIGAR += '{}M'.format(I_MatchingCount)
			S_OutPutCIGAR += '{}D'.format(I_DelCount)
			#-------------------------------------------------------------------------------------
			I_ErrCorrSeqStart += (I_MatchingCount+I_DelCount)
		elif S_Type == 'I' :
			I_MatchingCount = L_ErrCorrPos.index(I_Pos)-I_ErrCorrSeqStart
			I_InsCount = len(S_Seq)
			S_OutPutCIGAR += '{}M'.format(I_MatchingCount)
			S_OutPutCIGAR += '{}I'.format(I_InsCount)
			#-------------------------------------------------------------------------------------
			I_ErrCorrSeqStart += (I_MatchingCount)
	#---------------------------------------------------------------------------------------------
	I_EndMatchCount = I_ErrCorrSeqLen-I_ErrCorrSeqStart
	S_OutPutCIGAR += '{}M'.format(I_EndMatchCount)
	#---------------------------------------------------------------------------------------------
	#============================================================================================= SAM format
	S_Name = L_Names[I_ClusterOrder]
	#============================================================================================= UMI & GSP read distinguisher
	if S_ClusterCheck == 'umi' :
		L_OutPut = [
					S_Name,str(L_UmiFlag[I_ClusterOrder]),S_chr,str(I_MinStartPos),str(60),S_OutPutCIGAR,'=','PosMateRead',[I_MinStartPos,I_MaxEndPos],
					S_ErrCorrSeq,S_ErrCorrQual,
					'NM:i:{}'.format(I_NM),'MateCIGAR',
					'BN:Z:{},{},{}'.format(I_B4SnpCount,I_B4DelCount,I_B4InsCount),
					'AN:Z:{},{},{}'.format(I_AfSnpCount,I_AfDelCount,I_AfInsCount)
					]
		L_OutPut[13:13] = L_UmiTags[I_ClusterOrder]
	else :
		L_OutPut = [
					S_Name,str(L_GspFlag[I_ClusterOrder]),S_chr,str(I_MinStartPos),str(60),S_OutPutCIGAR,'=','PosMateRead',[I_MinStartPos,I_MaxEndPos],
					S_ErrCorrSeq,S_ErrCorrQual,
					'NM:i:{}'.format(I_NM),'MateCIGAR',
					'BN:Z:{},{},{}'.format(I_B4SnpCount,I_B4DelCount,I_B4InsCount),
					'AN:Z:{},{},{}'.format(I_AfSnpCount,I_AfDelCount,I_AfInsCount)
					]
		L_OutPut[13:13] = L_GspTags[I_ClusterOrder]
	return L_OutPut
######################################################################################################################################### Core Error Correction (EC) Class
class doEC(object):
	'''
	Description: This class is to do Deduplication based error correction of paired reads.
		
	Input:

		Required arguments:
		  -itxt INPUT_TXT, --input-txt INPUT_TXT
								* Required: Please provide the PATH of input cluster text file.
								
		  -odir OUTPUT_DIRECTORY, --output-directory OUTPUT_DIRECTORY
								* Required: Please provide the PATH of output directory.

		Optional arguments:
		  -ecc ERRORCORRECTION_CUTOFF, --errorcorrection-cutoff ERRORCORRECTION_CUTOFF
								Please provide the cutoff value of mismatch ratio.
								default) 0.8
								Depends on cluster read count(CRC), ecc can be different value.
									CRC <= 2 : ecc = 0.5	[fixed]
									CRC == 3 : ecc = 0.65	[fixed]
									CRC == 4 : ecc = 0.75	[fixed]
									CRC >= 5 : ecc = 0.8	[adjustable]
								
		  -rf REFERENCE_FASTA, --reference-fasta REFERENCE_FASTA
								Please provide the path of reference sequence fasta file.
								default) /Work/BIO/refs/hg19/ucsc.hg19.fasta
	'''
	#####################################################################################################################################
	def __init__(dd_self,reformatedCluster,GSP_samfile,samplename,outputDir,F_InPutECC_local,S_RefFasta_local):
		#--------------------------------------------------------------------------------------------------------------------------------
		#++++++++++++++++++++++++
		F_StartTime = time.time()
		#++++++++++++++++++++++++
		################################################################################################## convert cluster to dataframe
		L_UmiGspClusters = []
		for S_row in reformatedCluster:
			S_row_strip = S_row.strip()
			#---------------------------------------------------------------------------------------------
			if S_row_strip.startswith('+') :
				L_UmiGspClusters.append([])
			else:
				L_UmiGspClusters[-1].append(S_row_strip)
		#=================================================================================================
		global D_Symbol2Qscore,D_Qscore2Symbol,L_Names,L_UmiFlag,L_GspFlag,L_Chr,L_UmiTags,L_GspTags,L_UmiRowCluster,L_GspRowCluster,L_SelectedCols,F_InPutECC,S_RefFasta
		#--------------------------------------------------------------------------------------------------------------------------------
		D_Symbol2Qscore = {"!":0,'"':1,"#":2,"$":3,"%":4,"&":5,"'":6,"(":7,")":8,"*":9,"+":10,",":11,"-":12,".":13,"/":14,"0":15,
							"1":16,"2":17,"3":18,"4":19,"5":20,"6":21,"7":22,"8":23,"9":24,":":25,";":26,"<":27,"=":28,">":29,"?":30,
							"@":31,"A":32,"B":33,"C":34,"D":35,"E":36,"F":37,"G":38,"H":39,"I":40}
		D_Qscore2Symbol = {0: "!",1: '"',2: "#",3: "$",4: "%",5: "&",6: "'",7: "(",8: ")",9: "*",10: "+",11: ",",12: "-",13: ".",14: "/",15: "0",
							16: "1",17: "2",18: "3",19: "4",20: "5",21: "6",22: "7",23: "8",24: "9",25: ":",26: ";",27: "<",28: "=",29: ">",30: "?",
							31: "@",32: "A",33: "B",34: "C",35: "D",36: "E",37: "F",38: "G",39: "H",40: "I"}
		#--------------------------------------------------------------------------------------------------------------------------------
		F_InPutECC=F_InPutECC_local
		S_RefFasta=S_RefFasta_local
		#----------------------------------
		L_Names = []
		#----------------------------------
		L_UmiFlag = []
		L_GspFlag = []
		#----------------------------------
		L_Chr = []
		#----------------------------------
		L_UmiTags = []
		L_GspTags = []
		#----------------------------------
		L_UmiRowCluster = []
		L_GspRowCluster = []
		#----------------------------------
		L_SelectedCols = [0,4,6,10,11]
		#----------------------------------
		for L_rows in L_UmiGspClusters :
			I_DelimiterPos = (len(L_rows)/2)
			if I_DelimiterPos == 0 : #deal with bad clusters
				continue
			#---------------------------------------
			L_FirstUmiSplited = L_rows[:I_DelimiterPos][0].split('\t')
			'''
			#To Debug
			try:
				L_FirstUmiSplited = L_rows[:I_DelimiterPos][0].split('\t')
			except IndexError:
				print "row is %s:" %L_rows
				print I_DelimiterPos
				raise
			'''
			L_FirstGspSplited = L_rows[I_DelimiterPos+1:][0].split('\t')
			#---------------------------------------------------------------------------------------------
			L_Names.append(L_FirstUmiSplited[1])
			#---------------------------------------
			L_UmiFlag.append(L_FirstUmiSplited[2])
			L_GspFlag.append(L_FirstGspSplited[2])
			#---------------------------------------
			L_Chr.append(L_FirstUmiSplited[3])
			#---------------------------------------
			L_UmiTags.append(L_FirstUmiSplited[15:])
			L_GspTags.append(L_FirstGspSplited[15:])
			#---------------------------------------
			L_UmiRowCluster.append([])
			L_GspRowCluster.append([])
			#---------------------------------------------------------------------------------------------
			for S_UmiRow in L_rows[:I_DelimiterPos] :
				#-----------------------------------------------------------------------------------------
				L_SplitedRows = S_UmiRow.split('\t')
				#-----------------------------------------------------------------------------------------
				L_SelectedColElements = [ L_SplitedRows[i] for i in L_SelectedCols ]
				L_UmiRowCluster[-1].append(L_SelectedColElements)
				#-----------------------------------------------------------------------------------------
			for S_GspRow in L_rows[I_DelimiterPos+1:] :
				#-----------------------------------------------------------------------------------------
				L_SplitedRows = S_GspRow.split('\t')
				#-----------------------------------------------------------------------------------------
				L_SelectedColElements = [ L_SplitedRows[i] for i in L_SelectedCols ]
				L_GspRowCluster[-1].append(L_SelectedColElements)
				#-----------------------------------------------------------------------------------------
		#=================================================================================================
		L_ClusterDFs = []
		for L_UmiRows,L_GspRows in zip(L_UmiRowCluster,L_GspRowCluster) :
			df_UmiCluster = pd.DataFrame(L_UmiRows,
										columns=[
												'MD','StartPos','CIGAR','seq','qual'
												]
										)
			#---------------------------------------------------------------------------------------------
			df_GspCluster = pd.DataFrame(L_GspRows,
										columns=[
												'MD','StartPos','CIGAR','seq','qual'
												]
										)
			df_UmiCluster = df_UmiCluster.astype({'StartPos':int})
			df_GspCluster = df_GspCluster.astype({'StartPos':int})
			#---------------------------------------------------------------------------------------------
			L_ClusterDFs.append([df_UmiCluster,df_GspCluster])
		#=================================================================================================
		#####################################################################################################################################
		##################################################################################################################################### Run Error Correction
		#####################################################################################################################################
		L_EC_OutPut = []
		for I_index,L_DFs in enumerate(L_ClusterDFs) :
			df_UmiCluster = L_DFs[0]
			df_GspCluster = L_DFs[-1]
			#================================================================================================================================ Clustering Correction
			L_UmiClusterEC = ClusterErrorCorrection(I_index,df_UmiCluster,'umi')
			L_GspClusterEC = ClusterErrorCorrection(I_index,df_GspCluster,'gsp')
			#================================================================================================================================ Extract Clustered values
			I_UmiMinStartPos = L_UmiClusterEC[4]
			I_GspMinStartPos = L_GspClusterEC[4]
			I_UmiMaxEndPos = L_UmiClusterEC[5]
			I_GspMaxEndPos = L_GspClusterEC[5]
			D_UmiDepth = L_UmiClusterEC[6]
			D_GspDepth = L_GspClusterEC[6]
			df_UmiMisDel_ErrCor = L_UmiClusterEC[10]
			df_GspMisDel_ErrCor = L_GspClusterEC[10]
			df_UmiInser_ErrCor = L_UmiClusterEC[11]
			df_GspInser_ErrCor = L_GspClusterEC[11]
			#================================================================================================================================ Calculate UMI & GSP overlap values
			df_UmiGspMisDel_ErrCor = pd.concat([df_UmiMisDel_ErrCor,df_GspMisDel_ErrCor],ignore_index=True)
			df_UmiGspInser_ErrCor = pd.concat([df_UmiInser_ErrCor,df_GspInser_ErrCor],ignore_index=True)
			#--------------------------------------------------------------------------------------------------------------------------------
			df_UmiGspMisDel_Counts = df_UmiGspMisDel_ErrCor.groupby(['pos'])['count'].sum().reset_index()
			df_UmiGspInser_Counts = df_UmiGspInser_ErrCor.groupby(['pos'])['count'].sum().reset_index()
			#--------------------------------------------------------------------------------------------------------------------------------
			L_UmiGspMisDel_Depth = [ D_UmiDepth[pos]+D_GspDepth[pos] for pos in df_UmiGspMisDel_Counts['pos'] ]
			L_UmiGspInser_Depth = [ D_UmiDepth[pos]+D_GspDepth[pos] for pos in df_UmiGspInser_Counts['pos'] ]
			#--------------------------------------------------------------------------------------------------------------------------------
			df_UmiGspMisDel_Counts.insert(loc=df_UmiGspMisDel_Counts.shape[1],column='depth',value=L_UmiGspMisDel_Depth)
			df_UmiGspInser_Counts.insert(loc=df_UmiGspInser_Counts.shape[1],column='depth',value=L_UmiGspInser_Depth)
			#--------------------------------------------------------------------------------------------------------------------------------
			A_MisDel_ratio = df_UmiGspMisDel_Counts['count']/df_UmiGspMisDel_Counts['depth']
			A_Inser_ratio = df_UmiGspInser_Counts['count']/df_UmiGspInser_Counts['depth']
			#--------------------------------------------------------------------------------------------------------------------------------
			df_UmiGspMisDel_Counts.insert(loc=df_UmiGspMisDel_Counts.shape[1],column='ratio',value=A_MisDel_ratio)
			df_UmiGspInser_Counts.insert(loc=df_UmiGspInser_Counts.shape[1],column='ratio',value=A_Inser_ratio)
			#================================================================================================================================ Flter with threshold values
			L_MisDel_SelectedPos = []
			for I_index in df_UmiGspMisDel_Counts.index :
				I_pos = df_UmiGspMisDel_Counts['pos'][I_index]
				I_TargetPosDepth = df_UmiGspMisDel_Counts['depth'][I_index]
				F_ratio = df_UmiGspMisDel_Counts['ratio'][I_index]
				#----------------------------------------------------------------------------------------------------------------------------
				if I_TargetPosDepth == 2 and 1 <= F_ratio :
					L_MisDel_SelectedPos.append(I_pos)
				elif I_TargetPosDepth == 3 and 0.65 <= F_ratio :
					L_MisDel_SelectedPos.append(I_pos)
				elif I_TargetPosDepth == 4 and 0.75 <= F_ratio :
					L_MisDel_SelectedPos.append(I_pos)
				elif I_TargetPosDepth >= 5 and F_InPutECC <= F_ratio :
					L_MisDel_SelectedPos.append(I_pos)
			#--------------------------------------------------------------------------------------------------------------------------------
			L_Inser_SelectedPos = []
			for I_index in df_UmiGspInser_Counts.index :
				I_pos = df_UmiGspInser_Counts['pos'][I_index]
				I_TargetPosDepth = df_UmiGspInser_Counts['depth'][I_index]
				F_ratio = df_UmiGspInser_Counts['ratio'][I_index]
				#----------------------------------------------------------------------------------------------------------------------------
				if I_TargetPosDepth == 2 and 1 <= F_ratio :
					L_Inser_SelectedPos.append(I_pos)
				elif I_TargetPosDepth == 3 and 0.65 <= F_ratio :
					L_Inser_SelectedPos.append(I_pos)
				elif I_TargetPosDepth == 4 and 0.75 <= F_ratio :
					L_Inser_SelectedPos.append(I_pos)
				elif I_TargetPosDepth >= 5 and F_InPutECC <= F_ratio :
					L_Inser_SelectedPos.append(I_pos)
			#--------------------------------------------------------------------------------------------------------------------------------
			L_UmiMisDel_SelectedPos = [ pos for pos in L_MisDel_SelectedPos if pos in list(df_UmiMisDel_ErrCor['pos']) ]
			L_UmiInser_SelectedPos = [ pos for pos in L_Inser_SelectedPos if pos in list(df_UmiInser_ErrCor['pos']) ]
			L_GspMisDel_SelectedPos = [ pos for pos in L_MisDel_SelectedPos if pos in list(df_GspMisDel_ErrCor['pos']) ]
			L_GspInser_SelectedPos = [ pos for pos in L_Inser_SelectedPos if pos in list(df_GspInser_ErrCor['pos']) ]
			#================================================================================================================================ Overlap Correction
			L_UmiOutPut = OverlapErrorCorrection(L_UmiClusterEC[0],L_UmiClusterEC[1],L_UmiClusterEC[2],
												L_UmiClusterEC[3],L_UmiClusterEC[4],L_UmiClusterEC[5],
												L_UmiClusterEC[6],L_UmiClusterEC[7],L_UmiClusterEC[8],
												L_UmiClusterEC[9],
												L_UmiClusterEC[10],L_UmiClusterEC[11],
												L_UmiMisDel_SelectedPos,L_UmiInser_SelectedPos
												)
			L_GspOutPut = OverlapErrorCorrection(L_GspClusterEC[0],L_GspClusterEC[1],L_GspClusterEC[2],
												L_GspClusterEC[3],L_GspClusterEC[4],L_GspClusterEC[5],
												L_GspClusterEC[6],L_GspClusterEC[7],L_GspClusterEC[8],
												L_GspClusterEC[9],
												L_GspClusterEC[10],L_GspClusterEC[11],
												L_GspMisDel_SelectedPos,L_GspInser_SelectedPos
												)
			#================================================================================================================================ Format bulider
			I_TemplateLen = abs(L_UmiOutPut[8][0]-L_GspOutPut[8][-1])+1
			#--------------------------------------------------------------------------------------------------------------------------------
			L_UmiOutPut[7] = str(L_GspOutPut[3])
			L_GspOutPut[7] = str(L_UmiOutPut[3])
			#--------------------------------------------------------------------------------------------------------------------------------
			L_UmiOutPut[12] = 'MC:Z:{}'.format(L_GspOutPut[5])
			L_GspOutPut[12] = 'MC:Z:{}'.format(L_UmiOutPut[5])
			#--------------------------------------------------------------------------------------------------------------------------------
			L_UmiOutPut[8] = str(I_TemplateLen)
			L_GspOutPut[8] = str(I_TemplateLen*-1)
			#--------------------------------------------------------------------------------------------------------------------------------
			L_EC_OutPut.append("\t".join(L_UmiOutPut))
			L_EC_OutPut.append("\t".join(L_GspOutPut))
		#++++++++++++++++++++++++
		F_EndTime = time.time()
		#++++++++++++++++++++++++
		#==================================================================================================================================== Sam file creator
		#---------------------------------------Write the cluster
		writeDedupped_FH=open("%s/%s.incCLUSTER.sam"%(outputDir,samplename),'w+')
		writeDedupped_FH.write("\n".join(L_EC_OutPut))
		writeDedupped_FH.write("\n")
		writeDedupped_FH.close()
		#let's write the completion file
		completion_filename="%s/%s_EC.completed" % (outputDir,samplename)
		completion_FH=open(completion_filename,'w+')
		completion_FH.write('completed at %s' % time.strftime("%b %d %Y %H:%M:%S", time.localtime()))
		completion_FH.write('\nRunning time: {}'.format(F_EndTime-F_StartTime))
		completion_FH.close()		
		#====================================================================================================================================
#############################################################################################################################################
if __name__=='__main__': 
	M_parser = argparse.ArgumentParser(description=textwrap.dedent('''\n
		This script is only for error correction & deduplication algorithm using the output of DeepClean's clustering algorithm.
		\n'''),add_help=False,formatter_class=argparse.RawTextHelpFormatter)
	#=====================
	S_version = 'ErrorCorrection_v02'
	S_PWD = os.getcwd()
	#=====================
	#------------------------------------------------------------------------------------------------------------------------------------
	M_parser_Required = M_parser.add_argument_group('Required arguments')
	M_parser_Required.add_argument('-itxt','--input-txt',type=str,required=True,
		help=textwrap.dedent('''\
			* Required: Please provide the PATH of input cluster text file.
			\n'''))
	M_parser_Required.add_argument('-odir','--output-directory',type=str,default=S_PWD,
		help=textwrap.dedent('\
			* Required: Please provide the PATH of output directory.\n\
			current path) {}\n\
			'.format(S_PWD)))
	#------------------------------------------------------------------------------------------------------------------------------------
	M_parser_Optional = M_parser.add_argument_group('Optional arguments')
	M_parser_Optional.add_argument('-ecc','--errorcorrection-cutoff',type=float,default=0.8,
		help=textwrap.dedent('''\
			Please provide the cutoff value of mismatch ratio.
			default) 0.8
			Depends on cluster read count(CRC), ecc can be different value.
				CRC <= 2 : ecc = 0.5	[fixed]
				CRC == 3 : ecc = 0.65	[fixed]
				CRC == 4 : ecc = 0.75	[fixed]
				CRC >= 5 : ecc = 0.8	[adjustable]
			\n'''))
	M_parser_Optional.add_argument('-rf','--reference-fasta',type=str,default='/Work/BIO/refs/hg19/ucsc.hg19.fasta',
		help=textwrap.dedent('''\
			Please provide the path of reference sequence fasta file.
			default) /Work/BIO/refs/hg19/ucsc.hg19.fasta
			\n'''))
	M_parser_Optional.add_argument('-h','--help',action='help', help='show this help message and exit')
	M_parser_Optional.add_argument('-v','--version', action='version', version=S_version)
	#====================================================================================================================================
	M_args = M_parser.parse_args()
	#====================================================================================================================================
	S_inTXT = M_args.input_txt
	S_OutDir = M_args.output_directory
	#------------------------------------------------------------------------------------------------------------------------------------
	F_InPutECC = M_args.errorcorrection_cutoff
	S_RefFasta = M_args.reference_fasta
	#====================================================================================================================================
	S_OutSamName = S_inTXT.split('/')[-1].split('.txt')[0]
	S_InName = '{}_EC_result'.format(S_inTXT.split('/')[-1].split('.')[0])
	#====================================================================================================================================
	L_UmiGspClusters = []
	with open(S_inTXT) as f :
		for S_row in f:
			L_UmiGspClusters.append(S_row)
	doEC_instance = doEC(L_UmiGspClusters,True,S_OutSamName, S_OutDir,F_InPutECC,S_RefFasta)
#############################################################################################################################################