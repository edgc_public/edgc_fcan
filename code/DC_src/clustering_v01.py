#!/usr/bin/env python
import os
import sys
import re
import logging
import time as tm
import difflib as dl
import argparse
import textwrap
from multiprocessing import Process, Pool
import gc
from random import randint

'''
3.2V:
>also annotate reads with the total number of reads in cluster with following tags
			|____RCRC:i: --> Reads's Cluster's Read count --> Annotated at the EC/deduplication level for only the dedupped read
			|____RCUTC:i: --> Reads's Cluster's UMI type count --> Annotated at the Clustering level for all the reads
'''

class CoreClustering():
	'''
	Description: This class is to do UMI based clustering of paired reads.
	Input args:
		1) A list that contains the reads per position; different from the previous version
		2) start pinter location in file (byte position)
		3) end pointer location in file (byte position)
		2) Number of mismatches allowed in UMI
		3) value of 'n': Exclude clusters of 'n' reads from main cluster and write to a separate file
		4) 'True' or 'False' : Show progress bar or not
		5) 'True' or 'False' : Weather to write the final cluster file to the disk?
		6) Optional, If 8th arguement is true, please specify the location of the output directly, default is the launch directory
		7)Optional, If 8th arguement is true, please specify the prefix of the output filename.
	Class variable:
		<isntance>.clusters: list of file byte locations of dedupped UMI reads
		<isntance>.excludedClusters_allData: list of file byte locations of excluded read clusters
	'''	

	def __init__(cc_self,UMIreads,umi_mismatch,excludeCluster,pb,writeClusterData,GSP_bytePos_dict,GSP_samfile,doErrorCorrection,bestASmethod,writeUnpaired,srcpath,workingDirectory=None,output_prefix=None):

		if srcpath in sys.path:
			pass
		else:
			sys.path.append(srcpath)

		writeClusterData=writeClusterData
		startTime=tm.time()
		#print "\n====================>StartPointer: %d" % startPointer
		#print "\n====================>EndPointer: %d" % endPointer
		#handel the input type
		excludeCluster=int(excludeCluster)
		umi_mismatch=int(umi_mismatch)
		
		#dictionary to record UMI distribution
		cc_self.umiDistrib_list={}
		
		if workingDirectory == None:
			workingDirectory= os.getcwd()
		
		#open the UMI stats file in append mode
		umiStats_FH=open("%s/%s_clusterUMI.stats" % (workingDirectory,output_prefix),'w+')
		
		#let's fix the number of parts to 1:
		partsNum=1
		
		"""
		#not using vec anymore
		vec=False
		vecCutOff=None
		vecAllowedMismatches=None
		"""
		
		####fastq ID column number
		fastqID_column=1
		
		#handel the progressbar
		if pb == 'True' or (pb == True):
			try:
				from tqdm import tqdm
				pb=True
			except ImportError:
				print "\n\nERROR: Could not import module 'tqdm'. Either install the module or use option '-pb' to disable it."
				pb=False
		else:
			pb=False

		####Exclude/drop clusters with 'n' reads
		if excludeCluster > 0:
			#print "|____Exclude all the clusters that contain 'n' or less number of reads, where n: %d" %excludeCluster
			readBasedFilter=True
			excludedClusters=[] #to save excluded clusters
		elif excludeCluster == 0:
			#print "|____Exlude any cluster: False"
			readBasedFilter=False		
		
		###################################################################################################CODE STARTS HERE
		def getSimilarUMIs (query,targetList,allowedMismatchesNumber):
			'''
			Description:
				For a query UMI and UMI list, returns one level similar UMI's
			Input: 
				1) query: single UMI
				2) targetList: List of UMI to be searched
				3) allowedMismatchesNumber : How many mismatches to allow
			
			Output:
				list of UMIs, similar to query UMI
			'''
			similarUMIs = [] #let's start with 
			for eachUMI in targetList:
				if not eachUMI == query: #avoid the query UMI
					differentCount=0
					for index,eachBase in enumerate(eachUMI):
						if not eachBase == query[index]:
							#print "%d: %s" %(index,eachBase)
							differentCount += 1
							if differentCount > allowedMismatchesNumber: #if this UMI is already disqualified, stop the iteration
								break
					else: #Equivalent to '#if not differentCount > allowedMismatchesNumber:'
						similarUMIs.append(eachUMI)
			#clear the memory
			del targetList
			#return
			return similarUMIs

		def getUMI_tree (query,targetList,allowedMismatchesNumber):
			'''
			Description: Find a tree of similar UMIs
			Input: 
				1) query: Highest occurring UMI
				2) targetList: List of UMI to be searched at a position
				3) allowedMismatchesNumber : How many mismatches to allow
			
			Output:
				list of UMIs, similar to query UMI
			'''
			#there will be a loop that will travel on the branches of UMI tree
			similarUMIs = [query]
			if not allowedMismatchesNumber == 0:
				queryList=[query] #make it set, not the list so that we can do substraction of list
				#let's start the tree exploration
				for queryUMI in queryList:			
					node1_similarUMIs=getSimilarUMIs(queryUMI,targetList,allowedMismatchesNumber) #search on root node (query) and get the node0 UMIs
					similarUMIs.extend(node1_similarUMIs)#let's add the retreived UMIs to the 'similarUMIs'
					#queryList.remove(queryUMI)#first thing, remove the already travelled node # Learn: Do not remove or else the index will change and new added element may never be travelled
					queryList.extend(node1_similarUMIs)#second, whatever we retreived, becomes the query now
					[targetList.remove(doneUMI) for doneUMI in node1_similarUMIs]	#now, let's substract the already found UMI's from the target list

			#clear the memory
			del targetList
			#return
			return similarUMIs	
			
			
		perPos_readCount = 0
		###########FUNCTIONS
		#global umi_mismatch
		def makeCluster(UMIcountDictionary,UMIdataDictionary,umi_mismatch,readBasedFilter,excludeNumber,chrPos,umiStats_FH):
			'''
			Input:
				1) UMIcountDictionary --> For a position, this dictionary contains the UMI type and their count
				2) UMIdataDictionary --> For a position, this dictionary contains the UMI type and related data
				3) umi_mismatch --> allowed UMI mismatches
				4) readBasedFilter --> Using reads/cluster based filter or not
				5) dropNumber --> At max how many reads in cluster to be dropped?
				6) excludeNumber --> Al max, how many reads in a cluster to be excluded?
			output:
				1) returns generator/list that contains the clusters, separated by +
				+
				chr1:11176887	0	ACATGCTT	127	127	NB502108:6:HLTYTAFXX:4:11508:10783:11555	67	chr1	11176887	60	127M	=	11176885	-3	ATGGCATCACAATCAATAGGGAACTAAGGCTAATGTTGTAAAAGAGACCTTACATATACAATACCAATATTTATTTACCAAAAAGCCATATATTTAATTGGAAAACCAAATGAAACCATTCAGGAAA	EEE6EEEEEAEEEEEE/EEEEAEE/EEEA//EEEEEEAEEAEEEEE/EEEEEEEEEEEEEEEEE/EE<EE/EEAEEEEAEEEEEEAEAEEEEE///EEEA/EAEAAAEEEA/EAE6A<E</<E/EEE	MC:Z:20S129M	MD:Z:127	RG:Z:Sample	NM:i:0	AS:i:127	XS:i:21	RX:Z:ACATGCTT
				chr1:11176887	0	TCATGCTT	129	129	NB502108:6:HLTYTAFXX:1:11311:24752:4124	67	chr1	11176887	60	129M	=	11176885	-3	ATGGCATCACAATCAATAGGGAACTAAGGCTAATGTTGTAAAAGAGACCTTACATATACAATACCAATATTTATTTACCAAAAAGCCATATATTTAATTGGAAAACCAAATGAAACCATTCAGGAAAAC	EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEAEEEEEEEEEEEEEEAEEEEEEEEEEEEEEEEAEEEEEEAEEEEEEEAEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE<EEEEEEAEEE<EAEAAEEA	MC:Z:20S131M	MD:Z:129	RG:Z:Sample	NM:i:0	AS:i:129	XS:i:21	RX:Z:TCATGCTT
				chr1:11176887	0	TCATGCTT	129	129	NB502108:6:HLTYTAFXX:1:21202:11985:4187	67	chr1	11176887	60	129M	=	11176885	-3	ATGGCATCACAATCAATAGGGAACTAAGGCTAATGTTGTAAAAGAGACCTTACATATACAATACCAATATTTATTTACCAAAAAGCCATATATTTAATTGGAAAACCAAATGAAACCATTCAGGAAAAC	EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEAEEEEEEEEEEEEEAEEEEEEEAEEEEEEEEEEEEEEEAAEAEAEEAAE/EEEEEEEEAEAEEEEAEE6EAEAEAEEEEEA	MC:Z:20S131M	MD:Z:129	RG:Z:Sample	NM:i:0	AS:i:129	XS:i:21	RX:Z:TCATGCTT
				chr1:11176887	0	TCATGCTT	129	129	NB502108:6:HLTYTAFXX:1:21212:14761:12366	67	chr1	11176887	60	129M	=	11176885	-3	ATGGCATCACAATCAATAGGGAACTAAGGCTAATGTTGTAAAAGAGACCTTACATATACAATACCAATATTTATTTACCAAAAAGCCATATATTTAATTGGAAAACCAAATGAAACCATTCAGGAAAAC	EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEAEEEEEEEEEEEEEEEEEEEEEEEEAEEEEEEEEEEEEEAEEEEEEEEEEEEAEEEAEEEEEEEEEEEEEEEEEEEE6EEEAAEA<EEEA	MC:Z:20S131M	MD:Z:129	RG:Z:Sample	NM:i:0	AS:i:129	XS:i:21	RX:Z:TCATGCTT
				chr1:11176887	0	TCATGCTT	129	129	NB502108:6:HLTYTAFXX:2:21201:3375:12501	67	chr1	11176887	60	129M	=	11176885	-3	ATGGCATCACAATCAATAGGGAACTAAGGCTAATGTTGTAAAAGAGACCTTACATATACAATACCAATATTTATTTACCAAAAAGCCATATATTTAATTGGAAAACCAAATGAAACCATTCAGGAAAAC	AEEEEEEEAEEEEEEEEAEEEEEEE6EEEAE6EEEEEEEAEEEEE<AEEEEEE/EEAEEEEEEEEAAEAAAAAEEEEEEAAEEEAEEEEAE/EAA/AEEEEEEEEEE/<EEE/EEA6<EA<<E/AAAAA	MC:Z:20S131M	MD:Z:129	RG:Z:Sample	NM:i:0	AS:i:129	XS:i:21	RX:Z:TCATGCTT
				+
				chr1:11176887	0	CCAACCTG	127	127	NB502108:6:HLTYTAFXX:2:21105:18277:9507	67	chr1	11176887	60	127M	=	11176885	-3	ATGGCATCACAATCAATAGGGAACTAAGGCTAATGTTGTAAAAGAGACCTTACATATACAATACCAATATTTATTTACCAAAAAGCCATATATTTAATTGGAAAACCAAATGAAACCATTCAGGAAA	EEAAEEEAEEEEEEEEEEEAEAEE/EE6EEEEEEEEEE/EEEEE/AEEEE/EEEEE<EEEE/AEE<E/EE//E/6EEAEE/EEEE<AE///EEEE<EAE/AEE/EEEAEA<EEE<AAAE//EE/AEA	MC:Z:20S129M	MD:Z:127	RG:Z:Sample	NM:i:0	AS:i:127	XS:i:21	RX:Z:CCAACCTG
				chr1:11176887	0	GCAACCTG	129	129	NB502108:6:HLTYTAFXX:1:11201:3917:10219	67	chr1	11176887	60	129M	=	11176885	-3	ATGGCATCACAATCAATAGGGAACTAAGGCTAATGTTGTAAAAGAGACCTTACATATACAATACCAATATTTATTTACCAAAAAGCCATATATTTAATTGGAAAACCAAATGAAACCATTCAGGAAAAC	EEEEEEEEEEEEEEEEEEEEEEEEEAEEEEEEEEEEEEEAEEEEEEAEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEAEEEEEAEEEEEEEEEEEEEEEEEEEEEEAEEEEEEEEEEEEEEEEAEEE	MC:Z:20S131M	MD:Z:129	RG:Z:Sample	NM:i:0	AS:i:129	XS:i:21	RX:Z:
				2) returns the generator/list of excluded clusters
				3) returns the list of dedupped selected fastqids
			Algo:
				1) from the count list, find the highest count UMI
				2) search the similar UMIs using difflib library
				3) make cluster of all the returned similar UMIs and add it to the generator to be returned
				4) substract the already used UMI's from the UMIcountDictionary
				5) keep doing until the dictionary becomes empty
				6) return (yield) the generator
			'''
			clusterNum = 1
			collectClusters_list=[]
			excludedClusters=[]
			UMI_fastqID_set=set()
			umiDistrib_list_part=[]
			
			#for the UMI stats
			umiStatList=[] 
			
			while (len(UMIcountDictionary) > 0):
				UMI_list=[key for key,value in sorted(UMIcountDictionary.iteritems(), key=lambda (k,v): (v,k))] #get a list of all UMI's and in Ascending order by their count
				maxUMI = UMI_list[-1] #get last UMI of the sorted by count UMI in ascending order
				if umi_mismatch == 0:
					similarUMI_set=set([maxUMI])
				else:
					similarUMI_set=set(getUMI_tree(maxUMI,UMI_list, umi_mismatch)) #get similar UMIs' list

				#print UMI_list
				#print "Cluster %d:\n%s" % (clusterNum,similarUMI_set)
				#print "\n"
				
				#make clusters of all the similar UMIs and update the dictionary
				R1_readPos_list=[]
				#R2_read_list=[]
				
				totalUmitypeInCLuster=len(similarUMI_set)
				for eachUMI in similarUMI_set:
					try:
						#R1_readPos_list.extend("%s\tUMI:A:%s\n"%(x.replace("\n",''),eachUMI) for x in UMIdataDictionary[eachUMI])
						R1_readPos_list.extend(UMIdataDictionary[eachUMI])
						#print UMIdataDictionary[eachUMI]
						#print R1_readPos_list
						#sys.exit()
				
						try:
							del UMIcountDictionary[eachUMI] #delete the already done UMI from the 'UMIcountDictionary' dictionary
						except KeyError:
							pass
						'''
						try:
							UMI_list.remove(eachUMI) #delete the UMI from the UMI list too, as it need not to be searched again
						except ValueError:
							pass
						'''
					except KeyError:
						print "\n\nERROR: Should not happen. Key absent in respective UMI Data Dict.\nKey:%s\nLength:%d" % (eachUMI,len(eachUMI))
						print UMIdataDictionary
						print UMIcountDictionary
						#os.kill(os.getpid(), 9)
						raise
				
				###let's collect the UMI stats data
				umiStatList.append("%s:%s\t%d" %(chrPos,maxUMI,len(R1_readPos_list)))
				
				#let's filter out clusters based on the number of reads
				if readBasedFilter:
					readsInCluster=len(R1_readPos_list)
					
					#update the cluster with RCRC and RCUTC tags, and palce the read length to the end again
					#newTags=["RCRC:i:%d"%readsInCluster,"RCUTC:i:%d"%totalUmitypeInCLuster]
					for x in R1_readPos_list:
						x.insert(-1,"RC:i:%d"%readsInCluster)
						x.insert(-1,"UC:i:%d"%totalUmitypeInCLuster)
						x.insert(-1,"UD:Z:%s:%s"%(maxUMI,"_".join(similarUMI_set))) #UD --> UMI Data

					#R1_readPos_list_updated=[ x.extend(newTags) for x in R1_readPos_list]
					'''
					print "\n\n======================"
					print R1_readPos_list
					print "\n"
					sys.exit()
					'''
					if excludeNumber > 0:#save this cluster somewhere else
						if readsInCluster <= excludeNumber: #save this cluster	
							excludedClusters.append("+\n")
							excludedClusters.extend(R1_readPos_list)					
						else:
							collectClusters_list.append("+")
							collectClusters_list.extend(R1_readPos_list)
				else:
					readsInCluster=len(R1_readPos_list)
					for x in R1_readPos_list:
						x.insert(-1,"RC:i:%d"%readsInCluster)
						x.insert(-1,"UC:i:%d"%totalUmitypeInCLuster)
						#x.insert(-1,"CP:Z:%s-%s"%(chrPos,maxUMI))					
						#x.insert(-1,"CP:Z:%s:%s"%(chrPos,"_".join(similarUMI_set)))
						x.insert(-1,"UD:Z:%s:%s"%(maxUMI,"_".join(similarUMI_set)))
					collectClusters_list.append("+")
					collectClusters_list.extend(R1_readPos_list)
				
				clusterNum += 1

			UMIcountDictionary.clear() ####free some space
			UMIdataDictionary.clear()  ####free some space
				
			#Free up memory
			del R1_readPos_list[:]
			del UMI_list[:]
			#gc.collect()
			
			#update the UMI stats file
			umiStats_FH.write("\n".join(umiStatList))
			umiStats_FH.write("\n")
			del umiStatList[:]
			
			#return
			return collectClusters_list,excludedClusters

		def getQstats(inputQualityString):
			'''
			Description: Returns a score of fastq quality string.
			Input:
				1) First Argument: Quality string
			returned output: For 'all' case, else only the input stat type is returned
					a) 'mean' : mean quality of the read
					c) 'length' : length of the read
			'''	
			# !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
			qs_dic={'!':0,'"':1,'#':2,'$':3,'%':4,'&':5,"'":6,'(':7,')':8,'*':9,
			'+':10,',':11,'-':12,'.':13,'/':14,'0':15,'1':16,'2':17,'3':18,'4':19,
			'5':20,'6':21,'7':22,'8':23,'9':24,':':25,';':26,'<':27,'=':28,'>':29,
			'?':30,'@':31,'A':32,'B':33,'C':34,'D':35,'E':36,'F':37,'G':38,'H':39,'I':40,
			'J':41,'K':42,'L':43,'M':44,'N':45,'O':46,'P':47,'Q':48,'R':49,'S':50,'T':51,'U':52,'V':53,'W':54,'X':55,'Y':56,'Z':57,'[':58,'\\':59,']':60,
			'^':61,'_':62,'`':63,'a':64,'b':65,'c':66,'d':67,'e':68,'f':69,'g':70,'h':71,'i':72,'j':73,'k':74,'l':75,'m':76,'n':77,'o':78,'p':79,'q':80,
			'r':81,'s':82,'t':83,'u':84,'v':85,'w':86,'x':87,'y':88,'z':89,'{':90,'|':91,'}':92,'~':93
			}
			scoreList=[ qs_dic[qualLetter] for qualLetter in inputQualityString]
			length=len(scoreList)
			averageScore=float(sum(scoreList))/float(length)
			return averageScore,length

		def getQstatsUMI(inputQualityString,readLength):
			'''
			Description: Returns a score of fastq quality string.
			Input:
				1) First Argument: Quality string
			returned output: For 'all' case, else only the input stat type is returned
					a) 'mean' : mean quality of the read
					c) 'length' : length of the read
			'''	
			# !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
			qs_dic={'!':0,'"':1,'#':2,'$':3,'%':4,'&':5,"'":6,'(':7,')':8,'*':9,
			'+':10,',':11,'-':12,'.':13,'/':14,'0':15,'1':16,'2':17,'3':18,'4':19,
			'5':20,'6':21,'7':22,'8':23,'9':24,':':25,';':26,'<':27,'=':28,'>':29,
			'?':30,'@':31,'A':32,'B':33,'C':34,'D':35,'E':36,'F':37,'G':38,'H':39,'I':40,
			'J':41,'K':42,'L':43,'M':44,'N':45,'O':46,'P':47,'Q':48,'R':49,'S':50,'T':51,'U':52,'V':53,'W':54,'X':55,'Y':56,'Z':57,'[':58,'\\':59,']':60,
			'^':61,'_':62,'`':63,'a':64,'b':65,'c':66,'d':67,'e':68,'f':69,'g':70,'h':71,'i':72,'j':73,'k':74,'l':75,'m':76,'n':77,'o':78,'p':79,'q':80,
			'r':81,'s':82,'t':83,'u':84,'v':85,'w':86,'x':87,'y':88,'z':89,'{':90,'|':91,'}':92,'~':93
			}
			scoreList=[ qs_dic[qualLetter] for qualLetter in inputQualityString]
			averageScore=float(sum(scoreList))/float(readLength)
			return averageScore
		
		def reformatUMIRead(Samread_list):
			#It accepts list, instead of read as a string; because we already have UMI-reads in list format
			'''
			Description: Convert the samRead into EC input type read
			EC read format: chr:pos	NMtagValue	UMI	readLength	MDtagValue	average_readQual	<wholeRead>	
			'''
			
			#Samread_list=Samread.split("\t")
			#tags="\t".join(Samread_list[11:])
			#We know that in a Sam file, alway 0-10 fields (starting-11) are fixed for FastqID-readQuality
			#And mostly this order follows: NM:i:0	MD:Z:41	MC:Z:41M	AS:i:41	XS:i:20	RG:Z:A549_S2
			#By default, let's assume that 11th field is NMtag and 12 field is MD tag; 
			#if we can not find these tag at these fields, look in other fields after 10th field
			'''
			#Not needed for new ErrorCorrection
			try:
				NMtagValue=int(Samread_list[11].replace('NM:i:',''))
			except ValueError:#if some other tag is there, it will not be replaced, and can not be converted into int
			#find by looping
				for i in Samread_list[12:]:
					try:
						NMtagValue=int(i.replace('NM:i:',''))
						break
					except ValueError:
						continue
			'''		
			if 'MD:' in Samread_list[12]:#As MD tagg are stringg, above stategy will not work for MD tags
				MDtagValue=Samread_list[12].replace("MD:Z:",'')
			else:#if some other tag is there, it will not be replaced, and can not be converted into int
				#print "Prob in MD tag"
				#print Samread_list[12].replace("MD:Z:",'')
				#print Samread_list
				return None
				#sys.exit()			
			#find by looping
				for i in Samread_list[11:]:
					if 'MD:' in i:
						MDtagValue=i.replace("MD:Z:",'')
						break
					else:
						continue						
						
				
			#NMtagValue=re.search("(?P<NMtag>\tNM:i:)(?P<NMscore>[0-9]*)",Samread).group('NMscore')
			#MDtagValue=re.search("(?P<MDtag>\tMD:Z:)(?P<MDvalue>[0-9A-Z]*)",Samread).group('MDvalue')
			cigar=Samread_list[5]
			readSeq=Samread_list[9]
			readQual=Samread_list[10]

			if 'S' in cigar: #needs to remove softclipping for this
				if cigar.count('S') == 1:		
					#getS=re.search('[0-9]*S',cigar).group(0).replace("S",'')
					if cigar.index('S') < cigar.index('M'): #trimming at the beginning
						getS=cigar[:cigar.index('S')]
						readSeq=readSeq[int(getS):]
						readQual=readQual[int(getS):]
						cigar=cigar[cigar.index('S')+1:]
					else: #trimming at the end
						getS=cigar[cigar.rindex('M')+1:cigar.index('S')]
							#try:
						readSeq=readSeq[:-int(getS)]
						readQual=readQual[:-int(getS)]
						#except ValueError:
						#	print "Origional CIGAR: %s" % Samread_list[5]
						#	print "Retreived index of S in cigar: %s" % Samread_list[5]
						#	print "Retreived index of S in cigar: %s" % cigar.index('S')
						#	print "Retreived index of M in cigar: %s" % cigar.index('M')
						#	print "Bases to be trimmed: %s" % getS
						#	raise()
						#cigar=cigar.replace("%sS"%getS,'')
						cigar=cigar[:cigar.rindex('M')+1]
				elif cigar.count('S') == 2: #means that trimming occurs at both the ends
					#first trim from starting
					getS=cigar[:cigar.index('S')]
					readSeq=readSeq[int(getS):]
					readQual=readQual[int(getS):]
					cigar=cigar[cigar.index('S')+1:]
					#first trim from end
					getS=cigar[cigar.rindex('M')+1:cigar.index('S')]
					readSeq=readSeq[:-int(getS)]
					readQual=readQual[:-int(getS)]
					#cigar=cigar.replace("%sS"%getS,'')
					cigar=cigar[:cigar.rindex('M')+1]						
				
				'''
				print "\n**********"
				print Samread_list[5]
				print cigar
				print Samread_list[9]
				print readSeq
				print Samread_list[10]
				print readQual
				sys.exit()
				'''
			
			#now, let's get the average read quality and read length (soft clipping already removed if any)
			#readLength=Samread_list[-1] #
			
			#average_readQual=getQstatsUMI(readQual,Samread_list[-1]) #not needed for new EC

			#lets reformat
			reformated_read="%s\t%s\t%s\t%s\t%s\t%s\t%s"%(
			MDtagValue, #extra six columns, UMI is '-'
			"\t".join(Samread_list[0:5]), #string before cigar
			cigar, #cigar
			"\t".join(Samread_list[6:9]), #string beetween cigar and read sequence
			readSeq, #read sequence
			readQual, #read quality
			"\t".join(Samread_list[11:-1]))	#everything else, -1 because last is length that is int
			return reformated_read#return the formated read and fastqid of the read				
		
		def reformatGSPRead(Samread):
			'''
			Description: Convert the samRead into EC input type read
			EC read format: chr:pos	NMtagValue	UMI	readLength	MDtagValue	average_readQual	<wholeRead>	
			'''
			
			Samread_list=Samread.split("\t")
			"""
			#tags="\t".join(Samread_list[11:])
			try:
				NMtagValue=int(Samread_list[11].replace('NM:i:',''))
			except ValueError:#if some other tag is there, it will not be replaced, and can not be converted into int
			#find by looping
				for i in Samread_list[12:]:
					try:
						NMtagValue=int(i.replace('NM:i:',''))
						break
					except ValueError:
						continue
			"""		
			if 'MD:' in Samread_list[12]:#As MD tagg are stringg, above stategy will not work for MD tags
				MDtagValue=Samread_list[12].replace("MD:Z:",'')
			else:#if some other tag is there, it will not be replaced, and can not be converted into int
				#print "Prob in MD tag"
				#print Samread_list[12].replace("MD:Z:",'')
				#print Samread_list				
				#sys.exit()
				return None
			#find by looping
				for i in Samread_list[11:]:
					if 'MD:' in i:
						MDtagValue=i.replace("MD:Z:",'')
						break
					else:
						continue			
			#NMtagValue=re.search("(?P<NMtag>\tNM:i:)(?P<NMscore>[0-9]*)",Samread).group('NMscore')
			#MDtagValue=re.search("(?P<MDtag>\tMD:Z:)(?P<MDvalue>[0-9A-Z]*)",Samread).group('MDvalue')
			readSeq=Samread_list[9]
			readQual=Samread_list[10]
			cigar=Samread_list[5]
				
			if 'S' in cigar: #needs to remove softclipping for this
				if cigar.count('S') == 1:		
					#getS=re.search('[0-9]*S',cigar).group(0).replace("S",'')
					if cigar.index('S') < cigar.index('M'): #trimming at the beginning
						getS=cigar[:cigar.index('S')]
						readSeq=readSeq[int(getS):]
						readQual=readQual[int(getS):]
						cigar=cigar[cigar.index('S')+1:]
					else: #trimming at the end
						getS=cigar[cigar.rindex('M')+1:cigar.index('S')]
							#try:
						readSeq=readSeq[:-int(getS)]
						readQual=readQual[:-int(getS)]
						#except ValueError:
						#	print "Origional CIGAR: %s" % Samread_list[5]
						#	print "Retreived index of S in cigar: %s" % Samread_list[5]
						#	print "Retreived index of S in cigar: %s" % cigar.index('S')
						#	print "Retreived index of M in cigar: %s" % cigar.index('M')
						#	print "Bases to be trimmed: %s" % getS
						#	raise()
						#cigar=cigar.replace("%sS"%getS,'')
						cigar=cigar[:cigar.rindex('M')+1]
				elif cigar.count('S') == 2: #means that trimming occurs at both the ends
					#first trim from starting
					getS=cigar[:cigar.index('S')]
					readSeq=readSeq[int(getS):]
					readQual=readQual[int(getS):]
					cigar=cigar[cigar.index('S')+1:]
					#first trim from end
					getS=cigar[cigar.rindex('M')+1:cigar.index('S')]
					readSeq=readSeq[:-int(getS)]
					readQual=readQual[:-int(getS)]
					#cigar=cigar.replace("%sS"%getS,'')
					cigar=cigar[:cigar.rindex('M')+1]						
				
				'''
				print "\n**********"
				print Samread_list[5]
				print cigar
				print Samread_list[9]
				print readSeq
				print Samread_list[10]
				print readQual
				sys.exit()
				'''
			#now, let's get the average read quality and read length (soft clipping already removed if any)
			#average_readQual,readLength=getQstats(readQual) #Not needed for new error correction
			
			#lets reformat
			reformated_read="%s\t%s\t%s\t%s\t%s\t%s\t%s"%(
			MDtagValue, #extra six columns, UMI is '-'
			"\t".join(Samread_list[0:5]), #string before cigar
			cigar, #cigar
			"\t".join(Samread_list[6:9]), #string beetween cigar and read sequence
			readSeq, #read sequence
			readQual, #read quality
			"\t".join(Samread_list[11:]))	#everything else
			return reformated_read#return the formated read and fastqid of the read				

		def umiReadSelect(UMI_cluster):
			'''
			#From a cluster, arrange the reads from highest AS score to the lowest AS score
			'''
			MaxAStagValue=0
			clusterIndex_sortedByAS=[]
			for rIndex,i in enumerate(UMI_cluster):
				#get the AS tag value
				#AS tag is usually 14th field and an int value
				AStagValue=0
				try:
					AStagValue=int(i[14].replace('AS:i:',''))
					'''
					#DebugCode
					try:
						AStagValue=int(i[14].replace('AS:i:',''))
					except (TypeError, IndexError) as e:
						print "\n\n==================="
						print i
						print i[14]	
						print 
						sys.exit()
					'''
				except ValueError:#if some other tag is there, it will not be replaced, and can not be converted into int
					
					'''
					#find by looping
					for i in Samread_list[11:]:
						try:
							AStagValue=int(i.replace('AS:i:',''))
							break
						except ValueError:
							continue
					'''
					#To save execution time, if AS score is not present at the intended field, set it's AS score to 1
					AStagValue=1
				clusterIndex_sortedByAS.append([AStagValue,rIndex])
				'''
				if AStagValue > MaxAStagValue:
					MaxAStagValue=AStagValue
					selectedRead=i
					readIndex=rIndex
				'''
			clusterIndex_sortedByAS.sort(reverse=True)
			return clusterIndex_sortedByAS
		
		'''
		####Process R2 first and make a dictionary by the fastq id
		dicMaking_startTime = tm.time()
		print "\n\n>>>>Processing Starts Now:\nCollecting R2-Reads from '%s'" % inputR2

		inputR2_dict = {}
		with open(inputR2,'r') as R2:
			if pb:
				tq0 = tqdm(total=os.path.getsize(inputR2), unit='B', unit_scale=True)
			for eachLine in R2:
				if pb:
					tq0.update(len(eachLine))
				inputR2_dict[eachLine.split("\t")[fastqID_column]] = eachLine[:-1] #get each columns data as an element of a list and save by fastQ ID as keys
		tq0.close()
		print "R2 reads processed in %s seconds.\n\n>>>>Making the clusters now" % (tm.time()-dicMaking_startTime)
		'''

		####Process R1
		makeCluster_startTime = tm.time()
		cc_self.clusters = [] #store all the clusters
		cc_self.excludedClusters_allData=[] #store the excluded cluster data
		
		'''
		#READ THE umi SAM FILE
		UMI_FH=open(inputUMI,'r')
		
		####move to the start pointer location; so that we start reading from there
		UMI_FH.seek(startPointer)
		
		#R1_firstread_readPos_list = ["%s" %UMI_FH.tell()] #use 'startPointer' directly
		eachLine=UMI_FH.readline()
		'''
		
		#R1_readPos_list = ["%s" %UMI_FH.tell()]
		R1_readPos=0 #fake location
		lineNum=1
		
		#if no error correction is to be done, deduplication has to be done during clustering
		if not doErrorCorrection:
			deduppedReads=[]
		
		if pb:
			tq = tqdm(total=len(UMIreads), unit='B', unit_scale=True)
		for eachLine in UMIreads:
			#eachLine[0] --> UMI
			#eachLine[1] --> read start position
			#eachLine[2] --> read
			if len(eachLine[2]) < 5:
				#print "."
				#print eachLine
				#print len(eachLine)
				#sys.exit()
				continue
			if pb:
				tq.update(1)				
			#getCol = eachLine.split("\t") #get each columns data as an element of a list; not needed anymore
			'''
			#Following is for before version 3
			pos=getCol[0] #get the position
			umi = getCol[2] #get the umi
			'''
			#this version' updated method for position and umi extraction
			#umi = getCol[0].split(':')[-1].split('#')[0] #get the umi #not working if there is a : in UMI quality
			#umi = getCol[0].split('#')[0].split(':')[-1]
			umi=eachLine[0]
			
			#print "\n*****"
			#print umi
			#print getCol[0].split(':')[-1]
			#print getCol[0]
			#print getCol
			#sys.exit()
			
			#pos="%s:%s" %(getCol[2],getCol[3]) #get the position #first element of eachLine is position
			pos=eachLine[1]

			if lineNum == 1:
				lineNum+=1 #just do it once
				perPos_readCount += 1
				oldPos=pos #because this is the first line, so previous position is same as current
				
				samePos_UMI_dict = {} #to store all the UMI type and count in this position
				samePos_UMIData_dict = {} #to save all the data related to this position, separated by each UMI
				
				#get the fastq read from R2, proceed only if this read contains the read in R2 also
				try:			
					#Update the count
					samePos_UMI_dict[umi] = 1 #Update the dictionary with the first UMI
					
					#structure of the dictionary:
					'''
					UMI_key = [[UMI_reads]]
					'''
					
					'''
					#Depricated
					#samePos_UMIData_dict[umi]= [R1_firstread_readPos_list] #Update the dictionary with the first UMI Data inside a list, without newline character
					samePos_UMIData_dict[umi]= [str(startPointer)] #Update the dictionary with the first UMI Data inside a list, without newline character
					'''
					samePos_UMIData_dict[umi]=[eachLine[2]]
				except KeyError:				
					pass #	skip the read as this does not has any respective read in the R2
				'''
				#Depricated
				R1_readPos = "%s" %UMI_FH.tell()
				eachLine=UMI_FH.readline()
				'''
				continue #let's jump to the next cycle of the loop
				
			if (not pos == oldPos):
							####let's process the old position data first
				try:
					clus,excl=makeCluster(samePos_UMI_dict,samePos_UMIData_dict,umi_mismatch,readBasedFilter,excludeCluster,oldPos,umiStats_FH)			
					selectedRead=''
					longestReadlength=0
					cc_self.clusters.extend(clus)
					cc_self.excludedClusters_allData.extend(excl)
					#cc_self.clusters.append(clus)
					#cc_self.excludedClusters_allData.append(excl)					
					#update the umi distribution dictionary
					
				except NameError: 
					pass
				
				#reset the per position read count
				perPos_readCount = 1 #not 0, because this is first read of new position
				#get the fastq read from R2, proceed only if this read contains the read in R2 also
				samePos_UMI_dict = {} #Renew the UMI dict
				samePos_UMIData_dict = {} #to save all the data related to this position, separated by each UMI
				
				####Start new for the new position
				samePos_UMI_dict[umi] = 1 #Update the dictionary with the first UMI				
				samePos_UMIData_dict[umi]=[eachLine[2]]
				oldPos=pos
			else:
				perPos_readCount += 1
				if umi in samePos_UMI_dict:
					#let's update the existing R1 and R2 list				
					#get the fastq read from R2, proceed only if this read contains the read in R2 also
					try:
						#getR2_read = inputR2_dict.pop(getCol[fastqID_column])
						
						#first update the count
						samePos_UMI_dict[umi] += 1 #update the count
						
						'''
						#Depricated
						#Get the existing data and update the data
						oldR1_list = samePos_UMIData_dict.pop(umi)
						#oldR2_list = oldData[1]
											
						oldR1_list.append(R1_readPos)
						#oldR2_list.append(getR2_read)
						
						#samePos_UMIData_dict[umi]= [oldR1_list,oldR2_list] #Update the dictionary with the first UMI Data inside a list, without newline character
						samePos_UMIData_dict[umi]= oldR1_list #Update the dictionary with the first UMI Data inside a list, without newline character
						'''
						samePos_UMIData_dict[umi].append(eachLine[2])
					except KeyError:				
						pass #	skip the read as this does not has any respective read in the R2
				else:
					#get the fastq read from R2, proceed only if this read contains the read in R2 also
					try:
						#getR2_read = inputR2_dict.pop(getCol[fastqID_column])
						
						#let's first update the count
						samePos_UMI_dict[umi] = 1
						
						'''
						#Depricated
						#samePos_UMIData_dict[umi]= [R1_readPos_list,R2_read_list] #Update the dictionary with the first UMI Data inside a list, without newline character
						samePos_UMIData_dict[umi]= [R1_readPos] #Update the dictionary with the first UMI Data inside a list, without newline character					
						'''
						samePos_UMIData_dict[umi]=[eachLine[2]]
					except KeyError:
						pass #	skip the read as this does not has any respective read in the R2
				
				oldPos == pos #Because this is end of calculation for this line, let's make it old position before we move on to the next position			

			'''
			#Depricated
			#KEEP READING THE FILE UNTILL IT IS FINISHED
			R1_readPos = "%s" %UMI_FH.tell()
			if (UMI_FH.tell() > int(endPointer)): ####stop reading when we reach to the end pointer
				#print "+++++++++++FILE END+++++++++++++++++++"
				#print "Ended at: %s" % ("%s" %UMI_FH.tell())
				#print "End Point is at: %s" % ("%s" %endPointer)
				#sys.exit()				
				break			
			eachLine=UMI_FH.readline()
			'''

		#process the last position data
		try:
			clus,excl=makeCluster(samePos_UMI_dict,samePos_UMIData_dict,umi_mismatch,readBasedFilter,excludeCluster,pos,umiStats_FH)
		#So if error correction is not done, we will always write the dedupped parts; so that it can be used in downstream pipeline as such
			cc_self.clusters.extend(clus)
			cc_self.excludedClusters_allData.extend(excl)		
		except NameError: 
			pass
		if pb:
			tq.close()
		#clear the memory
		#samePos_UMIData_dict.clear()		
		###################################################################
		##write the dedupped data | Return the clusters; depending on the choice of error correction
		###################################################################
		if GSP_samfile:
			GSP_samfile_FH=open(GSP_samfile,'r')
		if not doErrorCorrection:
			if not bestASmethod: #Just select the first read of the cluster as dedupped read
				deduppedReads=[]
				if GSP_samfile:
					lastRead=''
					readSelected=True
					for i in cc_self.clusters:
						if '+' in i[:3]:
							#if there was no read selected from the previous cluster due to missing paired reads, select the last read as unpaired
							if (not readSelected) and writeUnpaired:
								deduppedReads.append("\t".join(lastRead[:-1]))
							readSelected=False
							newCluster=True
							continue
						if newCluster:
							newCluster=False
							lastRead=i
							#get the paired read of this selected read
							try:#Add only if this read is a paired read
								GSP_samfile_FH.seek(GSP_bytePos_dict[i[0]])
								deduppedReads.append("\t".join(i[:-1]))
								deduppedReads.append(GSP_samfile_FH.readline().replace("\n",''))
								readSelected=True
							except KeyError:
								newCluster=True
								readSelected=False
								continue
				else:
					for i in cc_self.clusters:
						if '+' in i[:3]:
							newCluster=True
							continue
						if newCluster:
							newCluster=False
							deduppedReads.append("\t".join(i[:-1]))
			else:#select based on the AS tag that is best alignment score; more the errors a read has, lower the alignment score
				deduppedReads=[]
				if GSP_samfile:
					#to get the paired read of this selected read; open the file
					
					clusterReads=[]
					for read in cc_self.clusters[1:]:#skip the first '+'
						#if '+' in read[:3]:
						if read== '+':
							#process old cluster
							#umiReadSelect --> to get a list of read index, sorted by best ASscore
							readindex_sortedByAS_Score=umiReadSelect(clusterReads) #readindex_sortedByAS_Score; reads will be sorted by AS score [[<ASScore,ReadIndex>]]
							pairFound=False
							for index in readindex_sortedByAS_Score: #index ==> [ASscore,readIndex]
								try:
									GSP_samfile_FH.seek(GSP_bytePos_dict[clusterReads[index[1]][0]]) #clusterReads[index[1]][0] --> fastqID header of read from Cluster at 'index' index
									deduppedReads.append("\t".join(clusterReads[index[1]][:-1]))
									deduppedReads.append(GSP_samfile_FH.readline().replace("\n",''))
									pairFound=True
									break #if paired read for the selected UMI read has been found, break the loop as no need to search more
								except KeyError: #sometime paired read may be missing, keep searching for other reads of cluster
									pass
								if not pairFound and writeUnpaired:
									deduppedReads.append("\t".join(clusterReads[index[1]][:-1]))
								#print "\n\n===============ERROR"
								#print "Could not find paired read data for the following cluster:\n%s" %("\n".join(clusterReads))
								#sys.exit()
							clusterReads=[]#reset the list
						else:
							#colelct the read
							clusterReads.append(read)
					
					#process the last cluster
					readindex_sortedByAS_Score=umiReadSelect(clusterReads) #readindex_sortedByAS_Score; reads will be sorted by AS score [[<ASScore,ReadIndex>]]
					pairFound=False
					for index in readindex_sortedByAS_Score: #index ==> [ASscore,readIndex]
						try:
							GSP_samfile_FH.seek(GSP_bytePos_dict[clusterReads[index[1]][0]]) #clusterReads[index[1]][0] --> fastqID header of read from Cluster at 'index' index
							deduppedReads.append("\t".join(clusterReads[index[1]][:-1]))
							deduppedReads.append(GSP_samfile_FH.readline().replace("\n",''))
							pairFound=True
							break
						except KeyError:
							pass
						if not pairFound and writeUnpaired:
							deduppedReads.append("\t".join(clusterReads[index[1]][:-1]))
	
				else:
					#just do dedup for UMI reads
					clusterReads=[]
					for read in cc_self.clusters[1:]:#skip the first '+'
						if read== '+':
							#process old cluster
							#umiReadSelect --> to get a list of read index, sorted by best ASscore
							readindex_sortedByAS_Score=umiReadSelect(clusterReads) #readindex_sortedByAS_Score; reads will be sorted by AS score [[<ASScore,ReadIndex>]]
							deduppedReads.append("\t".join(clusterReads[readindex_sortedByAS_Score[0][1]][:-1])) #readindex_sortedByAS_Score[0] --> first read of the AS score sorted clusters; the read with best AS score

							clusterReads=[]#reset the list
						else:
							#colelct the read
							clusterReads.append(read)
			
			if deduppedReads:
				writeDeduppedClusters_FH=open('%s/%s.incCLUSTER.sam'%(workingDirectory,output_prefix),'w+')
				writeDeduppedClusters_FH.write("\n".join(deduppedReads))
				writeDeduppedClusters_FH.close()	
		else: #reformat the clusetrs here before returning to the error correction code
			cc_self.reformatedCluster=['+']
			if GSP_samfile:
				previousGSPReads=[]
				for i in cc_self.clusters[1:]: #skip the first '+' line
					if '+' in i[:3]:
						cc_self.reformatedCluster.append('@')
						cc_self.reformatedCluster.extend(previousGSPReads)
						cc_self.reformatedCluster.append('+')
						previousGSPReads=[]
					else:
						#get the paired read of this selected read
						try:
							GSP_samfile_FH.seek(GSP_bytePos_dict[i[0]])
							reforRead=reformatUMIRead(i)
							if reforRead:
								cc_self.reformatedCluster.append(reforRead) #reformat the read and add it to the reformatedCluster
							else:
								continue
							
							refGSPread=reformatGSPRead(GSP_samfile_FH.readline().replace("\n",''))
							if refGSPread:
								previousGSPReads.append(refGSPread) #reformat the read and add it to the reformatedCluster
							else:
								continue
						except KeyError:
							pass
							#this is unpaired read, DO NOT ADD UMI READ TOO, EC IS NOT DESICNED FOR UNPAIRED READS
							'''
							if writeUnpaired:
								cc_self.reformatedCluster.append(reformatUMIRead(i)) #reformat the read and add it to the reformatedCluster
							'''

				
				#let's add the last GSP paired data
				cc_self.reformatedCluster.append('@')
				cc_self.reformatedCluster.extend(previousGSPReads)
				
			else:#single end sequencing
				for i in cc_self.clusters[1:]:
					if '+' in i[:3]:
						cc_self.reformatedCluster.append('+')
					else:
						reforRead=reformatUMIRead(i)
						if reforRead:
							cc_self.reformatedCluster.append(reformatUMIRead(i)) #reformat the read and add it to the reformatedCluster
						else:
							continue							
		
		def writePart(partData_list,o_filename):
			writeClusters_FH=open(o_filename,'w+')
			for i in partData_list:
				if '+' in i[:3]:
					writeClusters_FH.write('+\n')
					#print "found"
				else:
					try:
						#writeClusters_FH.write("\t".join("\t".join([str(x) for x in i])))
						writeClusters_FH.write("\t".join(i[:-1]))
						writeClusters_FH.write("\n")
					except TypeError:
						print i
						raise			
			writeClusters_FH.write("\n")
			writeClusters_FH.close()
		
		#if User wants to see the clusters, clusters will be written in a separate DIR 'CLUSTERS' 
		if writeClusterData:
			if not os.path.isdir('%s/CLUSTERS' % workingDirectory):
				os.system('mkdir %s/CLUSTERS' % workingDirectory)
			output_filename="%s/CLUSTERS/%s.CLUSTER.sam" %(workingDirectory,output_prefix)
			writePart(cc_self.clusters,output_filename)				

		if excludeCluster > 0:
			#print "Writing the excluded cluster data first."
			#print "All the excluded clusters' data will be written in: %s/%s_CLUSTERS/%s_excluded.CLUSTER.sam\n\n"% (workingDirectory,output_prefix,output_prefix)
			#cc_self.excludedClusters_allData
			excludedData_FH=open('%s/%s_excluded.CLUSTER.sam'% (workingDirectory,output_prefix),'w+')
			if GSP_samfile:
				for i in cc_self.excludedClusters_allData:
					if '+' in i[:3]:
						continue
					try:
						#writeClusters_FH.write("\t".join("\t".join([str(x) for x in i])))
						excludedData_FH.write("\t".join(i[:-1])) #skip writing RL tag as this would require converting last the elements in str and then manually add RL tag, it's not worth it.
						excludedData_FH.write("\n")
						#also write the GSP reads
						try:
							GSP_samfile_FH.seek(GSP_bytePos_dict[i[0]])
							excludedData_FH.write(GSP_samfile_FH.readline())
						except KeyError:
							pass						
					except TypeError:
						print i
						raise						
			else:
				for i in cc_self.excludedClusters_allData:
					if '+' in i[:3]:
						continue
					try:
						#writeClusters_FH.write("\t".join("\t".join([str(x) for x in i])))
						excludedData_FH.write("\t".join(i[:-1])) #skip writing RL tag as this would require converting last the elements in str and then manually add RL tag, it's not worth it.
						excludedData_FH.write("\n")
					except TypeError:
						print i
						raise				
			
			#excludedData_FH.write("".join(cc_self.excludedClusters_allData))
			#excludedData_FH.write("\n")
			excludedData_FH.close()
			#clear the memory
			#del cc_self.excludedClusters_allData[:]
		
		if GSP_samfile:
			GSP_samfile_FH.close()
		umiStats_FH.close()
		#free up memory
		#del clusters_in1part[:]
		#gc.collect()

		#print "\n\nAll done. Total Time of execution: %f" % ((tm.time()-startTime))
		#os.kill(os.getpid(), 9)

if __name__=='__main__':
	parser = argparse.ArgumentParser(description="This script is an intermediate script of DeepClean and is intended to make clusters using the position and UMI from the sam file.\n",
									 usage= 'use "python %s --help" for more information' % sys.argv[0],
									 formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument("--version",action='version',version=textwrap.dedent('''\
		Program: %(prog)s
		Version: 3, Modular and file's byte location dictionary (FBLD) based clustering.
		Author: shashank, BI TEAM, EDGC
		contact: s.katiyar@edgc.com

		Copyright (C) : EONE-DIAGNOMICS Genome Center, South Korea  - All Rights Reserved
		* Unauthorized copying/use of this file, via any medium is strictly prohibited
		* Proprietary and confidential
		* August 2018
		
		Update Notes:
			V3.0:
				Clustering Algorithm: Exactly same as of version 2.1, however its implementation has been changed to minimize the memory usage. Such as no longer the paired read will be read and kept in memory
				Clustering of the UMI read will be done first and then only resepective of dedupped UMI-reads will be saved from paired GSP reads. This will save lots of RAM as we no longer need to load full GSP-read file.
				Changes:
					1) Eaarlier versions were accepting the modified sam format, but now this clustering script accepts the normal SAM r1 and R2 but R1 must be sorted by position
					1) File handling changed to reduce the memory usage.
					2) Script changed to class so that can be imported directly into main DeepClean program
					
			V2.1:
				With the next version, The clustering algorithm is being modified. Earlier, in the version-1, we were identifying the nearest UMIs of the highest occurring UMI only. That is like a Network, but till only node level 1.
				However, In this version, the Network will be extended until a node is closed.
				E.G.:
					UMIs at a position : GACT, CACT, GGCT, GACC, CCCT, CGCT, CGCC, GCGT
					Highest Occurring UMI:
					
					Cluster 1:		GACT
									| Allowed Mismatch or Edit Distance:1
							 ---------------
							 |      |      |
							 CACT   GGCT   GACC #Node level 1; Version 1 implementation
							 |      |      |
							 CCCT   CGCT   X	#Node level 2
							 |      |
							 X      CGCC		#Node level 3
									|
									X			#Node level 4; all the node are terminated
					
					Cluster 2: GCGT
							 
			V1:
			V1:
			1) Implementing the Multiprocessing for writing the parts; now program will use -np parameters to decide how many cores to use; just for the writing the output files in this version.
			2) Functional VEC filter (Variable Error Correction): Use different number of allowed mismatch in UMI, if number of reads are low at a position.
			3) Supports simple mismatch count based clustering and is a default option for this version 
			4) Implemented VEC filter (Will work in the next version)
			5) Output file will be split into n number of parts. User can define the value of n, otherwise the default value is 30.
			6) Control the similarity based on the UMI length and number of mismatches in UMI
			7) Support for both 'paired' and 'single' end sequencing
		'''))
	parser.add_argument('--info', help=textwrap.dedent('''\

	------------------------------------------------------------------------REQUIRED ARGUEMENTS------------------------------------------------------------------------
	'''
	))
	parser.add_argument('-i',"--input-umisamfile",type=str,help=textwrap.dedent('''\
	Required: Please provide the location of input UMI sam-file.\n

	------------------------------------------------------------------------OPTIONAL: CORE ARGUEMENTS------------------------------------------------------------------------
	'''))

	parser.add_argument('-um',"--umi-mismatch",type=int,default=1,help=textwrap.dedent('''\
	Default: 1
	Please provide the allowed mismatches in UMI when making the clusters.
	Input type: int
	'''))

	parser.add_argument('-ec',"--exclude-clusters",type=int,default=0,help=textwrap.dedent('''\
	Optional: If user wishes to exclude clusters  from main cluster data that have UPTO 'n' number of reads, please provide the value of 'n'.
	Such clusters will be saved in *_excluded.CLUSTER.sam file.
	Choices:
		If n= 0 --> Do not exclude any cluster
		n=1 --> exclude any cluster that has only 1 read.
		n=2 --> exclude any cluster that has 2 or less number of reads.
		'n' can have any numerical value.
	Default: 0 (Do not exclude any cluster)
	
	------------------------------------------------------------------------OPTIONAL: General Parameters------------------------------------------------------------------------	
	'''))

	parser.add_argument('-od',"--out-dir",type=str,help='Optional= Location of output result files. Default: Current Directory')

	parser.add_argument('-p', "--output-prefix", type=str, help=textwrap.dedent('''\
	Optional: Please provide the output file.
	default: <Input file name>+'.incCLUSTER.sam'
	'''))
	
	parser.add_argument('-pb', "--progressbar", type=str,choices=['0','1'], default='1',help=textwrap.dedent('''\
	Optional: Use progress bar to display progress of execution or not. Disable this option if 'tqdm' library is not installed.
	Or you can install the 'tqdm' library as: 'sudo pip install tqdm' or 'pip install U tqdm'
	default: Enabled.
	Choices:
		0: Disabled
		1: Enabled (Default)
		
	------------------------------------------------------------------------OPTIONAL: Developmental Parameters------------------------------------------------------------------------	
	'''))

	parser.add_argument('-fic','--fastqid-column',type=int,default=0,help=textwrap.dedent('''\
	Optional: Please select the column number in modified input *.sam file that contains the fastqid data.
	Default: 0
	'''))
	
	parser.add_argument('-bw','--bytepos-write',type=str,default='True',choices=['True','False'],help=textwrap.dedent('''\
	Optional: Weather to write or not the final cluter's byte position data to hard disk.
	Choices: True('Write to the hard disk'), False
	'''))	
	i_args = parser.parse_args()
	print "\nStarting the multiprocess supported %s-version1.\n" % sys.argv[0]

	parser.print_help()

	if i_args.input_umisamfile:
		inputSam=os.path.abspath(i_args.input_umisamfile)
		if os.path.isfile(inputSam):
			print "|____Input sam File is: %s" % inputSam
			
			try:
				R1_filename = re.search('[0-9a-zA-Z-_\.]*.sam',inputSam).group(0)
			except AttributeError:
				print "\n\nERROR: Input file is not a .sam file. Please provide a .sam file."
				sys.exit()
			
		else:
			print "\n\nERROR: Invalid location to Input sam File. Please provide the correct PATH to R1 sam file."
			print "Present user provided location:\n\t%s" % inputSam
			sys.exit()	
	else:
		print "\n\nERROR: Please provide the R1 sam file."
		sys.exit()
		
	if i_args.out_dir:
		workingDir = os.path.abspath(i_args.out_dir)
		try:
			os.system("chdir %s" %workingDir)
			print "|____Working in the following user provided DIR:\n%s" %workingDir
		except OSError:
			print "\n\nERROR:User provided output DIR is invalid."
			print "Please provide the correct output dir."
			raise	
	else:
		workingDir = os.path.abspath(os.getcwd())
		print "|____Working in the following default DIR:\n     |____'%s'" %workingDir
	os.chdir(workingDir) #let's move to the workingDIR
		
	if i_args.output_prefix:
		output_prefix = i_args.output_prefix
	else:
		output_prefix = "%s" % R1_filename[:-4]
	print "|____Output Filename is: %s" % output_prefix

	umi_mismatch= i_args.umi_mismatch
	print "|____Allowed Mismatche/s in UMI: %d" % umi_mismatch

	if i_args.progressbar == '1':
		pb = True
		try:
			from tqdm import tqdm
		except ImportError:
			print "\n\nERROR: Could not import module 'tqdm'. Either install the module or use option '-pb' to disable it."
			sys.exit()
	else:
		pb = False

	"""
	####Obsolete Code: used for Variable region error correciton
	if i_args.vec_filter == 'true':
		vec = True
		vecCutOff = i_args.vec_cutoff
		vecAllowedMismatches = i_args.vec_allowedMismatches
		print "|____Variable Error Correction applied: Yes"
		print "|____Variable Error Correction Reads Cut-off: %d" % vecCutOff
		print "|____Variable Error Correction Allowed Mismatches : %d" % vecAllowedMismatches
	else:
		vec = False
		vecCutOff=None
		vecAllowedMismatches=None
		print "|____Variable Error Correction applied: No"
	"""
	####Match/Mismatch count based similarity search

	fastqID_column = i_args.fastqid_column
	print "|____Column number that contains fastqid data in input SAM file: %d" % fastqID_column

	#drop/exclude cluster parameters
	excludeCluster = i_args.exclude_clusters
	
	#write thr bytePosition data or not
	writeClusterData=i_args.bytepos_write
	
	inputSam_FH=open(inputSam,'r')
	inputSam_FH.seek(0, os.SEEK_END)
	endPoint=inputSam_FH.tell()
	inputSam_FH.close()
	
	#run the core clustering
	clustering_inst=CoreClustering(inputSam,0,endPoint,umi_mismatch,excludeCluster,pb,writeClusterData,workingDir,output_prefix)
	print "Cluster byte code length is: %d" % len(clustering_inst.clusters)