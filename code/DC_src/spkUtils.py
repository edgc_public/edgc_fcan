import numpy as np
import subprocess as sp
import os
import sys
import multiprocessing as mp

"""
		Copyright (C) : EONE-DIAGNOMICS Genome Center, South Korea  - All Rights Reserved
		* Unauthorized copying/use of this file, via any medium is strictly prohibited
		* Proprietary and confidential
		* August 2018
"""

class getQstats(object):
	'''
	Description: Returns a score of fastq quality string.
	Input:
		1) First Argument: Quality string
		2) Stat type: Following are possible inputs:
			a) 'total' : only total score is calculated
			b) 'mean' : only mean is calculated
			c) 'median' : only median is calculated
			d) 'sd' : only standard deviation is calculated
			e) 'var' : only variance is calculated
			f) 'qlist' : Return only the quality list
			g) 'all' : all possible stats are calculated (default)
	returned output: For 'all' case, else only the input stat type is returned
		1) totalScore: Total of all base's quality score 
		2) averageScore: Average of all base's quality score 
		3) medianScore: Average of all base's quality score 
		4) SD: Standard deviation of all base's quality score 		
		5) VAR: Variance of all base's quality score 		
		6) scoreList: Standard deviation of all base's quality score
		7) length: print the length of the sequence
	'''
	def __init__(utils,inputQualityString,statType='all'):
		# !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
		qs_dic={'!':0,'"':1,'#':2,'$':3,'%':4,'&':5,"'":6,'(':7,')':8,'*':9,
		'+':10,',':11,'-':12,'.':13,'/':14,'0':15,'1':16,'2':17,'3':18,'4':19,
		'5':20,'6':21,'7':22,'8':23,'9':24,':':25,';':26,'<':27,'=':28,'>':29,
		'?':30,'@':31,'A':32,'B':33,'C':34,'D':35,'E':36,'F':37,'G':38,'H':39,'I':40,
		'J':41,'K':42,'L':43,'M':44,'N':45,'O':46,'P':47,'Q':48,'R':49,'S':50,'T':51,'U':52,'V':53,'W':54,'X':55,'Y':56,'Z':57,'[':58,'\\':59,']':60,
		'^':61,'_':62,'`':63,'a':64,'b':65,'c':66,'d':67,'e':68,'f':69,'g':70,'h':71,'i':72,'j':73,'k':74,'l':75,'m':76,'n':77,'o':78,'p':79,'q':80,
		'r':81,'s':82,'t':83,'u':84,'v':85,'w':86,'x':87,'y':88,'z':89,'{':90,'|':91,'}':92,'~':93
		}
		utils.scoreList=[ qs_dic[qualLetter] for qualLetter in inputQualityString]
		if statType == 'all':
			utils.totalScore=sum(utils.scoreList)
			utils.length=len(utils.scoreList)
			utils.averageScore=float(utils.totalScore)/float(utils.length)
			utils.medianScore=np.median(utils.scoreList)
			utils.SD=np.std(utils.scoreList)
			utils.var=np.var(utils.scoreList)
			#return totalScore,averageScore,medianScore,SD,var,scoreList
		elif statType == 'total':
			utils.totalScore=sum(utils.scoreList)
			#return totalScore
		elif statType == 'mean':
			utils.length=len(utils.scoreList)
			utils.totalScore=sum(utils.scoreList)
			utils.averageScore=float(utils.totalScore)/float(utils.length)
			#return averageScore
		elif statType == 'median':
			utils.medianScore=np.median(utils.scoreList)
			#return medianScore
		elif statType == 'sd':
			utils.SD=np.std(utils.scoreList)
			#return SD
		elif statType == 'var':
			utils.var=np.var(utils.scoreList)
			#return var
		elif statType == 'qlist':
			utils.scoreList
			#return scoreList			
		else:
			raise ValueError('\nERROR: Wrong value for stat type in class qstat')
			raise ValueError('\nERROR: Wrong value for stat type in class qstat')
			
class makeProcess(object):
	'''
	Description: It launches a process for a system command using the subprocess.Popen, and returns the output, error and returncode
	input args:
		1)system command
	output variables:
		1) output
		2) error
		3) returncode
	'''
	#btc: belongs to the class
	def __init__(btc,command):
		try:
			process=sp.Popen(command,stderr=sp.PIPE,stdout=sp.PIPE,shell=True)
		except OSError:
			print "\n\nERROR in launching job."
			raise
		except ValueError:
			print "\n\nInvalid Argument ERROR."
			raise
		btc.output,btc.error=process.communicate()
		btc.rc=process.returncode
		
####mismatch generator
class generateMismatch(object):
	'''
	Description: For a given DNA sequence, generate all possible combinations of DNA sequence of 3 other nucleotide bases at each position
	Input: A sequence
	Class Variables:
	misMatch_list: List of all the possible sequences with one mismatch at every position
	'''
	def __init__(self,inputSeq):

		self.misMatch_list = []
		self.misMatch_list.append(inputSeq)
		inputSeq_list = [x for x in inputSeq]
		replacementDict = {'G':['A','C','T'],'A':['G','C','T'],'C':['A','G','T'],'T':['A','C','G'],}
		for index,base in enumerate(inputSeq_list):
			replacemnetList = replacementDict[base]
			for eachbase in replacemnetList:
				replacedList=[] #make a new list
				replacedList.extend(inputSeq_list) #fill it with the original list
				replacedList[index]=eachbase #mutate with the possible base name
				self.misMatch_list.append(''.join(replacedList)) #make it a sequence again and append it to mismatch list
		#return misMatch_list

####bamtoR1R2
class bam2R1R2(object):
	'''
	Description: For a paired BAM file, extract separate R1 and R2 sam files that are sorted by chromosome positions
	input: 
		1) bamfile location
		2) GSP-read: choices --> 'R1' or 'R2' or 'SE' : Please specify which read is GSP read. SE means Single End
		2) samtools location
		3) output folder location
		4) 'True' or 'False' : Weather or not to write the dictionary to hard disk ; Default is False
	class variable:
		1) GSPreadLoc: Location of the R1 SAM file (For paired end only)
		2) UMIreadLoc: Location of the R2 SAM file ((For paired end R1, For single End one and only SAM file)
		
		3) Extraction relate class variables
		 R1extract_output : output message after extracting R1 from BAM file (For paired end R1, For single End one and only SAM file)
		 R2extract_output : output message after extracting R2 from BAM file (For paired end only)
		 R1extract_error : error message after extracting R1 from BAM file
		 R2extract_error : error message after extracting R2 from BAM file (For paired end only)
		 R1extract_returncode : returncode after extracting R1 from BAM file
		 R2extract_returncode : returncode after extracting R2 from BAM file (For paired end only)
		
		4) GSP_bytePos_dict : dictionary that contains the information about the GSP byte position (For paired end only)
			Key: fastqID, Value: Read in SAM format
	'''
	def __init__(self,bamFile,GSPread,samtools,cores,outdir,writeGSPbytePos=False,properly_and_MappedOnly=True):
		
		#print "DEBUG CP1"
		
		#verify the bam location
		if not os.path.isfile(bamFile):
			print "\nERROR: bam file could not be located at: %s.\nTry to provide a correct and complete path." % bamFile
			sys.exit()
		
		getSamtoolsPath=makeProcess('which %s' %samtools)
		if not os.path.isfile(getSamtoolsPath.output.replace("\n",'')):
			print "\nERROR: samtools could not be located. Try to provide a correct and complete path."
			sys.exit()
			
		if not os.path.isdir(outdir.replace("\n",'')):
			print "\nERROR: output DIR could not be located. Try to provide a correct and complete path."
			sys.exit()
		
		#get file name
		bamname=os.path.basename(bamFile)
		#print "DEBUG CP2"
			
		if (GSPread == 'R1') or (GSPread == 'R2'):
			#print "DEBUG CP3"
			print "Processing the paired-end sequenced BAM file."
			R1samloc="%s/%s" %(outdir.replace("\n",''),bamname.replace(".bam","_R1.sam"))
			R2samloc="%s/%s" %(outdir.replace("\n",''),bamname.replace(".bam","_R2.sam"))
			#print "DEBUG CP4"
			if properly_and_MappedOnly:
				#get the R1 and R2 sam
				#makeR1_command='%s sort -@ %d %s | %s view -@ %d -f 0x40 -f 0x2 -F 0x904 -q 1 > %s' %(samtools,cores,bamFile,samtools,cores,R1samloc) #remove 0 mapping quality (-q 1), exclude unmapped (0x4), secondary (0x100), and supplementary (0x800) alignments
				makeR1_command='%s sort -@ %d %s | %s view -@ %d -f 0x40 -f 0x2 -F 0x904 > %s' %(samtools,cores,bamFile,samtools,cores,R1samloc) #exclude unmapped (0x4), secondary (0x100), and supplementary (0x800) alignments
				#print "DEBUG CP5"
				#makeR2_command='%s sort -@ %d %s | %s view -@ %d -f 0x80 -f 0x2 -F 0x904 -q 1 > %s' %(samtools,cores,bamFile,samtools,cores,R2samloc) #remove 0 mapping quality (-q 1), exclude unmapped (0x4), secondary (0x100), and supplementary (0x800) alignments
				makeR2_command='%s sort -@ %d %s | %s view -@ %d -f 0x80 -f 0x2 -F 0x904 > %s' %(samtools,cores,bamFile,samtools,cores,R2samloc) #exclude unmapped (0x4), secondary (0x100), and supplementary (0x800) alignments
				#print "DEBUG CP6"
			else:	
				makeR1_command='%s sort -@ %d %s | %s view -@ %d -f 0x40 -F 0x100 -F 0x800 > %s' %(samtools,cores,bamFile,samtools,cores,R1samloc) #We are still filtering out secondary alignment as it is not possible to run DC with split reads
				#print "DEBUG CP5_2"
				makeR2_command='%s sort -@ %d %s | %s view -@ %d -f 0x80 -F 0x100 -F 0x800 > %s' %(samtools,cores,bamFile,samtools,cores,R2samloc) #We are still filtering out secondary alignment as it is not possible to run DC with split reads
				#print "DEBUG CP6_2"
			makeR1_ins=makeProcess(makeR1_command)
			#print "DEBUG CP7"
			makeR2_ins=makeProcess(makeR2_command)
			#print "DEBUG CP8"
			
			self.R1extract_output =  makeR1_ins.output
			#print "DEBUG CP9"
			
			self.R2extract_output =  makeR2_ins.output
			#print "DEBUG CP10"
			
			self.R1extract_error =  makeR1_ins.error
			#print "DEBUG: %s" %self.R1extract_error	
			self.R2extract_error =  makeR2_ins.error
			#print "DEBUG: %s" %self.R2extract_error	
			self.R1extract_returncode =  makeR1_ins.rc
			self.R2extract_returncode =  makeR2_ins.rc

			#let's pickel the GSP reads; I like the chilli pickel by the way, but this will also do fine (as we don't have pickel in Korea :( )
			if GSPread == 'R1':
				self.GSPreadLoc= os.path.abspath(R1samloc)
				self.UMIreadLoc= os.path.abspath(R2samloc)
			elif GSPread == 'R2':
				self.GSPreadLoc= os.path.abspath(R2samloc)
				self.UMIreadLoc= os.path.abspath(R1samloc)
			else:
				print "\n\n: Wrong input argumnet type for GSPread in 'bam2R1R2 class, spkUtil' module. It can only be either 'R1' or 'R2'."
				raise TypeError
				sys.exit()		
			
			self.GSP_bytePos_dict={}  #Key: <fastqIDs>, Values: <byte location of the GSP reads>
			GSPreadLoc_FH = open(self.GSPreadLoc,'r')
			bytePos=GSPreadLoc_FH.tell()
			eachLine=GSPreadLoc_FH.readline()
			while(eachLine):
				fastqID=eachLine.split("\t")[0]
				if fastqID in self.GSP_bytePos_dict:
					print "%s fastq ID is already in Dictionary. This should not happed as we are already discarding follwoing from BAM:\n\t1) 0-Mapping Quality reads\n\t2)unmapped Reads\n\t3)Secondary reads\n\t4)Supplementary reads.\nThis means there is some other problem in BAM file/fastq file." % fastqID
					print "Current read: %s" % eachLine
					GSPreadLoc_FH.seek(int(self.GSP_bytePos_dict[fastqID]))
					oldLine=GSPreadLoc_FH.readline()
					print "Already saved line: %s" % oldLine
					sys.exit()
				
				self.GSP_bytePos_dict[fastqID]=bytePos
				bytePos=GSPreadLoc_FH.tell()
				eachLine=GSPreadLoc_FH.readline()
		else:
			print "Processing the single end sequenced BAM file."
			SEsamloc="%s/%s" %(outdir.replace("\n",''),bamname.replace(".bam","_SE.sam"))
			
			#get the R1 and R2 sam
			makeSE_command='%s sort -@ %d %s | %s view -@ %d -F 0x904 -q 1 > %s' %(samtools,cores,bamFile,samtools,cores,SEsamloc) #remove 0 mapping quality (-q 1), exclude unmapped (0x4), secondary (0x100), and supplementary (0x800) alignments
			makeR1_ins=makeProcess(makeSE_command)
			
			self.R1extract_output =  makeR1_ins.output
			
			self.R1extract_error =  makeR1_ins.error

			self.R1extract_returncode =  makeR1_ins.rc

			self.UMIreadLoc= os.path.abspath(SEsamloc)			
		
"""
#Development cancelled
class preClean(object):
	'''
	Description: Covert BAM to SAM --> Trim the read sequence and quality according to CIGAR string; and update the CIGAR string >> and generate the byte map of the softclipped R1 and R2
	input:
		1) BAM file
		2) samtools location
		3) output DIR location
		4) partsNum: process the file by splitting in how many parts? (will run parallelly)
		5) output type: 'list'/'write' : weather to also write bytemap to a file or just store the list in class variable (list is always stored in class variable)
		6) outputFilename (None is Default)
	class Variable:
		1) byteMapR1_list
		2) byteMapR2_list
	output:
		write the R1 and R2 sam (optional)
	'''
	def __init__(self,bamFile,samtools,outdir,oType='list',ifilename='None'):	
		#verify the bam location
		if not os.path.isfile(bamFile):
			print "\nERROR: bam file could not be located. Try to provide a correct and complete path."
			sys.exit()
		
		getSamtoolsPath=makeProcess('which %s' %samtools)
		if not os.path.isfile(getSamtoolsPath.output.replace("\n",'')):
			print "\nERROR: samtools could not be located. Try to provide a correct and complete path."
			sys.exit()
		
		if not os.path.isdir(outdir.replace("\n",'')):
			print "\nERROR: output DIR could not be located. Try to provide a correct and complete path."
			sys.exit()	
		
		extractR1R2_inst=bam2R1R2(bamFile,samtools,outdir) #this will write the R1 and R2 sam from bam
		
		R1samLoc=extractR1R2_inst.R1samloc
		R2samLoc=extractR1R2_inst.R2samloc
		
		if not (os.path.isfile(R1samLoc)) and (os.path.isfile(R2samLoc)):
			print "\n\nERROR: 'bam2R1R2()' class, inside 'softClipRead()' class of 'spkUtils' Could not generate the R1 and R2 from BAM.\n"
			sys.exit()
		
		#let's hardclip the files and write the output
		R1sam_FH=open(R1samLoc,'r')
		R2sam_FH=open(R2samLoc,'r')
		
		def hardclip(list,outputLocation):
			'''
			hardclip the reads in list and write to file
			'''
			processedRead=[]
			for eachRead in list:
				if (eachRead[:1] == '#') or (len(eachRead) < 2):
					continue
					
				eachRead_Data=eachRead.split("\t")
				CIGAR_col=eachRead_Data[5]
				#extract tags
				nmTag_data=re.search([0-9]*,re.search('NM:[a-z]:[0-9]*',eachRead).group(0)).group(0) #NM tag value
				
				#let's hardclip
				
			
		
		#processing the R1
		r1read_list=[]
		r2read_list=[]
		with open(R1sam_FH,'r')
"""		
if __name__=='__main__':
	pass