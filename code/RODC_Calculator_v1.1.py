#################################
import os,glob
import argparse
import textwrap
import pandas as pd
import numpy as np
import pysam as ps
from collections import Counter
from multiprocessing import Pool
from datetime import datetime
#-------------------------------
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
#################################
############################################################# arguments #############################################################
M_parser = argparse.ArgumentParser(description='BAMs Region Of Interest depth calculator considered by reads overlap.\
	RODC accept "ONLY 0-BASE and optionally exclusive-START and inclusive-END" format BED file as default option.\
	[ ex) chr1    2    5 => call chr1:3, chr1:4, chr1:5 position ]',add_help=False,formatter_class=argparse.RawTextHelpFormatter)
S_version = 'RODC v1.1'
#------------------------------------------------------------------------------------------------------------------------------------
M_parser_Required = M_parser.add_argument_group('Required arguments')
M_parser_Required.add_argument('-ipath','--input-path',type=str,required=True,help='* Required: Please provide the PATH of input file or directory.')
M_parser_Required.add_argument('-ibed','--input-bed',type=str,required=True,help='* Required: Please provide the PATH of input BED file.(columns:[chr start end gene])')
M_parser_Required.add_argument('-odir','--out-dir',type=str,required=True,help='* Required: Please provide the PATH of output directory.')
#------------------------------------------------------------------------------------------------------------------------------------
M_parser_Optional = M_parser.add_argument_group('Optional arguments')
M_parser_Optional.add_argument('-itype','--input-type',type=str,default='dir',choices=['dir','bam'],help='Please provide the input path type.(default:dir)')
M_parser_Optional.add_argument('-bf','--bed-format',type=str,default='ei',choices=['ei','ie','ii'],help="You can use 3 kinds of BED file format in RODC.\
	ei: 'Exclusive-start Inclusive-end', ie: 'Inclusive-start Exclusive-end', ii: 'Inclusive-start Inclusive-end'. (default:ei)'")
M_parser_Optional.add_argument('-mp','--multi-process',type=int,default=10,help='Please provide the multiprocessing core.(default:10)')
M_parser_Optional.add_argument('-td','--target-depth',type=str,default='1,5,10,20,30,50,100,200,300,500,1000,2000,3000,5000,10000',
																					help='Please provide the depth of targets.\
																					(default:1,5,10,20,30,50,100,200,300,500,1000,2000,3000,5000,10000)')
M_parser_Optional.add_argument('-cf','--cut-off',type=int,default=3000,help='Please provide the minimum cut-off depth.(default:3000)')
M_parser_Optional.add_argument('-v','--version', action='version', version=S_version)
M_parser_Optional.add_argument('-h','--help',action='help', help='show this help message and exit')
#------------------------------------------------------------------------------------------------------------------------------------
M_args=M_parser.parse_args()
#------------------------------------------------------------------------------------------------------------------------------------
S_inPath = M_args.input_path
S_bed=M_args.input_bed
S_outDir = M_args.out_dir
S_inType=M_args.input_type
S_BedFormat = M_args.bed_format
I_cores = M_args.multi_process
S_TargetDepth = M_args.target_depth
I_CutOff = M_args.cut_off
#------------------------------------------------------------------------------------------------------------------------------------
df_bed = pd.read_csv(S_bed, sep='\t', comment='#', header=None, usecols=[0,1,2,3], names=['chr','start','end','gene'] )
df_bed = df_bed.drop_duplicates().sort_values(by=['chr','start']).reset_index(drop=True)

if S_BedFormat == 'ei' :
	df_bed['start'] = df_bed['start']
	df_bed['end'] = df_bed['end']
elif S_BedFormat == 'ie' :
	df_bed['start'] = df_bed['start']-1
	df_bed['end'] = df_bed['end']-1
elif S_BedFormat == 'ii' :
	df_bed['start'] = df_bed['start']-1
	df_bed['end'] = df_bed['end']
#------------------------------------------------------------------------------------------------------------------------------------
S_outPath = '{}/RODC_result'.format(S_outDir)
S_outPlotPath = '{}/RODC_plot'.format(S_outPath)
os.system('mkdir '+S_outPath)
os.system('mkdir '+S_outPlotPath)
#------------------------------------------------------------------------------------------------------------------------------------
S_logName = '{}/RODC.log'.format(S_outPath)
Wf_log = open(S_logName,'w')
#------------------------------------------------------------------------------------------------------------------------------------
L_target_depths = map(int,S_TargetDepth.split(','))
#------------------------------------------------------------------------------------------------------------------------------------
if S_inType == 'dir' :
	L_inBAMs = sorted(glob.glob('{}/*bam'.format(S_inPath)))
else :
	L_inBAMs = [S_inPath]
I_SampleAmount = len(L_inBAMs)
#####################################################################################################################################
############################################################# function ##############################################################
def ROI_depth_Calculator(L_ChrStartEnd) :
	S_bed_chr = L_ChrStartEnd[0]
	I_bed_start = L_ChrStartEnd[1]
	I_bed_end = L_ChrStartEnd[2]
	#--------------------------------------------------------------------------------------------------------------------------------
	D_bed_ROI_depth = {}
	L_pos_queryIDs = [ read_fetched.query_name for read_fetched in Ps_inBAM.fetch(S_bed_chr,I_bed_start,I_bed_end,multiple_iterators=True) ]
	#--------------------------------------------------------------------------------------------------------------------------------
	D_Counted_queryIDs = Counter(L_pos_queryIDs)
	#--------------------------------------------------------------------------------------------------------------------------------
	I_Overlap_depth = len([ I_count for I_count in D_Counted_queryIDs.values() if I_count == 2 ])
	I_NotOverlap_depth = len([ I_count for I_count in D_Counted_queryIDs.values() if I_count == 1 ])
	I_Total_depth = I_Overlap_depth + I_NotOverlap_depth
	#--------------------------------------------------------------------------------------------------------------------------------
	D_bed_ROI_depth['{}:{}'.format(S_bed_chr,I_bed_end)] = [I_Overlap_depth,I_NotOverlap_depth,I_Total_depth]
	df_bed_ROI_depth = pd.DataFrame(D_bed_ROI_depth).transpose().reset_index().rename(columns={'index':'Locus',0:'OverlapDepth',1:'NotOverlapDepth',2:'TotalDepth'})
	#--------------------------------------------------------------------------------------------------------------------------------
	return df_bed_ROI_depth
#####################################################################################################################################
def ROI_Genes_Plotter(S_inGene) :
	df_bed_selectGene = df_bed[(df_bed['gene']==S_inGene)].reset_index(drop=True)
	df_BAMs_Total_ROI_depth_selectGene = df_BAMs_Total_ROI_depth[(df_BAMs_Total_ROI_depth['gene']==S_inGene)].reset_index(drop=True)
	#--------------------------------------------------------------------------------------------------------------------------------
	I_xlimEnd = df_BAMs_Total_ROI_depth_selectGene.shape[0]-1
	I_selecGene_ROIs = df_bed_selectGene.shape[0]
	L_BAMs_Total_ROI_depth_selectGene_pos = list(df_BAMs_Total_ROI_depth_selectGene['pos'])
	#--------------------------------------------------------------------------------------------------------------------------------
	plt.figure(figsize=(15,7))
	for S_inName in L_inNames :
		I_xIndex = df_BAMs_Total_ROI_depth_selectGene.index
		A_yValue = df_BAMs_Total_ROI_depth_selectGene[S_inName]
		#----------------------------------------------------------------------------------------------------------------------------
		plt.plot(I_xIndex,A_yValue)
	#--------------------------------------------------------------------------------------------------------------------------------
	for I_selectGene_index in df_bed_selectGene.index :
		S_selectGene_chr = df_bed_selectGene['chr'][I_selectGene_index]
		I_selectGene_start = df_bed_selectGene['start'][I_selectGene_index]+1
		I_selectGene_end = df_bed_selectGene['end'][I_selectGene_index]
		#----------------------------------------------------------------------------------------------------------------------------
		plt.axvspan(L_BAMs_Total_ROI_depth_selectGene_pos.index(I_selectGene_start),
					L_BAMs_Total_ROI_depth_selectGene_pos.index(I_selectGene_end),
					alpha=0.2,
					label='{}:{:,}-{:,}\nsize:{:,}'.format(S_selectGene_chr,
															I_selectGene_start,I_selectGene_end,
															I_selectGene_end-I_selectGene_start
															),
					color=np.random.rand(4,)
					)
	#--------------------------------------------------------------------------------------------------------------------------------
	plt.axhline(y=I_CutOff,linewidth=0.5,color='red')
	#--------------------------------------------------------------------------------------------------------------------------------
	plt.legend(prop={'size':7},bbox_to_anchor=(1,1),loc="upper left")
	plt.title(S_inGene, fontsize=15)
	plt.xlabel('bp',fontsize=10)
	plt.ylabel('depth',fontsize=10)
	plt.xlim(0,I_xlimEnd)
	plt.tight_layout()
	#--------------------------------------------------------------------------------------------------------------------------------
	plt.savefig('{}/{}_{}ROIs_Depth.png'.format(S_outPlotPath,S_inGene,I_selecGene_ROIs),bbox_inches='tight')
	plt.close()
#####################################################################################################################################
D_Total_ROI_depth_extract_frame = {'Locus':[],'gene':[]}
for I_index in df_bed.index :
	S_bed_chr = df_bed['chr'][I_index]
	I_bed_start = df_bed['start'][I_index]+1
	I_bed_end = df_bed['end'][I_index]+1
	L_bed_ROI = range(I_bed_start,I_bed_end)
	S_bed_gene = df_bed['gene'][I_index]
	for I_bed_pos in L_bed_ROI :
		D_Total_ROI_depth_extract_frame['Locus'].append('{}:{}'.format(S_bed_chr,I_bed_pos))
		D_Total_ROI_depth_extract_frame['gene'].append(S_bed_gene)
#------------------------------------------------------------------------------------------------------------------------------------
df_Total_ROI_depth_extract_frame = pd.DataFrame(D_Total_ROI_depth_extract_frame)
#------------------------------------------------------------------------------------------------------------------------------------
df_GenesLength = df_Total_ROI_depth_extract_frame['gene'].value_counts().reset_index().rename(columns={'index':'gene','gene':'length'})
I_Total_ROI_length = df_GenesLength['length'].sum()
#------------------------------------------------------------------------------------------------------------------------------------
S_Basic_logs = '=========================== RODC log ===========================\n\
\n\tVersion : {}\
\n\tStart info : {}\
\n\tTotal ROI length : {:,}\
\n\tSample count : {:,}\
\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Used Options\n\
\n\tInput path type : {}\
\n\tMaximum core : {}\
\n\tBed format option : {}\
\n\tTarget depth : {}\
\n\tCut-off depth : {:,}\
'.format(S_version,datetime.now(),I_Total_ROI_length,I_SampleAmount,
		S_inType,I_cores,S_BedFormat,L_target_depths,I_CutOff
		)
print S_Basic_logs
Wf_log.write(S_Basic_logs)
#------------------------------------------------------------------------------------------------------------------------------------
#####################################################################################################################################
L_ChrStartEnds = []
for I_index in df_bed.index :
	S_bed_chr = df_bed['chr'][I_index]
	I_bed_start = df_bed['start'][I_index]
	I_bed_end = df_bed['end'][I_index]
	L_bed_ROI = range(I_bed_start,I_bed_end)
	#--------------------------------------------------------------------------------------------------------------------------------
	for I_bed_pos in L_bed_ROI :
		L_ChrStartEnds.append([S_bed_chr, I_bed_pos, I_bed_pos+1])
	#--------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------
L_inNames = []
for S_inBAM in L_inBAMs :
	S_inName = S_inBAM.split('/')[-1].split('.')[0]
	L_inNames.append(S_inName)
	Ps_inBAM = ps.AlignmentFile(S_inBAM, 'rb')
	#--------------------------------------------------------------------------------------------------------------------------------
	########################################################################################### Multi-Processing
	M_pool_ROI_depth_Calculator = Pool(processes=I_cores)
	L_bed_ROI_depth_dfs = M_pool_ROI_depth_Calculator.map(ROI_depth_Calculator, L_ChrStartEnds)
	###########################################################################################
	df_Total_ROI_depth = pd.concat(L_bed_ROI_depth_dfs, ignore_index=True)
	df_Total_ROI_depth.to_csv('{}/{}_Total_ROI_depth.tsv'.format(S_outPath,S_inName),sep='\t',index=None)
	#--------------------------------------------------------------------------------------------------------------------------------
	S_Multi_time_log = "\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {}'s Multiprocessing is done.".format(S_inName)
	print S_Multi_time_log
	Wf_log.write(S_Multi_time_log)
	#--------------------------------------------------------------------------------------------------------------------------------
	I_ROI_Total_length = df_Total_ROI_depth.shape[0]
	F_ROI_Total_MeanDepth = round(df_Total_ROI_depth['TotalDepth'].mean(),2)
	#--------------------------------------------------------------------------------------------------------------------------------
	D_target_depths = {}
	for I_target_depth in L_target_depths :
		I_targeted_length = df_Total_ROI_depth[(df_Total_ROI_depth['TotalDepth']>=I_target_depth)].shape[0]
		#----------------------------------------------------------------------------------------------------------------------------
		D_target_depths['{}x'.format(I_target_depth)] = [I_ROI_Total_length,
														I_targeted_length,
														(I_targeted_length/float(I_ROI_Total_length))*100
														]
	#--------------------------------------------------------------------------------------------------------------------------------
	df_target_depths = pd.DataFrame(D_target_depths)[['{}x'.format(I_target_depth)
						for I_target_depth in L_target_depths]].round(2).transpose().reset_index().rename(columns={'index':'Target_depth',
																									0:'Total_ROI_length(#)',
																									1:'Covered_ROI_length(#)',
																									2:'Coverage(%)'})
	df_target_depths[['Total_ROI_length(#)','Covered_ROI_length(#)']] = df_target_depths[['Total_ROI_length(#)','Covered_ROI_length(#)']].astype(int)
	#--------------------------------------------------------------------------------------------------------------------------------
	S_tsvName = '{}/{}_Total_ROI_coverage.tsv'.format(S_outPath,S_inName)
	Wf_tsv = open(S_tsvName,'w')
	Wf_tsv.write('## input sample : {}\
		\n## mean depth : {}\n'.format(S_inName,F_ROI_Total_MeanDepth))
	Wf_tsv.close()
	#---------------------------------------------------------------------
	df_target_depths.to_csv(S_tsvName,sep='\t',index=False,mode='a')
	#--------------------------------------------------------------------------------------------------------------------------------
	df_Total_ROI_depth_extract = df_Total_ROI_depth[['Locus','TotalDepth']].rename(columns={'TotalDepth':S_inName})
	df_Total_ROI_depth_extract_frame = pd.merge(df_Total_ROI_depth_extract_frame,df_Total_ROI_depth_extract,how='left',on=['Locus'])
#------------------------------------------------------------------------------------------------------------------------------------
df_Total_ROI_depth_extract_frame.to_csv('{}/BAMs_Total_ROI_depth.tsv'.format(S_outPath),sep='\t',index=None)
#------------------------------------------------------------------------------------------------------------------------------------
A_Total_ROI_chr = df_Total_ROI_depth_extract_frame['Locus'].str.split(':').str[0]
A_Total_ROI_pos = df_Total_ROI_depth_extract_frame['Locus'].str.split(':').str[-1].astype(int)
df_Total_ROI_depth_extract_frame.insert(loc=0,column='pos',value=A_Total_ROI_pos)
df_Total_ROI_depth_extract_frame.insert(loc=0,column='chr',value=A_Total_ROI_chr)
df_BAMs_Total_ROI_depth = df_Total_ROI_depth_extract_frame.drop(['Locus'],axis=1)
#####################################################################################################################################
L_inGenes = sorted(list(set(df_BAMs_Total_ROI_depth['gene'])))
I_inGenesCount = len(L_inGenes)
####################################################################################### Multi-Processing
M_pool_ROI_Genes_Plotter = Pool(processes=I_cores)
M_pool_ROI_Genes_Plotter.map(ROI_Genes_Plotter, L_inGenes)
#######################################################################################
#------------------------------------------------------------------------------------------------------------------------------------
S_Multi_time_log = "\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {}sample's plotting of Gene is done.\n\
\n\tGenes count : {}\
\n\tGenes info : {}".format(I_SampleAmount,I_inGenesCount,L_inGenes)
print S_Multi_time_log
Wf_log.write(S_Multi_time_log)
#------------------------------------------------------------------------------------------------------------------------------------
#######################################################################################
#------------------------------------------------------------------------------------------------------------------------------------
S_end_time_log = '\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  RODC is done.\n\
\n\tEnd info : {}\
\n\n================================================================'.format(datetime.now())
print S_end_time_log
Wf_log.write(S_end_time_log)
#------------------------------------------------------------------------------------------------------------------------------------
#####################################################################################################################################
###################################
Ps_inBAM.close()
Wf_log.close()
M_pool_ROI_Genes_Plotter.close()
M_pool_ROI_depth_Calculator.close()
###################################