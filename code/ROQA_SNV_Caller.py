####################################
import os,sys,argparse,re,operator,textwrap
from collections import Counter
import scipy.stats as stats
import pandas as pd
import numpy as np
import pysam as ps
import time as tm
from multiprocessing import Pool
from contextlib import closing
from datetime import datetime
####################################
#Author: Daekeun Nam, BI TEAM, EDGC
#Contact: daekuen.nam@edgc.com
############################################################# arguments ############################################################
M_parser = argparse.ArgumentParser(description=textwrap.dedent('''\
		SNP and INDEL variants caller. This script is intended to call overlapped variants on Read1 and Read2.
		ROQA accept "ONLY 0-BASE and optionally exclusive-START and inclusive-END" format BED file as default option.
		[ ex) chr1    2    5 => call chr1:3,chr1:4,chr1:5 position ]
		\n'''),add_help=False,formatter_class=argparse.RawTextHelpFormatter)
S_version = 'ROQA_v3.5.3'
#-----------------------------------------------------------------------------------------------------------------------------------
M_parser_Required = M_parser.add_argument_group('Required arguments')
M_parser_Required.add_argument('-ibam','--input-bam',type=str,required=True,help='* Required: Please provide the PATH of input BAM file.')
M_parser_Required.add_argument('-ibed','--input-bed',type=str,required=True,help='* Required: Please provide the PATH of input BED file.')
M_parser_Required.add_argument('-odir','--out-path',type=str,required=True,help='* Required: Please provide the PATH of output directory.')
#-----------------------------------------------------------------------------------------------------------------------------------
M_parser_Optional = M_parser.add_argument_group('Optional arguments')
M_parser_Optional.add_argument('-mp','--multi-process',type=int,default=25,help='Please provide the multiprocessing core.(default:25)')
M_parser_Optional.add_argument('-bf','--bed-format',type=str,
	default='ei',
	choices=['ei','ie','ii'],
	help=textwrap.dedent('''\
	You can use 3 kinds of BED file format in ROQA.
	ei: Exclusive-start Inclusive-end, 
	ie: Inclusive-start Exclusive-end,
	ii: Inclusive-start Inclusive-end. (default:ei)
	\n'''))
M_parser_Optional.add_argument('-qc','--quality-check',type=str,
	default='n',
	choices=['y','n'],
	help=textwrap.dedent('''\
	If you want to check every reads quality and none filtered result, put 'y'.
	default) n
	\n'''))
M_parser_Optional.add_argument('-id','--indel-filter',type=str,
	default='n',
	choices=['y','n'],
	help=textwrap.dedent('''\
	If you want to filter out position which has duplicated indel at the same position, put 'y'.(default:n)
	\n'''))
M_parser_Optional.add_argument('-bq','--base-quality',type=int,default=20,help='Please provide the threshold of base quality.(default:20)')
M_parser_Optional.add_argument('-dp','--min-depth',type=int,default=0,help='Please provide the minimum depth.(default:0)')
M_parser_Optional.add_argument('-ao','--min-ao',type=int,default=0,help='Please provide the minimum Allele Observation.(default:0)')
M_parser_Optional.add_argument('-af','--min-af',type=float,default=0.0001,help='Please provide the minimum Allele Frequency.(default:0.0001)')
M_parser_Optional.add_argument('-ef','--min-ef',type=float,default=0.1,help='Please provide the minimum ErrorFree ratio.(default:0.1)')
M_parser_Optional.add_argument('-fp','--min-fp',type=float,
	default=0.03,
	help=textwrap.dedent('''\
	Please provide the minimum FisherPvalue(FP).
	default) 0.03
	FP is the p-value to determine variant strand bias using Fisher's Exact Test.
	\n'''))
M_parser_Optional.add_argument('-fsb','--max-fsb',type=float,
	default=1,
	help=textwrap.dedent('''\
	Please provide the maximum FragmentStrandBias(FSB).
	default) 1
	SSB is the ratio of simple calculation of strand bias.
	0 FSB is single of primer region variants.
	\n'''))
M_parser_Optional.add_argument('-sbf','--strandbias-filter',type=str,
	default='fsb',
	choices=['fp','fsb','both','none'],
	help=textwrap.dedent('''\
	Please provide the filter option of strand bias.
	
	fp = fisher p-value filter (good for low allele observation)
	fsb = fragment strand bias filter (good for high allele observation)
	both = fp & fsb
	none = no strand bias filter
	
	default) fsb
	\n'''))
M_parser_Optional.add_argument('-gr','--gsp-read',type=str,
	default='R1',
	choices=['R1','R2'],
	help=textwrap.dedent('''\
	Please provide the GSP read.
	default) R1
	\n'''))
M_parser_Optional.add_argument('-v','--version', action='version', version=S_version)
M_parser_Optional.add_argument('-h','--help',action='help', help='show this help message and exit')
#-----------------------------------------------------------------------------------------------------------------------------------
M_args=M_parser.parse_args()
#-----------------------------------------------------------------------------------------------------------------------------------
S_inBAM=M_args.input_bam
S_bed=M_args.input_bed
S_inPath = M_args.out_path
I_cores = M_args.multi_process
S_BedFormat = M_args.bed_format
I_Threshold_BaseQuality = M_args.base_quality
S_QualityCheck = M_args.quality_check
S_IndelFilter = M_args.indel_filter
I_min_depth = M_args.min_depth
I_min_AO = M_args.min_ao
F_min_AF = M_args.min_af
F_min_EF = M_args.min_ef
F_min_FP = M_args.min_fp
F_max_FSB = M_args.max_fsb
S_sbf = M_args.strandbias_filter
S_gr = M_args.gsp_read
#-----------------------------------------------------------------------------------------------------------------------------------
S_outName = S_inBAM.split('/')[-1].split('.')[0]
Ps_BAM = ps.AlignmentFile(S_inBAM,'rb')
df_bed = pd.read_csv(S_bed, sep='\t', comment='#', header=None, usecols=[0,1,2], names=['chr','start','end'] )
S_outPath = '{}/{}_ROQA_result'.format(S_inPath,S_outName)
os.system('mkdir {}'.format(S_outPath))
S_ROQA_Log = '{}/{}_ROQA.log'.format(S_outPath,S_outName)
#-----------------------------------------------------------------------------------------------------------------------------------
if S_QualityCheck == 'y' :
	S_QCoutPath = '{}/ROQA_QC'.format(S_outPath)
	os.system('rmdir {}'.format(S_QCoutPath))
	os.system('mkdir {}'.format(S_QCoutPath))
####################################################################################################################################
############################################################# function #############################################################
def FisherExactTest(L_input) :
	I_RefFor = L_input[0]
	I_RefRev = L_input[1]
	I_AltFor = L_input[2]
	I_AltRev = L_input[3]
	#-------------------------------------------------------------------------------------------------------------------------------
	F_OddsRatio, F_Pvalue = stats.fisher_exact([[I_RefFor,I_RefRev], [I_AltFor,I_AltRev]])
	#-------------------------------------------------------------------------------------------------------------------------------
	return round(F_Pvalue,5)
####################################################################################################################################
'''
def ChiSquareTest(L_input) :
	I_RefFor = L_input[0]
	I_RefRev = L_input[1]
	I_AltFor = L_input[2]
	I_AltRev = L_input[3]
	#-------------------------------------------------------------------------------------------------------------------------------
	try :
		F_Chi, F_Pvalue, F_Dof, A_Ex = stats.chi2_contingency([[I_RefFor,I_RefRev], [I_AltFor,I_AltRev]])
		#---------------------------------------------------------------------------------------------------------------------------
		return round(F_Pvalue,5)
	except :
		F_Pvalue = 0
		#---------------------------------------------------------------------------------------------------------------------------
		return round(F_Pvalue,5)
'''
####################################################################################################################################
def FragmentStrandBias(L_input) :
	I_RefFor = L_input[0]
	I_RefRev = L_input[1]
	I_AltFor = L_input[2]
	I_AltRev = L_input[3]
	#-------------------------------------------------------------------------------------------------------------------------------
	try :
		F_ForAF = I_AltFor/float(I_RefFor+I_AltFor)
		F_RevAF = I_AltRev/float(I_RefRev+I_AltRev)
		F_TotalAF = (I_AltFor+I_AltRev)/float(I_RefFor+I_RefRev+I_AltFor+I_AltRev)
		#---------------------------------------------------------------------------------------------------------------------------
		F_SSBvalue = abs( F_ForAF - F_RevAF ) / F_TotalAF
		#---------------------------------------------------------------------------------------------------------------------------
		return round(F_SSBvalue,5)
	except :
		return round(0,5)
####################################################################################################################################
def PrimerStrandBias(L_input) :
	I_RefFor = L_input[0]
	I_RefRev = L_input[1]
	I_AltFor = L_input[2]
	I_AltRev = L_input[3]
	#-------------------------------------------------------------------------------------------------------------------------------
	try :
		F_ForRatio = (I_RefFor+I_AltFor) / float(I_RefFor+I_RefRev+I_AltFor+I_AltRev)
		F_RevRatio = (I_RefRev+I_AltRev) / float(I_RefFor+I_RefRev+I_AltFor+I_AltRev)
		#---------------------------------------------------------------------------------------------------------------------------
		F_PSB = abs(F_ForRatio-F_RevRatio)
		#---------------------------------------------------------------------------------------------------------------------------
		return round(F_PSB,5)
	except :
		return round(0,5)
####################################################################################################################################
def OverlapVariantsCaller(inputList) :
	S_bed_chr = inputList[0]
	I_bed_start = inputList[1]
	I_Target_Pos = inputList[2]
	L_Variants = []
	#===============================================================================================================================
	for Read_fetched in Ps_BAM.fetch( S_bed_chr, I_bed_start, I_Target_Pos, multiple_iterators=True ) :
		###############################
		if Read_fetched.has_tag('MD') :
		###############################
			#===========================================================================================================================
			if Read_fetched.is_reverse :
				S_ForReCheck = 'REVERSE'
			else :
				S_ForReCheck = 'FORWARD'
			#===========================================================================================================================
			if Read_fetched.is_read1 :
				S_R1R2Check = 'R1'
			else :
				S_R1R2Check = 'R2'
			#===========================================================================================================================
			S_Ref_Seq = Read_fetched.get_reference_sequence().upper()
			L_Ref_Seq = list(S_Ref_Seq)
			L_Ref_Pos = [ I_pos+1 for I_pos in Read_fetched.get_reference_positions() ]
			#---------------------------------------------------------------------------------------------------------------------------
			if I_Target_Pos in L_Ref_Pos :
				#-----------------------------------------------------------------------------------------------------------------------
				S_Query_Name = Read_fetched.query_name
				S_Query_Seq = Read_fetched.query_alignment_sequence.upper()
				L_Query_Seq = list(S_Query_Seq)
				A_Query_Qul = Read_fetched.query_alignment_qualities
				#-----------------------------------------------------------------------------------------------------------------------
				S_cigar = Read_fetched.cigarstring
				T_cigars = Read_fetched.cigartuples
				L_cigars = Read_fetched.get_blocks()
				#-----------------------------------------------------------------------------------------------------------------------
				L_InDel_Check = set( re.findall('\D',S_cigar) )
				D_InDel_Check = Counter( re.findall('\D',S_cigar) )
				#======================================================================================================================= INDEL with Read Case
				if 'I' in L_InDel_Check or 'D' in L_InDel_Check :
					#===================================================================================================================
					I_Target_Ref_Pos = L_Ref_Pos.index(I_Target_Pos)
					#-------------------------------------------------------------------------------------------------------------------
					L_Ref_Pos_4Del = [ I_pos-(L_cigars[0][0]+1) for I_pos in L_Ref_Pos ]
					S_Ref_Seq_Del_Custom = ''.join([ S_Ref_Seq[I_pos] for I_pos in L_Ref_Pos_4Del ])
					#-------------------------------------------------------------------------------------------------------------------
					S_Target_Ref_Seq = S_Ref_Seq_Del_Custom[I_Target_Ref_Pos]
					#===================================================================================================================
					#--------------------------------------------------------------------
					I_InDel_Mis_Check = len(T_cigars)-1
					I_Custom_Query_Pos = 0
					I_InDel_Confirm_4_SNP = 0
					#--------------------------------------------------------------------
					for I_index, T_Cigar in enumerate(T_cigars) :
						I_Case = T_Cigar[0] # 0: Match, 1: Insertion, 2: Deletion, 4: SoftClipping
						I_BaseCount = T_Cigar[-1]
						#----------------------------------------------------------------
						if I_Case == 0 :
							I_Custom_Query_Pos += I_BaseCount-1
							#------------------------------------------------------------
							if I_Custom_Query_Pos == I_Target_Ref_Pos and I_index != I_InDel_Mis_Check :
								#--------------------------------------------------------
								T_Target_Cigar = T_cigars[I_index+1]
								I_Target_Case = T_Target_Cigar[0]
								I_Target_BaseCount = T_Target_Cigar[-1]
								#--------------------------------------------------------
								if I_Target_Case == 1 :
									#----------------------------------------------------
									S_Target_Insertion_Query_Seq = S_Query_Seq[I_Target_Ref_Pos:I_Target_Ref_Pos+I_Target_BaseCount+1]
									I_Target_Query_Qual = np.mean(A_Query_Qul[I_Target_Ref_Pos:I_Target_Ref_Pos+I_Target_BaseCount+1])
									#----------------------------------------------------
									L_Variants.append([S_bed_chr,I_Target_Pos,S_Target_Ref_Seq,S_Query_Name,S_Target_Insertion_Query_Seq,I_Target_Query_Qual,S_R1R2Check,S_ForReCheck,'I'])
									I_InDel_Confirm_4_SNP += 1
								#--------------------------------------------------------
								elif I_Target_Case == 2 :
									#----------------------------------------------------
									S_Target_Deletion_Ref_Seq = S_Ref_Seq[I_Target_Ref_Pos:I_Target_Ref_Pos+I_Target_BaseCount+1]
									S_Target_Deletion_Query_Seq = S_Query_Seq[I_Target_Ref_Pos]
									I_Target_Query_Qual = A_Query_Qul[I_Target_Ref_Pos]
									#----------------------------------------------------
									L_Variants.append([S_bed_chr,I_Target_Pos,S_Target_Deletion_Ref_Seq,S_Query_Name,S_Target_Deletion_Query_Seq,I_Target_Query_Qual,S_R1R2Check,S_ForReCheck,'D'])
									I_InDel_Confirm_4_SNP += 1
					#=================================================================================================================== SNP with INDELs in Read
					if I_InDel_Confirm_4_SNP != 1 :
						I_Target_BaseCount_CutOff = 0
						I_Target_Custom_BaseCount = 0
						for T_Cigar in T_cigars : 
							I_Case = T_Cigar[0] # 0: Match, 1: Insertion, 2: Deletion, 4: SoftClipping
							I_BaseCount = T_Cigar[-1]
							#----------------------------------------------------------------
							if I_Target_BaseCount_CutOff <= I_Target_Ref_Pos :
								#------------------------------------------------------------
								if I_Case == 0 :
									I_Target_BaseCount_CutOff += I_BaseCount
								elif I_Case == 1 :
									I_Target_Custom_BaseCount += I_BaseCount
									I_Target_BaseCount_CutOff += I_BaseCount
								elif I_Case == 2 :
									#I_Target_Custom_BaseCount -= I_BaseCount # Already considered in Ref pos
									I_Target_BaseCount_CutOff -= I_BaseCount
						#--------------------------------------------------------------------
						I_Target_Query_Pos = I_Target_Ref_Pos + I_Target_Custom_BaseCount
						#---------------------------------------------------------------------------------------------------------------
						S_Target_Query_Seq = S_Query_Seq[I_Target_Query_Pos]
						I_Target_Query_Qual = A_Query_Qul[I_Target_Query_Pos]
						#---------------------------------------------------------------------------------------------------------------
						L_Variants.append([S_bed_chr,I_Target_Pos,S_Target_Ref_Seq,S_Query_Name,S_Target_Query_Seq,I_Target_Query_Qual,S_R1R2Check,S_ForReCheck,'SNP'])
						#===============================================================================================================
				#======================================================================================================================= None INDEL with Read Case
				else :
					#-------------------------------------------------------------------------------------------------------------------
					I_Target_Ref_Pos = L_Ref_Pos.index(I_Target_Pos)
					#-------------------------------------------------------------------------------------------------------------------
					S_Target_Ref_Seq = S_Ref_Seq[I_Target_Ref_Pos]
					S_Target_Query_Seq = S_Query_Seq[I_Target_Ref_Pos]
					I_Target_Query_Qual = A_Query_Qul[I_Target_Ref_Pos]
					#-------------------------------------------------------------------------------------------------------------------
					L_Variants.append([S_bed_chr,I_Target_Pos,S_Target_Ref_Seq,S_Query_Name,S_Target_Query_Seq,I_Target_Query_Qual,S_R1R2Check,S_ForReCheck,'SNP'])
	#=============================================================================================================================== collect reads
	df_Called_Variants = pd.DataFrame(L_Variants, columns=['chr','pos','ref','read_id','base','quality','read_type','strand','type'])
	#=============================================================================================================================== Abnormal filter
	df_Called_Variants = df_Called_Variants[(df_Called_Variants['ref'].str.findall('\W+').apply(lambda x : len(list(x)))==0)]
	#df_Called_Variants.to_csv('test.tsv',sep='\t',index=None)
	#=============================================================================================================================== BQ filter
	df_Called_Variants_BQfiltered = df_Called_Variants[(df_Called_Variants['quality']>I_Threshold_BaseQuality)]
	#############################################################
	I_CallVariantsCheck = df_Called_Variants_BQfiltered.shape[0]
	#############################################################
	if I_CallVariantsCheck == 0 :
		pass
	else :
		if S_QualityCheck == 'y' :
			S_QC_Name = '{}:{}-{}'.format(S_bed_chr,I_bed_start,I_Target_Pos)
			df_Called_Variants_BQfiltered.to_csv('{}/{}_ROQA.qc'.format(S_QCoutPath,S_QC_Name),sep='\t', index=None)
		#=============================================================================================================================== Overlap check
		df_OverlapCheck = df_Called_Variants_BQfiltered.groupby(['chr','pos','ref','read_id','base','type'])['quality'].size().reset_index().rename(columns={'quality':'OverlapCheck'})
		#-------------------------------------------------------------------------------------------------------------------------------
		df_Overlaped = df_OverlapCheck[(df_OverlapCheck['OverlapCheck']==2)]
		df_NotOverlaped = df_OverlapCheck[(df_OverlapCheck['OverlapCheck']==1)]
		#-------------------------------------------------------------------------------------------------------------------------------
		df_MismatchCheck = df_NotOverlaped.groupby(['chr','pos','ref','read_id','type'])['base'].size().reset_index().rename(columns={'base':'OverlapCheck'})
		df_MismatchChecked = df_MismatchCheck[(df_MismatchCheck['OverlapCheck']==2)]
		df_MatchChecked = df_MismatchCheck[(df_MismatchCheck['OverlapCheck']==1)]
		if df_NotOverlaped.shape[0] == 0 and df_MatchChecked.shape[0] == 0 : 
			df_NotOverlaped_rmMismatch = pd.DataFrame(columns=['chr','pos','ref','read_id','type','OverlapCheck'])
		else :
			df_NotOverlaped_rmMismatch = pd.merge(df_NotOverlaped,df_MatchChecked,how='inner',on=['chr','pos','ref','read_id','type','OverlapCheck'])
		#-------------------------------------------------------------------------------------------------------------------------------
		if df_Overlaped.shape[0] == 0 and df_NotOverlaped_rmMismatch.shape[0] == 0 :
			pass
		else :
			df_rowInSR = pd.concat([df_Overlaped,df_NotOverlaped_rmMismatch],ignore_index=True)
			#=============================================================================================================================== Strand bias overlap check
			df_StrandBiasCheck = df_Called_Variants_BQfiltered[['chr','pos','ref','read_id','base','read_type','strand','type']]
			df_StrandBiasOverlapCheck = pd.merge(df_rowInSR,df_StrandBiasCheck,how='left',on=['chr','pos','ref','read_id','base','type'])
			#print df_StrandBiasOverlapCheck
			#df_StrandBiasOverlapCheck.to_csv('SBOC.tsv',sep='\t',index=None)
			#=============================================================================================================================== Include Single Read 
			df_InSR = df_rowInSR.groupby(['chr','pos','ref','base','type'])['read_id'].size().reset_index()
			#-------------------------------------------------------------------------------------------------------------------------------
			for df,S_SR_Option in [(df_InSR,'InSR')] :
				#---------------------------------------------------------------------------------------------------------------------------
				L_form_base = []
				for I_index in range(df.shape[0]) :
					S_ref = df['ref'][I_index]
					S_base = df['base'][I_index]
					S_type = df['type'][I_index]
					if S_type == 'D' :
						L_form_base.append( '{}_{}'.format(S_base,S_ref[1:]) )
					else :
						L_form_base.append(S_base)
				#---------------------------------------------------------------------------------------------------------------------------
				df.insert(loc=df.shape[-1],column='from_base',value=L_form_base)
				A_BaseCounts = df['from_base']+':'+df['read_id'].astype(str)
				A_1st_ref = df['ref'].str[0]
				df.insert(loc=df.shape[-1],column='1st_ref',value=A_1st_ref)
				df.insert(loc=df.shape[-1],column='BaseCounts',value=A_BaseCounts)
				#---------------------------------------------------------------------------------------------------------------------------
				if S_IndelFilter == 'y' :
					df_sorted_indel_filtered = df[['chr','pos','type']].drop_duplicates().groupby(['chr','pos'])['type'].size().reset_index()
					I_indel_filter = df_sorted_indel_filtered[(df_sorted_indel_filtered['type']>2)].shape[0]
					df_sorted_indel_filtered = df_sorted_indel_filtered[(df_sorted_indel_filtered['type']<3)][['chr','pos']]
					df = pd.merge(df,df_sorted_indel_filtered,how='inner',on=['chr','pos'])
					#-----------------------------------------------------------------------------------------------------------------------
					S_INDEL_filter_log = '\n\t{} Filtered out position on indel filter : {:,}'.format(S_SR_Option,I_indel_filter)
					#print S_INDEL_filter_log
					#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					with open(S_ROQA_Log,'a') as f :
						f.write(S_INDEL_filter_log)
					#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
				#---------------------------------------------------------------------------------------------------------------------------
				df_Caller_form = df[['chr','pos','ref','1st_ref','base']]
				df_Caller_form_BC = df.groupby(['chr','pos','1st_ref'])['BaseCounts'].apply(' '.join).reset_index()
				df_Caller_form_BC_indel = pd.merge(df_Caller_form,df_Caller_form_BC, how='left',on=['chr','pos','1st_ref'])
				A_depth = df_Caller_form_BC_indel['BaseCounts'].str.findall('\d+').apply( lambda x : sum(map(int,list(x))) )
				df_Caller_form_BC_indel.insert( loc=2,column='depth',value=A_depth )
				df_Caller_form_BC_indel[['pos','depth']]=df_Caller_form_BC_indel[['pos','depth']].astype(int)
				#---------------------------------------------------------------------------------------------------------------------------
				L_Caller_form_BC_alt = []
				for I_index in range(df_Caller_form_BC_indel.shape[0]) :
					S_chr = df_Caller_form_BC_indel['chr'][I_index]
					I_pos = df_Caller_form_BC_indel['pos'][I_index]
					I_depth = df_Caller_form_BC_indel['depth'][I_index]
					S_ref = df_Caller_form_BC_indel['ref'][I_index]
					S_1st_ref = df_Caller_form_BC_indel['1st_ref'][I_index]
					S_base = df_Caller_form_BC_indel['base'][I_index]
					S_BaseCounts = df_Caller_form_BC_indel['BaseCounts'][I_index]
					D_BaseCounts = { S_alt_info.split(':')[0]:int(S_alt_info.split(':')[-1]) for S_alt_info in S_BaseCounts.split(' ') }
					L_BaseCounts_sorted = sorted( D_BaseCounts.items(), key=operator.itemgetter(1), reverse=True )
					#-----------------------------------------------------------------------------------------------------------------------
					for S_base_sorted,I_count_sorted in L_BaseCounts_sorted :
						I_del_finder = len(S_base_sorted.split('_'))
						S_del_seq = S_base_sorted.split('_')[-1]
						if S_ref != S_1st_ref and I_del_finder > 1 and S_ref[1:] == S_del_seq :  # deletion
							F_AF = round(I_count_sorted/float(I_depth),5)
							F_rm1st_count = 0.0
							for S_base_sorted_EF,I_count_sorted_EF in L_BaseCounts_sorted :
								if S_1st_ref != S_base_sorted_EF :
									F_rm1st_count += I_count_sorted_EF
							#---------------------------------------------------------------------------------------------------------------
							F_ErrorFree = round(I_count_sorted/F_rm1st_count,5)
							#=============================================================================================================== Strand bias for Overlap
							I_Overlap_RefFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==2)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_Overlap_RefRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==2)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_Overlap_AltFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==2)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_Overlap_AltRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==2)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#=============================================================================================================== Strand bias for NOT Overlap [gsp]
							I_NotOverlap_RefFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_RefRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_AltFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_AltRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#=============================================================================================================== Strand bias for NOT Overlap [not gsp]
							I_NotOverlap_NotGSP_RefFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']!=S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_NotGSP_RefRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']!=S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_NotGSP_AltFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']!=S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_NotGSP_AltRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']!=S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#===============================================================================================================
							I_RefFor = I_Overlap_RefFor + I_NotOverlap_RefFor + I_NotOverlap_NotGSP_RefFor
							I_RefRev = I_Overlap_RefRev + I_NotOverlap_RefRev + I_NotOverlap_NotGSP_RefRev
							I_AltFor = I_Overlap_AltFor + I_NotOverlap_AltFor + I_NotOverlap_NotGSP_AltFor
							I_AltRev = I_Overlap_AltRev + I_NotOverlap_AltRev + I_NotOverlap_NotGSP_AltRev
							#---------------------------------------------------------------------------------------------------------------
							F_FP = FisherExactTest([I_RefFor,I_RefRev,I_AltFor,I_AltRev])
							F_PSB = PrimerStrandBias([I_RefFor,I_RefRev,I_AltFor,I_AltRev])
							F_FSB = FragmentStrandBias([I_RefFor,I_RefRev,I_AltFor,I_AltRev])
							S_SC = '{}|{}|{}|{}'.format(I_RefFor,I_RefRev,I_AltFor,I_AltRev)
							#---------------------------------------------------------------------------------------------------------------
							L_Caller_form_BC_alt.append([S_chr,I_pos,I_depth,S_ref,S_BaseCounts,S_SC,S_1st_ref,F_AF,I_count_sorted,F_ErrorFree,F_FP,F_FSB,F_PSB])
						elif S_ref == S_1st_ref and S_ref != S_base_sorted and S_base_sorted == S_base :
							F_AF = round(I_count_sorted/float(I_depth),5)
							F_rm1st_count = 0.0
							for S_base_sorted_EF,I_count_sorted_EF in L_BaseCounts_sorted :
								if S_1st_ref != S_base_sorted_EF :
									F_rm1st_count += I_count_sorted_EF
							#---------------------------------------------------------------------------------------------------------------
							F_ErrorFree = round(I_count_sorted/F_rm1st_count,5)
							#=============================================================================================================== Strand bias for Overlap
							I_Overlap_RefFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==2)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_Overlap_RefRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==2)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_Overlap_AltFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_base)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==2)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_Overlap_AltRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_base)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==2)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#=============================================================================================================== Strand bias for NOT Overlap
							I_NotOverlap_RefFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_RefRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_AltFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_base)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_AltRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_base)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']==S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#=============================================================================================================== Strand bias for NOT Overlap [not gsp]
							I_NotOverlap_NotGSP_RefFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']!=S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_NotGSP_RefRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['base']==S_1st_ref)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']!=S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_NotGSP_AltFor = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_base)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']!=S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='REVERSE')
																].shape[0]
							#---------------------------------------------------------------------------------------------------------------
							I_NotOverlap_NotGSP_AltRev = df_StrandBiasOverlapCheck[(df_StrandBiasOverlapCheck['chr']==S_chr)&
																(df_StrandBiasOverlapCheck['pos']==I_pos)&
																(df_StrandBiasOverlapCheck['ref']==S_ref)&
																(df_StrandBiasOverlapCheck['base']==S_base)&
																(df_StrandBiasOverlapCheck['OverlapCheck']==1)&
																(df_StrandBiasOverlapCheck['read_type']!=S_gr)&
																(df_StrandBiasOverlapCheck['strand']=='FORWARD')
																].shape[0]
							#===============================================================================================================
							I_RefFor = I_Overlap_RefFor + I_NotOverlap_RefFor + I_NotOverlap_NotGSP_RefFor
							I_RefRev = I_Overlap_RefRev + I_NotOverlap_RefRev + I_NotOverlap_NotGSP_RefRev
							I_AltFor = I_Overlap_AltFor + I_NotOverlap_AltFor + I_NotOverlap_NotGSP_AltFor
							I_AltRev = I_Overlap_AltRev + I_NotOverlap_AltRev + I_NotOverlap_NotGSP_AltRev
							#---------------------------------------------------------------------------------------------------------------
							F_FP = FisherExactTest([I_RefFor,I_RefRev,I_AltFor,I_AltRev])
							F_PSB = PrimerStrandBias([I_RefFor,I_RefRev,I_AltFor,I_AltRev])
							F_FSB = FragmentStrandBias([I_RefFor,I_RefRev,I_AltFor,I_AltRev])
							S_SC = '{}|{}|{}|{}'.format(I_RefFor,I_RefRev,I_AltFor,I_AltRev)
							#---------------------------------------------------------------------------------------------------------------
							L_Caller_form_BC_alt.append([S_chr,I_pos,I_depth,S_ref,S_BaseCounts,S_SC,S_base,F_AF,I_count_sorted,F_ErrorFree,F_FP,F_FSB,F_PSB])
				#-------------------------------------------------------------------------------------------------------------------
				df_Caller_form_BC_alt = pd.DataFrame(L_Caller_form_BC_alt,columns=['chr','pos','depth','ref','BaseCounts','StrandCounts','alt','AF','AO','ErrorFree','FisherPvalue','FragmentSB','PrimerRatio'])
				df_Caller_form_BC_alt = df_Caller_form_BC_alt.drop_duplicates().reset_index(drop=True)
				#---------------------------------------------------------------------------------------------------------------------------
				if S_QualityCheck == 'y' :
					#-----------------------------------------------------------------------------------------------------------------------
					df_Caller_form_BC_alt.to_csv('{}/{}_{}_ROQA_QC_report.txt'.format(S_outPath,S_outName,S_SR_Option),sep='\t', index=None, mode='a',header=False)
				#---------------------------------------------------------------------------------------------------------------------------
				if S_sbf == 'none' :
					df_Caller_form_BC_alt = df_Caller_form_BC_alt[(df_Caller_form_BC_alt['depth']>=I_min_depth)&
																			(df_Caller_form_BC_alt['AO']>=I_min_AO)&
																			(df_Caller_form_BC_alt['AF']>=F_min_AF)&
																			(df_Caller_form_BC_alt['ErrorFree']>=F_min_EF)
																			]
				elif S_sbf == 'both' :
					df_Caller_form_BC_alt = df_Caller_form_BC_alt[(df_Caller_form_BC_alt['depth']>=I_min_depth)&
																			(df_Caller_form_BC_alt['AO']>=I_min_AO)&
																			(df_Caller_form_BC_alt['AF']>=F_min_AF)&
																			(df_Caller_form_BC_alt['ErrorFree']>=F_min_EF)&
																			(df_Caller_form_BC_alt['FisherPvalue']>=F_min_FP)&
																			(df_Caller_form_BC_alt['FragmentSB']<=F_max_FSB)
																			]
				elif S_sbf == 'fp' :
					df_Caller_form_BC_alt = df_Caller_form_BC_alt[(df_Caller_form_BC_alt['depth']>=I_min_depth)&
																			(df_Caller_form_BC_alt['AO']>=I_min_AO)&
																			(df_Caller_form_BC_alt['AF']>=F_min_AF)&
																			(df_Caller_form_BC_alt['ErrorFree']>=F_min_EF)&
																			(df_Caller_form_BC_alt['FisherPvalue']>=F_min_FP)
																			]
				elif S_sbf == 'fsb' :
					df_Caller_form_BC_alt = df_Caller_form_BC_alt[(df_Caller_form_BC_alt['depth']>=I_min_depth)&
																			(df_Caller_form_BC_alt['AO']>=I_min_AO)&
																			(df_Caller_form_BC_alt['AF']>=F_min_AF)&
																			(df_Caller_form_BC_alt['FragmentSB']<=F_max_FSB)
																			]
				#---------------------------------------------------------------------------------------------------------------------------
				df_Caller_form_BC_alt.to_csv('{}/{}_{}_ROQA_report.txt'.format(S_outPath,S_outName,S_SR_Option),sep='\t', index=None, mode='a',header=False)
				df_Called_vcf = df_Caller_form_BC_alt[['chr','pos','ref','alt']]
				df_Called_vcf.insert(loc=2,column='ID',value=['.']*df_Called_vcf.shape[0])
				df_Called_vcf.insert(loc=df_Called_vcf.shape[1],column='QUAL',value=['.']*df_Called_vcf.shape[0])
				df_Called_vcf.insert(loc=df_Called_vcf.shape[1],column='FILTER',value=['.']*df_Called_vcf.shape[0])
				df_Called_vcf = df_Called_vcf.rename(columns={'chr':'#CHROM','pos':'POS','ref':'REF','alt':'ALT'}) 
				df_Caller_form_BC_alt = df_Caller_form_BC_alt.astype(str)
				df_Caller_form_BC_alt['BaseCounts'] = df_Caller_form_BC_alt['BaseCounts'].str.split(' ').apply(','.join)
				A_info = 'BC='+df_Caller_form_BC_alt['BaseCounts']+';DP='+df_Caller_form_BC_alt['depth']+';AO='+df_Caller_form_BC_alt['AO']+';AF='+df_Caller_form_BC_alt['AF']+';EF='+df_Caller_form_BC_alt['ErrorFree']+';SC='+df_Caller_form_BC_alt['StrandCounts']+';FP='+df_Caller_form_BC_alt['FisherPvalue']+';FSB='+df_Caller_form_BC_alt['FragmentSB']+';PSB='+df_Caller_form_BC_alt['PrimerRatio']
				df_Called_vcf.insert(loc=df_Called_vcf.shape[1],column='INFO',value=A_info)
				#---------------------------------------------------------------------------------------------------------------------------
				df_Called_vcf.to_csv(S_VCF_Name,sep='\t',index=None,mode='a',header=False)
				#---------------------------------------------------------------------------------------------------------------------------
####################################################################################################################################
############################################################# Multiprocess #########################################################
I_start_time = tm.time()
L_multiCall = []
for I_index in range(df_bed.shape[0]) :
	S_bed_chr = df_bed['chr'][I_index]
	#-------------------------------------------------------------------------------------------------------------------------------
	if S_BedFormat == 'ei' :
		I_bed_start = df_bed['start'][I_index]
		I_bed_end = df_bed['end'][I_index]
	#-------------------------------------------------------------------------------------------------------------------------------
	elif S_BedFormat == 'ie' :
		I_bed_start = df_bed['start'][I_index]-1
		I_bed_end = df_bed['end'][I_index]-1
	#-------------------------------------------------------------------------------------------------------------------------------
	elif S_BedFormat == 'ii' :
		I_bed_start = df_bed['start'][I_index]-1
		I_bed_end = df_bed['end'][I_index]
	#-------------------------------------------------------------------------------------------------------------------------------
	for I_pos in range(I_bed_start,I_bed_end) :
		L_multiCall.append([S_bed_chr,I_pos,I_pos+1])
#-----------------------------------------------------------------------------------------------------------------------------------
df_multiCall_uniq = pd.DataFrame(L_multiCall,columns=['chr','start','end']).drop_duplicates().reset_index(drop=True)
L_multiCall_uniq = df_multiCall_uniq.values.tolist()
#-----------------------------------------------------------------------------------------------------------------------------------
I_Position = len(L_multiCall)
I_UniqPosition = len(L_multiCall_uniq)
I_DupPosition = I_Position-I_UniqPosition
#-----------------------------------------------------------------------------------------------------------------------------------
#=================================================================================================================================== Output format
df_Caller_form_BC_alt = pd.DataFrame(columns=['chr','pos','depth','ref','BaseCounts','StrandCounts','alt','AF','AO','ErrorFree','FisherPvalue','FragmentSB','PrimerRatio'])
df_Caller_form_BC_alt.to_csv('{}/{}_{}_ROQA_report.txt'.format(S_outPath,S_outName,'InSR'),sep='\t', index=None)
#-----------------------------------------------------------------------------------------------------------------------------------
if S_QualityCheck == 'y' :
	df_Caller_form_BC_alt = pd.DataFrame(columns=['chr','pos','depth','ref','BaseCounts','StrandCounts','alt','AF','AO','ErrorFree','FisherPvalue','FragmentSB','PrimerRatio'])
	df_Caller_form_BC_alt.to_csv('{}/{}_{}_ROQA_QC_report.txt'.format(S_outPath,S_outName,'InSR'),sep='\t', index=None)
#-----------------------------------------------------------------------------------------------------------------------------------
S_VCF_Name = '{}/{}_{}_ROQA.vcf'.format(S_outPath,S_outName,'InSR')
Wf_VCF_info = open(S_VCF_Name,'w')
Wf_VCF_info.write('##fileformat={}\
\n##version={}\
\n##fileDate={}\
\n##source={}\
\n##INFO=<ID=BC,Number=4,Type=Integer,Description="Base Counts">\
\n##INFO=<ID=DP,Number=1,Type=Integer,Description="Total Depth">\
\n##INFO=<ID=AO,Number=1,Type=Integer,Description="Allele Observation">\
\n##INFO=<ID=AF,Number=1,Type=Float,Description="Allele Frequency">\
\n##INFO=<ID=EF,Number=1,Type=Float,Description="Error Free">\
\n##INFO=<ID=SC,Number=4,Type=Integer,Description="Strand Counts">\
\n##INFO=<ID=SP,Number=1,Type=Float,Description="Strand Pvalue">\
\n##FILTER=<ID=bq,Description="Minimum Base Quality {}">\
\n##FILTER=<ID=dp,Description="Minimum Total Depth {}">\
\n##FILTER=<ID=ao,Description="Minimum Allele Observation {}">\
\n##FILTER=<ID=af,Description="Minimum Allele Frequency {}">\
\n##FILTER=<ID=ef,Description="Minimum Error Free {}">\
\n##FILTER=<ID=sp,Description="Minimum FisherPvalue {}">\
\n##FILTER=<ID=sp,Description="Maximum FragmentStrandBias {}">\
\n##FILTER=<ID=sp,Description="Strand Bias filter {}">\
\n##FILTER=<ID=gr,Description="Gsp read {}">\
\n##FILTER=<ID=bf,Description="Bed file format {}">\
\n##FILTER=<ID=id,Description="Duplicated InDel position {}">\
\n##FILTER=<ID=sr,Description="Single Read {}">\
\n#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO\
	\n'.format('VCFv4.0',S_version,datetime.now(),' '.join(sys.argv),
			I_Threshold_BaseQuality,I_min_depth,I_min_AO,F_min_AF,F_min_EF,F_min_FP,F_max_FSB,S_sbf,S_gr,S_BedFormat,S_IndelFilter,'InSR'))
Wf_VCF_info.close()
#===================================================================================================================================
S_Basic_logs = '=========================== ROQA log ===========================\n\
\n\tVersion : {}\
\n\tStart info : {}\
\n\tTotal ROI position : {:,}\
\n\tDuplicated ROI position : {:,}\
\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  files path\n\
\n\tInput bam : {}\
\n\tInput bed : {}\
\n\tOutput dir : {}\
\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Used Options\n\
\n\tMaximum core : {}\
\n\tMinimum base quality : {}\
\n\tMinimum depth : {}\
\n\tMinimum allele observation : {}\
\n\tMinimum allele frequency : {}\
\n\tMinimum ErrorFree ratio : {}\
\n\tMinimum FisherPvalue : {}\
\n\tMaximum FragmentStrandBias : {}\
\n\tStrand Bias Filter : {}\
\n\tGsp read : {}\
\n\tBed format option : {}\
\n\tIndel filter option : {}\
\n\tQuality check option : {}\
'.format(S_version,datetime.now(),I_Position,I_DupPosition,
		S_inBAM,S_bed,S_inPath,
		I_cores, I_Threshold_BaseQuality,I_min_depth,I_min_AO,F_min_AF,F_min_EF,F_min_FP,F_max_FSB,S_sbf,S_gr,
		S_BedFormat,S_IndelFilter,S_QualityCheck
		)
print S_Basic_logs
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
with open(S_ROQA_Log,'w') as f :
	f.write(S_Basic_logs)
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#===================================================================================================================================
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ Multi process
with closing(Pool(processes=I_cores)) as M_pool_ROQA :
	L_variants_DFs = M_pool_ROQA.map(OverlapVariantsCaller, L_multiCall_uniq)
	##############
	Ps_BAM.close()
	##############
#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#===================================================================================================================================
I_end_time = tm.time()
S_end_time_log = '\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ROQA is done.\n\
\n\tTotal running time : {}s\
\n\tEnd info : {}\
\n\n================================================================'.format(round(I_end_time-I_start_time,2),datetime.now())
print S_end_time_log
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
with open(S_ROQA_Log,'a') as f :
	f.write(S_end_time_log)
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#===================================================================================================================================