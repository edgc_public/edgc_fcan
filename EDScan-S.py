################################
import sys,glob,os
import subprocess
import argparse
import textwrap
import pysam as ps
import pandas as pd
import numpy as np
from datetime import datetime
#-------------------------------
from multiprocessing import Pool
#-------------------------------
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
################################
############################################################# arguments #############################################################
M_parser = argparse.ArgumentParser(description='Liquid Biopsy pipeline multiple runner.',add_help=False,formatter_class=argparse.RawTextHelpFormatter)
S_version = 'Wrapper v1.4.1'
#------------------------------------------------------------------------------------------------------------------------------------
M_parser_Required = M_parser.add_argument_group('Required arguments')
M_parser_Required.add_argument('-idir','--input-path',type=str,required=True,help=textwrap.dedent('''\
	* Required: Please provide the PATH of input FASTQs file directory.
	\n'''))
M_parser_Required.add_argument('-ibed','--input-bed',type=str,
	default='/Work/Cancer/LiquidBiopsy/EDScan-S/data/EDGC_v4/target/Reveal_ctDNA_EDGC_v4_dSA12302-v1.1_target_annotated.bed',
	help=textwrap.dedent('''\
	* Required: Please provide the PATH of input BED file.
		ROI bed file: 4 column <chr> <start> <end> <Gene>
		Default) /Work/Cancer/LiquidBiopsy/EDScan-S/data/EDGC_v4/target/Reveal_ctDNA_EDGC_v4_dSA12302-v1.1_target_annotated.bed [EDGC Custom v4 Panel]
	\n'''))
M_parser_Required.add_argument('-iseq','--input-primer',type=str,
	default='/Work/Cancer/LiquidBiopsy/EDScan-S/data/EDGC_v4/primers/Reveal_ctDNA_EDGC_v4_dSA12302-v1_1_allprimers.txt',
	help=textwrap.dedent('''\
	* Required: Please provide the PATH of input Primer sequence file
		Default) /Work/Cancer/LiquidBiopsy/EDScan-S/data/EDGC_v4/primers/Reveal_ctDNA_EDGC_v4_dSA12302-v1_1_allprimers.txt
	\n'''))
M_parser_Required.add_argument('-odir','--out-path',type=str,required=True,help='* Required: Please provide the PATH of output directory.')
#------------------------------------------------------------------------------------------------------------------------------------
M_parser_Optional = M_parser.add_argument_group('Optional arguments')
M_parser_Optional.add_argument('-panel','--input-panel',type=str,default='archer',
	choices=['archer','edgc','qiagen'],
	help='Please provide the panel of input.(default:archer)')
M_parser_Optional.add_argument('-mp1','--multi-process1',type=int,default=5,
	help='Please provide the multiprocessing core for samples.(default:5)')
M_parser_Optional.add_argument('-mp2','--multi-process2',type=int,default=10,
	help='Please provide the multiprocessing core for scripts.(default:10)')
#------------------------------------------------------------------------------------------------------------------------------------ Prim arguments
M_parser_Optional.add_argument('-mr','--minimum-readlength',type=int,
	default=15,
	help=textwrap.dedent('''\
	Optional but important: Discard reads that are less than -mr parameter length, after all kind of trimming.
		Default: 15
		Note: This read lengtht is the processed read length means after removing 'UMI','SSE','Primer' sequences.
	\n'''))
#------------------------------------------------------------------------------------------------------------------------------------ DeepClean arguments
M_parser_Optional.add_argument('-um','--umi-mismatch',type=int,default=1,
	help='Please provide the number of mismatch allowed in UMI while Clustering.(default:1)')
M_parser_Optional.add_argument('-ec','--exclude-clusters',type=int,default=1,
	help="If user wish to exclude clusters from main cluster data that have UPTO 'n' number of reads.(default:1)")
M_parser_Optional.add_argument('-ecc','--errorcorrection-cutoff',type=float,default=0.8,
	help="Please provide the cutoff value of mismatch ratio.(default:0.8)")
#------------------------------------------------------------------------------------------------------------------------------------ RODC arguments
M_parser_Optional.add_argument('-td','--target-depth',type=int,default=3000,
	help='Please provide the target coverage depth.(default:3000)')
#------------------------------------------------------------------------------------------------------------------------------------ vcf2report arguments
M_parser_Optional.add_argument('-pfdb','--pfdb-location',type=str,
	default='/Work/BIO/refs/KRGDB/KRGDB_622+1100.txt',
	help=textwrap.dedent('''\
	* Optional: Please provide path of a population allele frequency database.
		Default) /Work/BIO/refs/KRGDB/KRGDB_622+1100.txt
	\n'''))
M_parser_Optional.add_argument('-afpe','--auto-filterpanelerrors',type=str,
	default='yes',
	choices=['yes','no'],
	help=textwrap.dedent('''\
	* Optional: exclusive to '-edb': Filter variants by automatically making a panel specific False Positive Database.
		Default) yes
	\n'''))
M_parser_Optional.add_argument('-fpc','--falsepositive-cutoff',type=int,
	default=3,
	help=textwrap.dedent('''\
	* Optional: Required but valid only if '-afpe' is 'yes' or -edb: Define the cut-off count for a variant to consider it in false positive database.
				A variant is false positive if found more than -fpc cutoff samples.
		Default) 3
	\n'''))
M_parser_Optional.add_argument('-gc','--germline-cutoff',type=int,
	default=3,
	help=textwrap.dedent('''\
	* Optional: Required but valid only if '-afpe' is 'yes' or -edb: Define the cut-off count for a variant to consider it in germline database.
				A variant is false positive if found more than -gc cutoff samples.
		Default) 3
	\n'''))
M_parser_Optional.add_argument('-afc','--af-cutoff',type=float,
	default=0.1,
	help=textwrap.dedent('''\
	* Optional: Allele Frequency based Cut-off: Please provide a cut-off of Allele Frequency below which all the variants will be filtered out (dropped).
		Default) 0.1
	\n'''))
M_parser_Optional.add_argument('-acc','--altcount-cutoff',type=int,
	default=3,
	help=textwrap.dedent('''\
	* Optional: Any variant that has euqls to or lower than this alt count, will be dropped.
		Default) 3
	\n'''))
M_parser_Optional.add_argument('-dc','--depth-cutoff',type=int,
	default=0,
	help=textwrap.dedent('''\
	* Optional: Turn on the Depth-Based filter by providing a cutoff value of depth below which all the variants will be filtered out.
		Default) 0
	\n'''))

M_parser_Optional.add_argument('-cpc','--cosmic-prioritycutoff',default=50,type=int,help=('''\
Optional: Use this option to give cosmic count highest priority at the time of filteration.
		  If the cosmic count for a variable is equals or more than this cut-off, it will always be kept irrespective of any other filter (even False Positive database filter will not drop such variant).
		  However, It may be labelled either as Somatic / Germline.
Choices:
		50 (Default) --> Means do not use this filter
		Any other number --> cosmic priority cutoff threshold.
'''))
#------------------------------------------------------------------------------------------------------------------------------------ action arguments
M_parser_Optional.add_argument('-v','--version', action='version', version=S_version)
M_parser_Optional.add_argument('-h','--help',action='help', help='show this help message and exit')
#====================================================================================================================================
M_args=M_parser.parse_args()
#------------------------------------------------------------------------------------------------------------------------------------
S_inFastqPath = M_args.input_path
S_bed = M_args.input_bed
S_PrimerSeq = M_args.input_primer
S_outPath = M_args.out_path
#------------------------------------------------------------------------------------------------------------------------------------
S_Panel = M_args.input_panel
I_cores1 = M_args.multi_process1
I_cores2 = M_args.multi_process2
I_mr = M_args.minimum_readlength
I_um = M_args.umi_mismatch
I_ec = M_args.exclude_clusters
F_ecc =M_args.errorcorrection_cutoff
I_TargetDepth = M_args.target_depth
S_pfdb = M_args.pfdb_location
S_afpe = M_args.auto_filterpanelerrors
I_fpc = M_args.falsepositive_cutoff
I_gc = M_args.germline_cutoff
F_afc = M_args.af_cutoff
I_acc = M_args.altcount_cutoff
I_dc = M_args.depth_cutoff
I_cpc = M_args.cosmic_prioritycutoff
#------------------------------------------------------------------------------------------------------------------------------------
S_SummaryDir = '{}/Summary_result'.format(S_outPath)
S_SummaryTxt = '{}/Summary_result.txt'.format(S_SummaryDir)
S_WrapperLog = '{}/Samples_Wrapper.log'.format(S_outPath)
#-------------------------------------------------------------
S_StatisticDir = '{}/StatisticPlot'.format(S_SummaryDir)
#-------------------------------------------------------------
os.system('mkdir {}'.format(S_SummaryDir))
os.system('mkdir {}'.format(S_StatisticDir))
#-------------------------------------------------------------
with open(S_SummaryTxt,'w') as f :
	f.write('{}\t{}\t{}\n'.format('id','info','value'))
#-------------------------------------------------------------
#==================================================================================================================================== Panel's arguments
if S_Panel == 'archer' :
	I_ssp = 9
	S_ss = 'AACCGCCAGGAGT'
	I_st = 0
	S_GSPread = 'R2'
	S_UMIread = 'R1'
elif S_Panel == 'edgc' :
	I_ssp = 11
	S_ss = 'AACCGCCAGGAGT'
	I_st = 0
	S_GSPread = 'R2'
	S_UMIread = 'R1'
elif S_Panel =='qiagen' :
	I_ssp = 13
	S_ss = 'ATTGGAGTCC'
	I_st = 1
	S_GSPread = 'R1'
	S_UMIread = 'R2'
#==================================================================================================================================== Out Scripts
S_hg19 = '/Work/BIO/refs/hg19/ucsc.hg19.fasta'
#--------------------------------------------------
Os_Fastqc = '/Work/BIO/bin/FastQCv0.11.5/fastqc'
Os_pTrim = '/Work/Util/shashank/pTrim.py'
Os_Bwa = '/Work/BIO/bin/bwa-0.7.16a/bwa'
Os_Samtools = '/Work/BIO/bin/samtools-1.8/samtools'
Os_make200BED = '/Work/Util/shashank/statsGeneration_scripts/make200BED.py'
Os_Qualimap = '/Work/BIO/bin/qualimap_v2.2/qualimap'
Os_DeepClean = '/Work/Util/symlinks/DeepClean.py'
Os_RODC = '/Work/Util/dknam/RODC_ROI_Calculator/RODC_Calculator_v1.1.py'
Os_ROQA = '/Work/100T_Sequencer/Util/symlinks/ROQA_SNV_Caller.py'
Os_ANNO = '/Work/Util/dknam/VariantsAnnotator/VariantsAnnotator_v1.py'
#------------------------------------------------------------------------------------------------------------------------------------
L_FORGE = [S_bed,S_PrimerSeq,S_hg19,Os_Fastqc,Os_pTrim,Os_Bwa,Os_Samtools,Os_make200BED,Os_Qualimap,Os_DeepClean,Os_RODC,Os_ROQA,Os_ANNO]
#####################################################################################################################################
############################################################# functions #############################################################
def MultiRunner(L_R1R2_fastq) :
	S_Fastq_R1 = L_R1R2_fastq[0]
	S_Fastq_R2 = L_R1R2_fastq[1]
	S_R1Name = S_Fastq_R1.split('/')[-1].split('.')[0]
	S_R2Name = S_Fastq_R2.split('/')[-1].split('.')[0]
	S_outName = S_Fastq_R1.split('/')[-1].split('_R1')[0]
	#------------------------------------------------------
	S_SampleLog = '{}/{}_Wrapper.log'.format(S_outPath,S_outName)
	S_SampleDir = '{}/{}'.format(S_outPath,S_outName)
	os.system('mkdir {}'.format(S_SampleDir))
	#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	S_Basic_Samplelog = '=========================== {} log ===========================\n\
	\n\tFastq R1 info : {}\
	\n\tFastq R2 info : {}\
	\n\tStart info : {}\
	'.format(S_R1Name,S_Fastq_R1,S_Fastq_R2,
		datetime.now())
	with open(S_SampleLog,'w') as f :
		f.write(S_Basic_Samplelog)
	#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	#-------------------------------------------------------------------------------------------------------------------------------- FastQC
	try :
		S_FastqcDir = '{}/fastqc'.format(S_SampleDir)
		S_FastqcR1 = '{}/{}_fastqc/fastqc_data.txt'.format(S_FastqcDir,S_R1Name)
		S_FastqcR2 = '{}/{}_fastqc/fastqc_data.txt'.format(S_FastqcDir,S_R2Name)
		#------------------------------------------------------------------------------------
		with open(S_FastqcR1) as f :
			for S_row in f :
				if S_row.startswith('Total Sequences') :
					I_R1_ReadCount = int(S_row.strip().split('\t')[-1])
		#------------------------------------------------------------------------------------
		with open(S_FastqcR2) as f :
			for S_row in f :
				if S_row.startswith('Total Sequences') :
					I_R2_ReadCount = int(S_row.strip().split('\t')[-1])
		#------------------------------------------------------------------------------------
		I_Total_Fq_ReadCount = I_R1_ReadCount+I_R2_ReadCount
		#------------------------------------------------------------------------------------
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  FastQC.\n\
		\n\tFastQC already done.\
		\n\tTotal Fastq ReadCount : {:,}\
		'.format(I_Total_Fq_ReadCount)
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_row_SummaryText = '{}\t{}\t{}\n'.format(S_outName,'Total_Fq_ReadCount',I_Total_Fq_ReadCount)
		with open(S_SummaryTxt,'a') as f :
			f.write(S_row_SummaryText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	except :
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  FastQC.\n\
		\n\tFastQC Start info : {}'.format(datetime.now())
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_FastqcDir = '{}/fastqc'.format(S_SampleDir)
		os.system('mkdir {}'.format(S_FastqcDir))
		#-----------------------------------------------------
		os.system('{} -t {} {} {} -extract -o {}'.format(
			Os_Fastqc,
			I_cores2,
			S_Fastq_R1,S_Fastq_R2,
			S_FastqcDir
			))
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Check point
		try :
			S_FastqcR1 = '{}/{}_fastqc/fastqc_data.txt'.format(S_FastqcDir,S_R1Name)
			S_FastqcR2 = '{}/{}_fastqc/fastqc_data.txt'.format(S_FastqcDir,S_R2Name)
			#------------------------------------------------------------------------------------
			with open(S_FastqcR1) as f :
				for S_row in f :
					if S_row.startswith('Total Sequences') :
						I_R1_ReadCount = int(S_row.strip().split('\t')[-1])
			#------------------------------------------------------------------------------------
			with open(S_FastqcR2) as f :
				for S_row in f :
					if S_row.startswith('Total Sequences') :
						I_R2_ReadCount = int(S_row.strip().split('\t')[-1])
			#------------------------------------------------------------------------------------
			I_Total_Fq_ReadCount = I_R1_ReadCount+I_R2_ReadCount
			#------------------------------------------------------------------------------------
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_row_SummaryText = '{}\t{}\t{}\n'.format(S_outName,'Total_Fq_ReadCount',I_Total_Fq_ReadCount)
			with open(S_SummaryTxt,'a') as f :
				f.write(S_row_SummaryText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\tFastQC End   info : {}\
			\n\n\t|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*| Well Done.\
			\n\tTotal Fastq ReadCount : {:,}\
			'.format(datetime.now(),I_Total_Fq_ReadCount)
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		except :
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\t*************\n\t{}\n\t*************\
			\n'.format('FastQC Error')
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	#-------------------------------------------------------------------------------------------------------------------------------- Primer trimming
	try :
		S_pTrimDir = '{}/pTrim'.format(S_SampleDir)
		S_pTrimLog = glob.glob('{}/*_Ptrim.log'.format(S_pTrimDir))[0]
		with open(S_pTrimLog) as f :
			for S_row in f :
				S_row_strip = S_row.strip()
				if S_row_strip.startswith('*Total reads in trimmed output fastq:') :
					I_Total_pTrimFq_ReadCount = int(S_row_strip.split(' ')[-1])*2
				elif S_row_strip.startswith('GSP Reads dropped due to Error:') :
					I_pTrim_DropRC_PrimerError = int(S_row_strip.split(' ')[-1])*2
				elif S_row_strip.startswith('Dropped reads due to lack of pair') :
					I_pTrim_DropRC_SseError = int(S_row_strip.split(' ')[-1])*2
				elif S_row_strip.startswith('Dropped reads due to ultra poor quality base:') :
					I_pTrim_DropRC_UmiError = int(S_row_strip.split(' ')[-1])*2
				elif S_row_strip.startswith('Reads dropped due to short read length:') :
					I_pTrim_DropRC_ShortLength = int(S_row_strip.split(' ')[-1])*2
		#------------------------------------------------------------------------------------
		I_Total_Error_ReadCount = I_pTrim_DropRC_PrimerError+I_pTrim_DropRC_SseError+I_pTrim_DropRC_UmiError+I_pTrim_DropRC_ShortLength
		F_pTrim_Total_Error_Ratio = round(I_Total_Error_ReadCount/float(I_Total_Fq_ReadCount)*100,2)
		#------------------------------------------------------------------------------------
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  pTrim.\n\
		\n\tpTrim already done.\
		\n\tTotal after pTrim Fastq ReadCount : {:,}\
		\n\tPrimer Error ReadCount : {:,}\
		\n\tSSE Error ReadCount : {:,}\
		\n\tUMI Error ReadCount : {:,}\
		\n\tShort Length ReadCount : {:,}\
		\n\tpTrim read drop ratio : {}%\
		'.format(I_Total_pTrimFq_ReadCount,I_pTrim_DropRC_PrimerError,I_pTrim_DropRC_SseError,I_pTrim_DropRC_UmiError,I_pTrim_DropRC_ShortLength,F_pTrim_Total_Error_Ratio)
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_row_SummaryText = '{}\t{}\t{}\n{}\t{}\t{}\n'.format(S_outName,'Total_pTrimFq_ReadCount',I_Total_pTrimFq_ReadCount,
			S_outName,'pTrimFq_Error_Ratio',F_pTrim_Total_Error_Ratio
			)
		with open(S_SummaryTxt,'a') as f :
			f.write(S_row_SummaryText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	except :
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  pTrim.\n\
		\n\tpTrim Start info : {}'.format(datetime.now())
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_pTrimDir = '{}/pTrim'.format(S_SampleDir)
		os.system('mkdir {}'.format(S_pTrimDir))
		#-----------------------------------------------------
		if S_Panel == 'archer' :
			os.system('python {} -s {} -if {} -if2 {} -pl {}\
				-ssp {} -ss {} -st {}\
				-od {} -uem {} -mr {}'.format(
				Os_pTrim,'paired',S_Fastq_R2,S_Fastq_R1,S_PrimerSeq,
				I_ssp,S_ss,I_st,
				S_pTrimDir,'append',I_mr
				))
		elif S_Panel == 'edgc' :
			os.system('python {} -s {} -if {} -if2 {} -pl {}\
				-ssp {} -ss {} -st {}\
				-od {} -uem {} -mr {}'.format(
				Os_pTrim,'paired',S_Fastq_R2,S_Fastq_R1,S_PrimerSeq,
				I_ssp,S_ss,I_st,
				S_pTrimDir,'append',I_mr
				))		
		elif S_Panel == 'qiagen' :
			os.system('python {} -s {} -if {} -if2 {} -pl {}\
				-ssp {} -ss {} -st {}\
				-od {} -uem {} -mr {}'.format(
				Os_pTrim,'paired',S_Fastq_R1,S_Fastq_R2,S_PrimerSeq,
				I_ssp,S_ss,I_st,
				S_pTrimDir,'append',I_mr
				))
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Check point
		try :
			S_pTrimDir = '{}/pTrim'.format(S_SampleDir)
			S_pTrimLog = glob.glob('{}/*_Ptrim.log'.format(S_pTrimDir))[0]
			with open(S_pTrimLog) as f :
				for S_row in f :
					S_row_strip = S_row.strip()
					if S_row_strip.startswith('*Total reads in trimmed output fastq:') :
						I_Total_pTrimFq_ReadCount = int(S_row_strip.split(' ')[-1])*2
					elif S_row_strip.startswith('GSP Reads dropped due to Error:') :
						I_pTrim_DropRC_PrimerError = int(S_row_strip.split(' ')[-1])*2
					elif S_row_strip.startswith('Dropped reads due to lack of pair') :
						I_pTrim_DropRC_SseError = int(S_row_strip.split(' ')[-1])*2
					elif S_row_strip.startswith('Dropped reads due to ultra poor quality base:') :
						I_pTrim_DropRC_UmiError = int(S_row_strip.split(' ')[-1])*2
					elif S_row_strip.startswith('Reads dropped due to short read length:') :
						I_pTrim_DropRC_ShortLength = int(S_row_strip.split(' ')[-1])*2
			#------------------------------------------------------------------------------------
			I_Total_Error_ReadCount = I_pTrim_DropRC_PrimerError+I_pTrim_DropRC_SseError+I_pTrim_DropRC_UmiError+I_pTrim_DropRC_ShortLength
			F_pTrim_Total_Error_Ratio = round(I_Total_Error_ReadCount/float(I_Total_Fq_ReadCount)*100,2)
			#------------------------------------------------------------------------------------
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_row_SummaryText = '{}\t{}\t{}\n{}\t{}\t{}\n'.format(S_outName,'Total_pTrimFq_ReadCount',I_Total_pTrimFq_ReadCount,
				S_outName,'pTrimFq_Error_Ratio',F_pTrim_Total_Error_Ratio
				)
			with open(S_SummaryTxt,'a') as f :
				f.write(S_row_SummaryText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\tpTrim End   info : {}\
			\n\n\t|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*| Well Done.\
			\n\tTotal after pTrim Fastq ReadCount : {:,}\
			\n\tPrimer Error ReadCount : {:,}\
			\n\tSSE Error ReadCount : {:,}\
			\n\tUMI Error ReadCount : {:,}\
			\n\tShort Length ReadCount : {:,}\
			\n\tpTrim read drop ratio : {}%\
			'.format(datetime.now(),
				I_Total_pTrimFq_ReadCount,I_pTrim_DropRC_PrimerError,I_pTrim_DropRC_SseError,I_pTrim_DropRC_UmiError,I_pTrim_DropRC_ShortLength,F_pTrim_Total_Error_Ratio)
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		except :
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\t*************\n\t{}\n\t*************\
			\n'.format('pTrim Error')
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	#-------------------------------------------------------------------------------------------------------------------------------- BWA Mapping
	try :
		S_sortedBAM = '{}/{}_sorted_filtered_propered.bam'.format(S_SampleDir,S_outName)
		I_Mapped_ReadCount = int(os.popen('{} view -@ {} -q 1 -f 0x02 -c {}'.format(Os_Samtools,I_cores2,S_sortedBAM)).read().strip())
		I_OnTarget_Mapped_ReadCount = int(os.popen('{} view -@ {} -q 1 -f 0x02 -c {} -L {}'.format(Os_Samtools,I_cores2,S_sortedBAM,S_bed)).read().strip())
		#------------------------------------------------------------------------------------
		I_Filtered_ReadCount = I_Total_pTrimFq_ReadCount-I_Mapped_ReadCount
		I_OffTarget_Mapped_ReadCount = I_Mapped_ReadCount-I_OnTarget_Mapped_ReadCount
		I_BWA_DropRC = I_Total_pTrimFq_ReadCount-I_OnTarget_Mapped_ReadCount
		F_OnTarget_Mapped_Ratio_RoiBamMapBam = round(I_OnTarget_Mapped_ReadCount/float(I_Mapped_ReadCount)*100,2)
		F_OnTarget_Mapped_Ratio_RoiBamFq = round(I_OnTarget_Mapped_ReadCount/float(I_Total_Fq_ReadCount)*100,2)
		F_BWA_Mapping_Ratio = round(I_Mapped_ReadCount/float(I_Total_pTrimFq_ReadCount)*100,2)
		F_BWA_DropRC_Ratio = round(I_BWA_DropRC/float(I_Total_pTrimFq_ReadCount)*100,2)
		#------------------------------------------------------------------------------------
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_row_SummaryText = '{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n'.format(
			S_outName,'Mapped_ReadCount',I_Mapped_ReadCount,
			S_outName,'Filtered_ReadCount',I_Filtered_ReadCount,
			S_outName,'OffTarget_Mapped_ReadCount',I_OffTarget_Mapped_ReadCount,
			S_outName,'BWA_Drop_ReadCount',I_BWA_DropRC,
			S_outName,'OnTarget_Mapped_ReadCount',I_OnTarget_Mapped_ReadCount,
			S_outName,'OnTarget_Mapped_Ratio_RoiBamMapBam',F_OnTarget_Mapped_Ratio_RoiBamMapBam,
			S_outName,'OnTarget_Mapped_Ratio',F_OnTarget_Mapped_Ratio_RoiBamFq,
			S_outName,'BWA_Mapping_Ratio',F_BWA_Mapping_Ratio,
			S_outName,'BWA_Drop_Ratio',F_BWA_DropRC_Ratio)
		with open(S_SummaryTxt,'a') as f :
			f.write(S_row_SummaryText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  BWA.\n\
		\n\tBWA already done.\
		\n\tMapped ReadCount : {:,}\
		\n\tFiltered ReadCount : {:,}\
		\n\tOffTarget ReadCount : {:,}\
		\n\tOnTarget mapped ReadCount : {:,}\
		\n\tOnTarget mapped ratio (RoiBam/MapBam) : {}%\
		\n\tOnTarget mapped ratio (RoiBam/Fq) : {}%\
		\n\tBWA mapping ratio : {}%\
		\n\tBWA read drop ratio : {}%\
		'.format(I_Mapped_ReadCount,I_Filtered_ReadCount,I_OffTarget_Mapped_ReadCount,I_OnTarget_Mapped_ReadCount,
			F_OnTarget_Mapped_Ratio_RoiBamMapBam,F_OnTarget_Mapped_Ratio_RoiBamFq,F_BWA_Mapping_Ratio,F_BWA_DropRC_Ratio)
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	except :
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  BWA.\n\
		\n\tBWA Start info : {}'.format(datetime.now())
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		os.system("{} mem -M -R '@RG\\tID:{}\\tSM:{}\\tPL:{}'\
			-t {} {}\
			{}/{}_primerTrimmed.fastq {}/{}_primerTrimmed.fastq |\
			{} sort -@ {} -O bam -o {}/{}_sorted.bam -T {}/{}_sorted".format(
			Os_Bwa,S_outName,S_outName,'ILLUMINA',
			I_cores2,S_hg19,
			S_pTrimDir,S_R1Name,S_pTrimDir,S_R2Name,
			Os_Samtools,I_cores2,S_SampleDir,S_outName,S_SampleDir,S_outName
			))
		os.system('{} index -@ {} {}/{}_sorted.bam'.format(
			Os_Samtools,I_cores2,S_SampleDir,S_outName
			))
		os.system('{} view -b -q 1 -f 0x2 -@ {} {}/{}_sorted.bam \
			-o {}/{}_sorted_filtered_propered.bam'.format(
			Os_Samtools,I_cores2,S_SampleDir,S_outName,S_SampleDir,S_outName
			))
		os.system('{} index -@ {} {}/{}_sorted_filtered_propered.bam'.format(
			Os_Samtools,I_cores2,S_SampleDir,S_outName
			))
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Check point
		try :
			S_sortedBAM = '{}/{}_sorted_filtered_propered.bam'.format(S_SampleDir,S_outName)
			I_Mapped_ReadCount = int(os.popen('{} view -@ {} -q 1 -f 0x02 -c {}'.format(Os_Samtools,I_cores2,S_sortedBAM)).read().strip())
			I_OnTarget_Mapped_ReadCount = int(os.popen('{} view -@ {} -q 1 -f 0x02 -c {} -L {}'.format(Os_Samtools,I_cores2,S_sortedBAM,S_bed)).read().strip())
			#------------------------------------------------------------------------------------
			I_Filtered_ReadCount = I_Total_pTrimFq_ReadCount-I_Mapped_ReadCount
			I_OffTarget_Mapped_ReadCount = I_Mapped_ReadCount-I_OnTarget_Mapped_ReadCount
			I_BWA_DropRC = I_Total_pTrimFq_ReadCount-I_OnTarget_Mapped_ReadCount
			F_OnTarget_Mapped_Ratio_RoiBamMapBam = round(I_OnTarget_Mapped_ReadCount/float(I_Mapped_ReadCount)*100,2)
			F_OnTarget_Mapped_Ratio_RoiBamFq = round(I_OnTarget_Mapped_ReadCount/float(I_Total_Fq_ReadCount)*100,2)
			F_BWA_Mapping_Ratio = round(I_Mapped_ReadCount/float(I_Total_pTrimFq_ReadCount)*100,2)
			F_BWA_DropRC_Ratio = round(I_BWA_DropRC/float(I_Total_pTrimFq_ReadCount)*100,2)
			#------------------------------------------------------------------------------------
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_row_SummaryText = '{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n'.format(
				S_outName,'Mapped_ReadCount',I_Mapped_ReadCount,
				S_outName,'Filtered_ReadCount',I_Filtered_ReadCount,
				S_outName,'OffTarget_Mapped_ReadCount',I_OffTarget_Mapped_ReadCount,
				S_outName,'BWA_Drop_ReadCount',I_BWA_DropRC,
				S_outName,'OnTarget_Mapped_ReadCount',I_OnTarget_Mapped_ReadCount,
				S_outName,'OnTarget_Mapped_Ratio_RoiBamMapBam',F_OnTarget_Mapped_Ratio_RoiBamMapBam,
				S_outName,'OnTarget_Mapped_Ratio',F_OnTarget_Mapped_Ratio_RoiBamFq,
				S_outName,'BWA_Mapping_Ratio',F_BWA_Mapping_Ratio,
				S_outName,'BWA_Drop_Ratio',F_BWA_DropRC_Ratio)
			with open(S_SummaryTxt,'a') as f :
				f.write(S_row_SummaryText)
			#------------------------------------------------------------------------------------
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\tBWA End   info : {}\
			\n\n\t|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*| Well Done.\
			\n\tMapped ReadCount : {:,}\
			\n\tFiltered ReadCount : {:,}\
			\n\tOffTarget ReadCount : {:,}\
			\n\tOnTarget mapped ReadCount : {:,}\
			\n\tOnTarget mapped ratio (RoiBam/MapBam) : {}%\
			\n\tOnTarget mapped ratio (RoiBam/Fq) : {}%\
			\n\tBWA mapping ratio : {}%\
			\n\tBWA read drop ratio : {}%\
			'.format(datetime.now(),
				I_Mapped_ReadCount,I_Filtered_ReadCount,I_OffTarget_Mapped_ReadCount,I_OnTarget_Mapped_ReadCount,
			F_OnTarget_Mapped_Ratio_RoiBamMapBam,F_OnTarget_Mapped_Ratio_RoiBamFq,F_BWA_Mapping_Ratio,F_BWA_DropRC_Ratio)
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		except :
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\t*************\n\t{}\n\t*************\
			\n'.format('BWA Error')
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	#-------------------------------------------------------------------------------------------------------------------------------- ROI Clipping
	try :
		S_sorted_clipped_BAM = '{}/{}_sorted_filtered_propered_clipped.bam'.format(S_SampleDir,S_outName)
		I_Mapped_clipped_ReadCount = int(os.popen('{} view -@ {} -q 1 -c {}'.format(Os_Samtools,I_cores2,S_sorted_clipped_BAM)).read().strip())
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ROI Clipping.\n\
		\n\tClipping already done.\
		\n\tClipped mapped ReadCount : {:,}\
		'.format(I_Mapped_clipped_ReadCount)
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	except :
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ROI Clipping.\n\
		\n\tClipping Start info : {}'.format(datetime.now())
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		os.system('python {} -ib {} -eru {} -erd {} -od {}'.format(
			Os_make200BED,S_bed,150,150,S_SampleDir
			))
		#-----------------------------------------------------
		S_ExtraBED = glob.glob('{}/*bed*_ExtraRegion.bed'.format(S_SampleDir))[0]
		#-----------------------------------------------------
		os.system('{} view -@ {} -L {} -b {}/{}_sorted_filtered_propered.bam -o {}/{}_sorted_filtered_propered_clipped.bam'.format(
			Os_Samtools,I_cores2,S_ExtraBED,S_SampleDir,S_outName,S_SampleDir,S_outName
			))
		os.system('{} index -@ {} {}/{}_sorted_filtered_propered_clipped.bam'.format(
			Os_Samtools,I_cores2,S_SampleDir,S_outName
			))
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Check point
		try :
			S_sorted_clipped_BAM = '{}/{}_sorted_filtered_propered_clipped.bam'.format(S_SampleDir,S_outName)
			I_Mapped_clipped_ReadCount = int(os.popen('{} view -@ {} -q 1 -c {}'.format(Os_Samtools,I_cores2,S_sorted_clipped_BAM)).read().strip())
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\tClipping End   info : {}\
			\n\n\t|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*| Well Done.\
			\n\tClipped mapped ReadCount : {:,}\
			'.format(datetime.now(),I_Mapped_clipped_ReadCount)
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		except :
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\t*************\n\t{}\n\t*************\
			\n'.format('Clipping Error')
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	#-------------------------------------------------------------------------------------------------------------------------------- Qualimap
	try :
		S_GerDir = '{}/GER'.format(S_SampleDir)
		S_GerResult = '{}/genome_results.txt'.format(S_GerDir)
		with open(S_GerResult) as f :
			for S_row in f :
				S_row_strip = S_row.strip()
				if S_row_strip.startswith('general error rate') :
					F_GER = float(S_row_strip.split(' ')[-1])*100
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_row_SummaryText = '{}\t{}\t{}\n'.format(S_outName,'GER',F_GER)
		with open(S_SummaryTxt,'a') as f :
			f.write(S_row_SummaryText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Qualimap.\n\
		\n\tQualimap already done.\
		\n\tGER : {}%\
		'.format(F_GER)
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	except :
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Qualimap.\n\
		\n\tQualimap Start info : {}'.format(datetime.now())
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_GerDir = '{}/GER'.format(S_SampleDir)
		os.system('mkdir {}'.format(S_GerDir))
		#-----------------------------------------------------
		os.system('{} bamqc -bam {}/{}_sorted_filtered_propered_clipped.bam -outdir {}'.format(
			Os_Qualimap,S_SampleDir,S_outName,S_GerDir
			))
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Check point
		try :
			S_GerResult = '{}/genome_results.txt'.format(S_GerDir)
			with open(S_GerResult) as f :
				for S_row in f :
					S_row_strip = S_row.strip()
					if S_row_strip.startswith('general error rate') :
						F_GER = float(S_row_strip.split(' ')[-1])*100
			#------------------------------------------------------------------------------------
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_row_SummaryText = '{}\t{}\t{}\n'.format(S_outName,'GER',F_GER)
			with open(S_SummaryTxt,'a') as f :
				f.write(S_row_SummaryText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\tQualimap End   info : {}\
			\n\n\t|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*| Well Done.\
			\n\tGER : {}%\
			'.format(datetime.now(),F_GER)
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		except :
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\t*************\n\t{}\n\t*************\
			\n'.format('Qualimap Error')
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	#-------------------------------------------------------------------------------------------------------------------------------- DeepClean [Dedup]
	try :
		S_dedup_ExSL_BAM = '{}/{}_sorted_filtered_propered_clipped_DC.bam'.format(S_SampleDir,S_outName)
		S_dedup_InSL_BAM = '{}/{}_sorted_filtered_propered_clipped_DC_InSL.bam'.format(S_SampleDir,S_outName)
		I_dedup_ExSL_OnTarget_ReadCount = int(os.popen('{} view -@ {} -q 1 -f 0x02 -c {} -L {}'.format(Os_Samtools,I_cores2,S_dedup_ExSL_BAM,S_bed)).read().strip())
		I_dedup_InSL_OnTarget_ReadCount = int(os.popen('{} view -@ {} -q 1 -f 0x02 -c {} -L {}'.format(Os_Samtools,I_cores2,S_dedup_InSL_BAM,S_bed)).read().strip())
		#------------------------------------------------------------------------------------
		F_ExSL_DedupRatio = round((1-I_dedup_ExSL_OnTarget_ReadCount/float(I_OnTarget_Mapped_ReadCount))*100,2)
		F_InSL_DedupRatio = round((1-I_dedup_InSL_OnTarget_ReadCount/float(I_OnTarget_Mapped_ReadCount))*100,2)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_row_SummaryText = '{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n'.format(S_outName,'dedup_ExSL_ReadCount',I_dedup_ExSL_OnTarget_ReadCount,
			S_outName,'dedup_InSL_ReadCount',I_dedup_InSL_OnTarget_ReadCount,
			S_outName,'ExSL_DedupRatio',F_ExSL_DedupRatio,
			S_outName,'InSL_DedupRatio',F_InSL_DedupRatio)
		with open(S_SummaryTxt,'a') as f :
			f.write(S_row_SummaryText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  DeepClean.\n\
		\n\tDeepClean already done.\
		\n\tExSL OnTarget ReadCount : {:,}\
		\n\tInSL OnTarget ReadCount : {:,}\
		\n\tExSL Dedup Ratio : {}%\
		\n\tInSL Dedup Ratio : {}%\
		'.format(I_dedup_ExSL_OnTarget_ReadCount,I_dedup_InSL_OnTarget_ReadCount,F_ExSL_DedupRatio,F_InSL_DedupRatio)
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	except :
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  DeepClean.\n\
		\n\tDeepClean Start info : {}'.format(datetime.now())
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		if S_Panel == 'archer' :
			os.system('python {} -i {}/{}_sorted_filtered_propered_clipped.bam\
				-ur {} -um {} -ec {}\
				-ecc {}\
				-np {} -refgen {}\
				-od {}'.format(
				Os_DeepClean,S_SampleDir,S_outName,
				S_UMIread,I_um,I_ec,
				F_ecc,
				I_cores2,S_hg19,
				S_SampleDir
				))
		elif S_Panel == 'edgc' :
			os.system('python {} -i {}/{}_sorted_filtered_propered_clipped.bam\
				-ur {} -um {} -ec {}\
				-ecc {}\
				-np {} -refgen {}\
				-od {}'.format(
				Os_DeepClean,S_SampleDir,S_outName,
				S_UMIread,I_um,I_ec,
				F_ecc,
				I_cores2,S_hg19,
				S_SampleDir
				))
		elif S_Panel == 'qiagen' :
			os.system('python {} -i {}/{}_sorted_filtered_propered_clipped.bam\
				-ur {} -um {} -ec {}\
				-ecc {}\
				-np {} -refgen {}\
				-od {}'.format(
				Os_DeepClean,S_SampleDir,S_outName,
				S_UMIread,I_um,I_ec,
				F_ecc,
				I_cores2,S_hg19,
				S_SampleDir
				))
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Check point
		try :
			S_dedup_ExSL_BAM = '{}/{}_sorted_filtered_propered_clipped_DC.bam'.format(S_SampleDir,S_outName)
			S_dedup_InSL_BAM = '{}/{}_sorted_filtered_propered_clipped_DC_InSL.bam'.format(S_SampleDir,S_outName)
			I_dedup_ExSL_OnTarget_ReadCount = int(os.popen('{} view -@ {} -q 1 -f 0x02 -c {} -L {}'.format(Os_Samtools,I_cores2,S_dedup_ExSL_BAM,S_bed)).read().strip())
			I_dedup_InSL_OnTarget_ReadCount = int(os.popen('{} view -@ {} -q 1 -f 0x02 -c {} -L {}'.format(Os_Samtools,I_cores2,S_dedup_InSL_BAM,S_bed)).read().strip())
			#------------------------------------------------------------------------------------
			F_ExSL_DedupRatio = round((1-I_dedup_ExSL_OnTarget_ReadCount/float(I_OnTarget_Mapped_ReadCount))*100,2)
			F_InSL_DedupRatio = round((1-I_dedup_InSL_OnTarget_ReadCount/float(I_OnTarget_Mapped_ReadCount))*100,2)
			#----------------------------------------------------------------------------------------
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_row_SummaryText = '{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n'.format(S_outName,'dedup_ExSL_ReadCount',I_dedup_ExSL_OnTarget_ReadCount,
				S_outName,'dedup_InSL_ReadCount',I_dedup_InSL_OnTarget_ReadCount,
				S_outName,'ExSL_DedupRatio',F_ExSL_DedupRatio,
				S_outName,'InSL_DedupRatio',F_InSL_DedupRatio)
			with open(S_SummaryTxt,'a') as f :
				f.write(S_row_SummaryText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\tDeepClean End   info : {}\
			\n\n\t|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*| Well Done.\
			\n\tExSL OnTarget ReadCount : {:,}\
			\n\tInSL OnTarget ReadCount : {:,}\
			\n\tExSL Dedup Ratio : {}%\
			\n\tInSL Dedup Ratio : {}%\
			'.format(datetime.now(),I_dedup_ExSL_OnTarget_ReadCount,I_dedup_InSL_OnTarget_ReadCount,F_ExSL_DedupRatio,F_InSL_DedupRatio)
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		except :
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\t*************\n\t{}\n\t*************\
			\n'.format('DeepClean Error')
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	#-------------------------------------------------------------------------------------------------------------------------------- UMI QC
	try :
		S_UmiqcDir = '{}/UMI_distribution'.format(S_SampleDir)
		S_countFile = '{}/{}.count'.format(S_UmiqcDir,S_outName)
		#-----------------------------------------------------
		df_count = pd.read_csv(S_countFile,sep='\t',header=None,names=['umi','count'])
		df_count_sorted = df_count.sort_values(by=['count'])
		#------------------------------------------------------------------------------------
		I_Total_UmiCount = np.sum(df_count_sorted['count'])
		I_Total_UmiTypeCount = len(set(df_count_sorted['umi']))
		I_Single_UmiCount = df_count_sorted[(df_count_sorted['count']==1)].shape[0]
		F_SingleUMI_ratio = round(I_Single_UmiCount/float(I_Total_UmiCount)*100,2)
		F_SingleUMItype_ratio = round(I_Single_UmiCount/float(I_Total_UmiTypeCount)*100,2)
		#------------------------------------------------------------------------------------
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_row_SummaryText = '{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n'.format(S_outName,'Total_UmiCount',I_Total_UmiCount,
			S_outName,'Total_UmiTypeCount',I_Total_UmiTypeCount,
			S_outName,'Single_UmiCount',I_Single_UmiCount,S_outName,'SingleUMI_ratio',F_SingleUMI_ratio,S_outName,'SingleUMItype_ratio',F_SingleUMItype_ratio)
		with open(S_SummaryTxt,'a') as f :
			f.write(S_row_SummaryText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  UMI distribution.\n\
		\n\tUMIdistribution already done.\
		\n\tTotal UMI count : {:,}\
		\n\tSingle UMI count : {:,}\
		\n\tSingle UMI Ratio : {}%\
		'.format(I_Total_UmiCount,I_Single_UmiCount,F_SingleUMI_ratio)
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	except :
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  UMI distribution.\n\
		\n\tUMIdistribution Start info : {}'.format(datetime.now())
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_UmiqcDir = '{}/UMI_distribution'.format(S_SampleDir)
		os.system('mkdir {}'.format(S_UmiqcDir))
		#-----------------------------------------------------
		os.system('cp {}/{}_sorted_filtered_propered_clipped_ClusterUMIStats.txt {}/{}.count'.format(S_SampleDir,S_outName,
			S_UmiqcDir,S_outName
			))
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Check point
		try :
			S_countFile = '{}/{}.count'.format(S_UmiqcDir,S_outName)
			#-----------------------------------------------------
			df_count = pd.read_csv(S_countFile,sep='\t',header=None,names=['umi','count'])
			df_count_sorted = df_count.sort_values(by=['count'])
			#------------------------------------------------------------------------------------
			I_Total_UmiCount = np.sum(df_count_sorted['count'])
			I_Total_UmiTypeCount = len(set(df_count_sorted['umi']))
			I_Single_UmiCount = df_count_sorted[(df_count_sorted['count']==1)].shape[0]
			F_SingleUMI_ratio = round(I_Single_UmiCount/float(I_Total_UmiCount)*100,2)
			F_SingleUMItype_ratio = round(I_Single_UmiCount/float(I_Total_UmiTypeCount)*100,2)
			#------------------------------------------------------------------------------------
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_row_SummaryText = '{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n{}\t{}\t{}\n'.format(S_outName,'Total_UmiCount',I_Total_UmiCount,
				S_outName,'Total_UmiTypeCount',I_Total_UmiTypeCount,
				S_outName,'Single_UmiCount',I_Single_UmiCount,S_outName,'SingleUMI_ratio',F_SingleUMI_ratio,S_outName,'SingleUMItype_ratio',F_SingleUMItype_ratio)
			with open(S_SummaryTxt,'a') as f :
				f.write(S_row_SummaryText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\tUMIdistribution End   info : {}\
			\n\n\t|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*| Well Done.\
			\n\tTotal UMI count : {:,}\
			\n\tSingle UMI count : {:,}\
			\n\tSingle UMI Ratio : {}%\
			'.format(datetime.now(),I_Total_UmiCount,I_Single_UmiCount,F_SingleUMI_ratio)
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		except :
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\t*************\n\t{}\n\t*************\
			\n'.format('UMIdistribution Error')
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@	
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	#-------------------------------------------------------------------------------------------------------------------------------- RODC [ROI depth coverage calculator]
	try :
		S_RODC_result = glob.glob('{}/RODC_result/{}*_Total_ROI_coverage.tsv'.format(S_SampleDir,S_outName))[0]
		with open(S_RODC_result) as f :
			for S_row in f :
				S_row_strip = S_row.strip()
				if S_row_strip.startswith('## mean depth') :
					F_MeanDepth = float(S_row_strip.split(' ')[-1])
				elif S_row_strip.startswith('{}x'.format(I_TargetDepth)) :
					F_TargetCoverage = float(S_row_strip.split('\t')[-1])
		#------------------------------------------------------------------------------------
		S_TargetCoverage = 'TargetCoverage({:,})'.format(I_TargetDepth)
		#------------------------------------------------------------------------------------
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  RODC.\n\
		\n\tRODC alread done.\
		\n\tMeanDepth : {:,}\
		\n\t{} : {}%\
		'.format(F_MeanDepth,S_TargetCoverage,F_TargetCoverage)
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_row_SummaryText = '{}\t{}\t{}\n{}\t{}\t{}\n'.format(S_outName,'MeanDepth',F_MeanDepth,
			S_outName,S_TargetCoverage,F_TargetCoverage)
		with open(S_SummaryTxt,'a') as f :
			f.write(S_row_SummaryText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	except :
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  RODC.\n\
		\n\tRODC Start info : {}'.format(datetime.now())
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		os.system('python {} -ipath {}/{}_sorted_filtered_propered_clipped_DC.bam -ibed {} -odir {}\
			-itype {} -mp {} -cf {}'.format(
			Os_RODC,S_SampleDir,S_outName,S_bed,S_SampleDir,
			'bam',I_cores2,I_TargetDepth
			))
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Check point
		try :
			S_RODC_result = glob.glob('{}/RODC_result/{}*_Total_ROI_coverage.tsv'.format(S_SampleDir,S_outName))[0]
			with open(S_RODC_result) as f :
				for S_row in f :
					S_row_strip = S_row.strip()
					if S_row_strip.startswith('## mean depth') :
						F_MeanDepth = float(S_row_strip.split(' ')[-1])
					elif S_row_strip.startswith('{}x'.format(I_TargetDepth)) :
						F_TargetCoverage = float(S_row_strip.split('\t')[-1])
			#------------------------------------------------------------------------------------
			S_TargetCoverage = 'TargetCoverage({:,})'.format(I_TargetDepth)
			#------------------------------------------------------------------------------------
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_row_SummaryText = '{}\t{}\t{}\n{}\t{}\t{}\n'.format(S_outName,'MeanDepth',F_MeanDepth,
				S_outName,S_TargetCoverage,F_TargetCoverage)
			with open(S_SummaryTxt,'a') as f :
				f.write(S_row_SummaryText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\tRODC End   info : {}\
			\n\n\t|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*| Well Done.\
			\n\tMeanDepth : {:,}\
			\n\t{} : {}%\
			'.format(datetime.now(),F_MeanDepth,S_TargetCoverage,F_TargetCoverage)
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		except :
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\t*************\n\t{}\n\t*************\
			\n'.format('RODC Error')
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@	
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	#-------------------------------------------------------------------------------------------------------------------------------- ROQA [Variants call]
	try :
		S_ROQA_InSR = '{}/{}_sorted_filtered_propered_clipped_DC_ROQA_result/{}_sorted_filtered_propered_clipped_DC_InSR_ROQA_report.txt'.format(S_SampleDir,S_outName,S_outName)
		df_ROQA_InSR = pd.read_csv(S_ROQA_InSR,sep='\t')
		#----------------------------------------------------------------------------------------
		I_Called_Variants_Count = df_ROQA_InSR.shape[0]
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ROQA.\n\
		\n\tROQA already done.\
		\n\tCalled Variants Count : {:,}\
		'.format(I_Called_Variants_Count)
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	except :
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ROQA.\n\
		\n\tROQA Start info : {}'.format(datetime.now())
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		os.system('python {} -ibam {}/{}_sorted_filtered_propered_clipped_DC.bam -ibed {} -odir {}\
			-bq {} -mp {} -id {}'.format(
			Os_ROQA,S_SampleDir,S_outName,S_bed,S_SampleDir,
			30,I_cores2,'n'
			))
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Check point
		try :
			S_ROQA_InSR = '{}/{}_sorted_filtered_propered_clipped_DC_ROQA_result/{}_sorted_filtered_propered_clipped_DC_InSR_ROQA_report.txt'.format(S_SampleDir,S_outName,S_outName)
			df_ROQA_InSR = pd.read_csv(S_ROQA_InSR,sep='\t')
			#------------------------------------------------------------------------------------
			I_Called_Variants_Count = df_ROQA_InSR.shape[0]
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\tROQA End   info : {}\
			\n\n\t|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*| Well Done.\
			\n\tCalled Variants Count : {:,}\
			'.format(datetime.now(),I_Called_Variants_Count)
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		except :
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\t*************\n\t{}\n\t*************\
			\n'.format('ROQA Error')
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	#-------------------------------------------------------------------------------------------------------------------------------- Annotation
	try :
		S_ANNO_InSR = '{}/{}_sorted_filtered_propered_clipped_DC_ROQA_result/Annotate/{}_sorted_filtered_propered_clipped_DC_InSR_ROQA_report_annotated.txt'.format(
			S_SampleDir,S_outName,S_outName)
		df_ANNO_InSR = pd.read_csv(S_ANNO_InSR,sep='\t')
		#----------------------------------------------------------------------------------------
		I_ANNO_Count = df_ANNO_InSR.shape[0]
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Annovar.\n\
		\n\tAnnovar already done.\
		\n\tAnnotated Variants Count : {:,}\
		\n\n================================================================\
		'.format(I_ANNO_Count)
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	except :
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		S_LogText = '\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Annovar.\n\
		\n\tAnnovar Start info : {}'.format(datetime.now())
		with open(S_SampleLog,'a') as f :
			f.write(S_LogText)
		#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		os.system('python {} -itxt {}/{}_sorted_filtered_propered_clipped_DC_ROQA_result/{}_sorted_filtered_propered_clipped_DC_InSR_ROQA_report.txt\
			'.format(
			Os_ANNO,S_SampleDir,S_outName,S_outName
			))
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Check point
		try :
			S_ANNO_InSR = '{}/{}_sorted_filtered_propered_clipped_DC_ROQA_result/Annotate/{}_sorted_filtered_propered_clipped_DC_InSR_ROQA_report_annotated.txt'.format(
				S_SampleDir,S_outName,S_outName)
			df_ANNO_InSR = pd.read_csv(S_ANNO_InSR,sep='\t')
			#------------------------------------------------------------------------------------
			I_ANNO_Count = df_ANNO_InSR.shape[0]
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\tAnnovar End   info : {}\
			\n\n\t|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*|*^o^*| Well Done.\
			\n\tAnnotated Variants Count : {:,}\
			\n\n================================================================\
			'.format(datetime.now(),I_ANNO_Count)
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		except :
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			S_LogText = '\n\t*************\n\t{}\n\t*************\
			\n'.format('Annovar Error')
			with open(S_SampleLog,'a') as f :
				f.write(S_LogText)
			#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	#--------------------------------------------------------------------------------------------------------------------------------
	#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	S_LogText = '\n\n\tEnd   info : {}\
	\n\n================================================================\
	'.format(datetime.now())
	with open(S_SampleLog,'a') as f :
		f.write(S_LogText)
	#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	S_LogText = '\n\n\t{} End   info : {}\
	\n\n================================================================\
	'.format(S_outName,datetime.now())
	with open(S_WrapperLog,'a') as f :
		f.write(S_LogText)
	#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
####################################################################################################################################
L_Fastqs = sorted(glob.glob(S_inFastqPath+'/*.fastq.gz'))
L_R1R2_fastqs = []
for I_index,S_inFile in enumerate(L_Fastqs) :
	S_inName = S_inFile.split('/')[-1]
	if not S_inName.startswith('Undetermined') :
		if I_index % 2 == 0 :
			S_R1 = S_inFile
			L_R1R2_fastqs.append([S_R1])
		else :
			S_R2 = S_inFile
			I_order =  len(L_R1R2_fastqs)-1
			L_R1R2_fastqs[I_order].append(S_R2)
I_R1R2_fastqs_count = len(L_R1R2_fastqs)
####################################################################################################################################
#----------------------------------------------------------------------------------
S_Basic_logs = '=========================== Wrapper log ===========================\n\
\n\tVersion : {}\
\n\tStart info : {}\
\n\tSample directory : {}\
\n\tSample count (R1,R2) : {}\
\n\tInput BED : {}\
\n\tInput primer : {}\
\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Used Options\n\
\n\tInput panel : {}\
\n\tMaximum core for multiple sample : {}\
\n\tMaximum core for multiple script : {}\
\n\tMinimum read length in primer trim : {}\
\n\tAllowed UMI mismatch : {}\
\n\tExcluded cluster of read count : {}\
\n\tMinimum ratio of error : {}\
\n\tTarget coverage depth : {:,}\
\n\tPopulation allele frequency database : {}\
\n\tAuto filter panel errror : {}\
\n\tFalse positive cutoff : {}\
\n\tGermline cutoff : {}\
\n\tAllele frequency cutoff : {}\
\n\tAlt count cutoff : {}\
\n\tDepth cutoff : {}\
\n\tCosmic hotspot cutoff : {}\
\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Panel Options\n\
\n\tSSE start position : {}\
\n\tSSE sequence : {}\
\n\tSSE number of tail base : {}\
\n\tGSP read : {}\
\n\tUMI read : {}\
\n\n================================================================\
'.format(S_version,datetime.now(),S_inFastqPath,I_R1R2_fastqs_count,
		S_bed,S_PrimerSeq,
		S_Panel,I_cores1,I_cores2,
		I_mr,
		I_um,I_ec,
		F_ecc,
		I_TargetDepth,
		S_pfdb,S_afpe,I_fpc,I_gc,F_afc,I_acc,I_dc,I_cpc,
		I_ssp,S_ss,I_st,S_GSPread,S_UMIread
		)
print S_Basic_logs
with open(S_WrapperLog,'w') as f :
	f.write(S_Basic_logs)
#----------------------------------------------------------------------------------
#=================================================================================================================================== Multi Processing for multi samples

M_pool_MultiRunner = Pool(processes=I_cores1)
M_pool_MultiRunner.map(MultiRunner, L_R1R2_fastqs)
##########################
M_pool_MultiRunner.close()
##########################
#===================================================================================================================================
#################################################################################################################################### Summary result
df_row_Summary = pd.read_csv(S_SummaryTxt,sep='\t')
#-----------------------------------------------------------------------------------------------------------------------------------
L_sample_ID = list(df_row_Summary['id'].drop_duplicates())
L_info = list(df_row_Summary['info'].drop_duplicates())
#-----------------------------------------------------------------------------------------------------------------------------------
df_SamplesSummary = pd.DataFrame()
for S_sample_ID in L_sample_ID :
	df_row_sample_summary = df_row_Summary[(df_row_Summary['id']==S_sample_ID)]
	df_sample_summary = df_row_sample_summary.drop(['id'],axis=1).rename(columns={'info':'id','value':S_sample_ID}).transpose().reset_index()
	df_sample_summary.columns = list(df_sample_summary.iloc[0])
	df_SampleSummary = df_sample_summary.reindex(df_sample_summary.index.drop(0))
	df_SamplesSummary = pd.concat([df_SamplesSummary,df_SampleSummary],ignore_index=True)
#-----------------------------------------------------------------------------------------------------------------------------------
df_SamplesSummary = df_SamplesSummary.sort_values(by=['id'])
df_SamplesSummary.to_csv('{}/Samples_Summary.txt'.format(S_StatisticDir),sep='\t',index=None)
#=================================================================================================================================== BAMs Dir
S_DC_BAMs_Dir = '{}/DC_BAMs'.format(S_outPath)
os.system('mkdir {}'.format(S_DC_BAMs_Dir))
os.system('ln -s {}/*/*DC.bam* {}'.format(S_outPath,S_DC_BAMs_Dir))
####################################################################################################################################  RODC

os.system('python {} -ipath {} -ibed {} -odir {} -mp {} -cf {}'.format(
	Os_RODC,
	S_DC_BAMs_Dir,
	S_bed,
	S_SummaryDir,
	I_cores2,
	I_TargetDepth
	))

#=================================================================================================================================== ANNOs Dir
S_DC_ANNOs_Dir = '{}/DC_ANNOs'.format(S_outPath)
os.system('mkdir {}'.format(S_DC_ANNOs_Dir))
os.system('cp {}/*/*_ROQA_result/Annotate/*_InSR_ROQA_report_annotated.txt {}'.format(S_outPath,S_DC_ANNOs_Dir))
#=================================================================================================================================== VCFs Dir
S_DC_VCFs_Dir = '{}/DC_VCFs'.format(S_outPath)
os.system('mkdir {}'.format(S_DC_VCFs_Dir))
os.system('cp {}/*/*_ROQA_result/*_InSR_ROQA.vcf {}'.format(S_outPath,S_DC_VCFs_Dir))
#=================================================================================================================================== Create forge
S_DC_FORGE_Dir = '{}/DC_FORGE'.format(S_outPath)
os.system('mkdir {}'.format(S_DC_FORGE_Dir))
#-----------------------------------------------------------------------------------------------------------------------------------
with open('{}/Forge.log'.format(S_DC_FORGE_Dir), 'w') as f :
	f.write('{}\n\n'.format(' '.join(sys.argv)))
for S_Forge in L_FORGE :
	with open('{}/Forge.log'.format(S_DC_FORGE_Dir), 'a') as f :
		f.write('{}\n'.format(S_Forge))
	if S_Forge != S_hg19 :
		os.system('cp {} {}'.format(S_Forge, S_DC_FORGE_Dir))
#===================================================================================================================================
S_LogText = '\n\
\n\tEnd info : {}\
\n\n================================================================'.format(datetime.now())
print S_LogText
with open(S_WrapperLog,'a') as f :
	f.write(S_LogText)
#-----------------------------------------------------------------------------------------------------------------------------------
####################################################################################################################################
